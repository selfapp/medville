//
//  LoginController.swift
//  JustStuck
//
//  Created by Gyan Routray on 13/12/16.
//  Copyright © 2016 Headerlabs. All rights reserved.
//


import UIKit
import MobileCoreServices

class LoginController: UIViewController, UITextFieldDelegate {
    
//MARK:- ----------@IBOutlets----------
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var credentialsBaseView: UIView!
    @IBOutlet weak var emailTextField: GRFloatingTextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

//MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        credentialsBaseView.layer.cornerRadius = 5.0
        credentialsBaseView.layer.borderColor = CustomColors.borderColor.cgColor
        credentialsBaseView.layer.borderWidth = 1
        credentialsBaseView.layer.masksToBounds = true
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        let border: CALayer = CALayer()
        border.backgroundColor = CustomColors.blueTheme.cgColor
        border.frame = CGRect(x: 0, y: (self.loginButton.titleLabel?.frame.size.height)! - 1, width: (self.loginButton.titleLabel?.frame.size.width)!, height: 1)
        self.loginButton.titleLabel?.layer.addSublayer(border)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    //MARK:- ----------@IBActions----------
    
    @IBAction func loginButtonAction(_ sender: Any) {
        //navigate to search controller
        let textStr = self.emailTextField.text?.replacingOccurrences(of: " ", with: "")
        let trimmedStr = textStr?.replacingOccurrences(of: "[ |()-]", with: "", options: .regularExpression, range: nil)

        guard let mobileNumber =  trimmedStr, mobileNumber.characters.count >= 10 else {
             self.showOkAlertWithTitle(title: "", message: "Please enter a valid mobile number", okCompletion: nil)
             return
        }
        self.emailTextField.resignFirstResponder()
        let locale = Locale.current as NSLocale
        let isdCode = locale.ISDCode()
        sendRequestToLogin(mobileNumber: mobileNumber, countryCode: isdCode)
      }
    
//MARK:- -----------API Requests------------
    func sendRequestToLogin(mobileNumber: String, countryCode: String) {
        startIndicator()
        
        let dict = ["user":["country_code":countryCode,"phone_number":mobileNumber]]
        makeHttpPostRequestWith(url: "/sessions/send_otp", dataDict:dict, shouldAuthorize: false, successBlock: { (success, result, statusCode
            ) in
            self.stopIndicator()
//            print(result ?? "D value")
                 DispatchQueue.main.async {
                    let passcodeVC = VerificationCodeController(nibName: "VerificationCodeController", bundle: Bundle.main)
                    passcodeVC.mobileNumber = mobileNumber
                    self.present(passcodeVC, animated: true, completion: nil)
                }
        }) { (message, statusCode) in
            self.stopIndicator()
            DispatchQueue.main.async {
                self.showOkAlertWithTitle(title: "", message: message, okCompletion: nil)
            }
        }
    }
    
    func isValidEmail(emailString:String) -> Bool {
        
//        print("validate emilId: \(emailString)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: emailString)
        return result
        
    }

//MARK:- ------ Alert & ActivityIndicator ------
//    func showAlertWithTitle(title: String, message: String ) {
//        let alertVC = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
//        let alertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (_ action) in
//            
//        })
//        alertVC.addAction(alertAction)
//        self.present(alertVC, animated: true, completion: nil)
//    }
    func startIndicator() {
        self.activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    func stopIndicator() {
        DispatchQueue.main.async() {
            if UIApplication.shared.isIgnoringInteractionEvents{
                UIApplication.shared.endIgnoringInteractionEvents()
            }
            self.activityIndicator.stopAnimating()
        }
    }
    
//MARK:- -------TextField Delegates-------
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let nsString = textField.text as NSString?
        let str = nsString!.replacingCharacters(in: range, with: string)
        if textField == self.emailTextField{
            return isPhoneNumberInCorrectFormat(string: string, str: str)
        }else{
            return true
        }
    }
    func isPhoneNumberInCorrectFormat(string: String?, str: String?) -> Bool{
        //If BackSpace
        if string == ""{
            return true
        }else if str!.characters.count < 3{
            if str!.characters.count == 1{
                self.emailTextField.text = "("
            }
        }else if str!.characters.count == 5{
            self.emailTextField.text = self.emailTextField.text! + ") "
        }else if str!.characters.count == 10{
            self.emailTextField.text = self.emailTextField.text! + "-"
        }else if str!.characters.count > 14{
            return false
        }
        return true
    }

//MARK:- -------TextField Notifications observer.-------
    @objc func keyBoardWillShow(notification: NSNotification) {
//        print("Keyboard will appear")
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue{
            let keyboardY = keyboardSize.origin.y
            
            if let textField = self.view.firstResponder(){
            let textFieldOrigin = textField.superview?.convert((textField.frame.origin), to: self.view)
            let originPlusHeight = (textFieldOrigin?.y)!+(textField.frame.size.height)
//            print(originPlusHeight)

            if originPlusHeight > keyboardY - 120 {
                var selfFrame = self.view.frame
                selfFrame.origin.y = -(originPlusHeight - keyboardY + 120)
                self.view.frame = selfFrame
            }
            }
        }
     }
    
    @objc func keyBoardWillHide(notification: NSNotification) {
//        print("Keyboard will dissapear")
        var selfFrame = self.view.frame
        selfFrame.origin.y = 0
        self.view.frame = selfFrame
    }

//MARK:-
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 }

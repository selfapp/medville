//
//  SearchResultCell.swift
//  JustStuck
//
//  Created by Gyan Routray on 15/12/16.
//  Copyright © 2016 Headerlabs. All rights reserved.
//

import UIKit
import SDWebImage

class SearchResultCell:

UITableViewCell {
    
    @IBOutlet weak var starView: HCSStarRatingView!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var cellTitleLabel: UILabel!

    @IBOutlet weak var optionButton: UIButton!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var cellSubTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cellSubTitleLabel.textColor = UIColor(red: 133/255.0, green: 165/255.0, blue: 255/255.0, alpha: 1.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func populateCellFor(doctor:Doctor){
        self.cellTitleLabel.text = doctor.first_name
        self.cellSubTitleLabel.text = doctor.specialization
        self.descriptionLabel.text = doctor.description
        self.iconImageView.sd_setImage(with: doctor.avatarPath) { (image, error, none, url) in
            if image != nil{
                self.iconImageView.image = image
            }
        }
        self.starView.value = 4.0
    }
    
}

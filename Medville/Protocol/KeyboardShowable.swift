//
//  KeyboardShowable.swift
//  Kannects
//
//  Created by Jitendra Solanki on 7/21/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import Foundation
import UIKit

 protocol KeyboardShowable {
   // associatedtype VCtype = LogInViewController
    
   // var activeTextField:UITextField {get set}
     func keyBoardWillShow(notification: NSNotification)
     func keyBoardWillHide(notification: NSNotification)
}

extension KeyboardShowable where Self == ViewController  {
    
   func keyBoardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue{
            let keyboardY = keyboardSize.origin.y
            if  let textField = self.view.firstResponder(){
               
                let textFieldOrigin = textField.superview?.convert((textField.frame.origin), to: self.view)
                var originPlusHeight: CGFloat = 0
                if textField.tag == 555{
                    originPlusHeight = (textFieldOrigin?.y)!+(textField.frame.size.height)
                }else{
                    originPlusHeight = (textFieldOrigin?.y)!+(textField.frame.size.height) + 120
                }
//                print(originPlusHeight)
                
                if originPlusHeight > keyboardY {
                    var selfFrame = self.view.frame
                    selfFrame.origin.y = -(originPlusHeight - keyboardY)
                    self.view.frame = selfFrame
                }
            }
        }
    }
    
    func keyBoardWillHide(notification: NSNotification) {
        var selfFrame = self.view.frame
        selfFrame.origin.y = 0
        self.view.frame = selfFrame
    }
    
}

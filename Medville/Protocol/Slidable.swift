//
//  Slidable.swift
//  Pocco
//
//  Created by Jitendra Solanki on 7/18/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import Foundation
import UIKit
import Photos

protocol Slidable {
    
    func slideRight(with completion:(Bool)->Void)
    func slideLeft(with completion:(Bool)->Void)
    
}


protocol ImageSelectable {
    //associatedtype ImageSelecter
     func openImagePicker()
}

//extension ImageSelectable where Self == EditProfileController{
//     
//}

extension ImageSelectable where  Self == ViewController{
    func openImagePicker(){
        imagePickerControllerWith(delegate: self)
    }
    
    func imagePickerControllerWith(delegate:(UIImagePickerControllerDelegate & UINavigationControllerDelegate)?){
        let actionSheet =  UIAlertController(title: "Medville", message: "", preferredStyle: .actionSheet)
        
        let camera  = UIAlertAction(title: "Take Photo", style: .default) { (cameraAction) in
            
            let mediaType = AVMediaType.video
            
            let authorizationStatus =  AVCaptureDevice.authorizationStatus(for: mediaType)
            
            if authorizationStatus == .authorized{
                self.prsentImagePickerWith(option: "Camera",delegate: delegate)
                
            }else if authorizationStatus == .notDetermined{
                AVCaptureDevice.requestAccess(for: mediaType, completionHandler: { (newStatus) in
                    if newStatus {
                        let authorizationStatus1 =  AVCaptureDevice.authorizationStatus(for: mediaType)
                        if authorizationStatus1 == .authorized{
                            self.prsentImagePickerWith(option: "Camera",delegate: delegate)
                        }
                        
                    }else{
                        
                    }
                })
            }else{
                
                let message = "It seems access for camera is denied for Medville. To make it work, Go to Setting -> Privacy -> Camera and enable the access for Medville app"
                self.showAlertWith(title: "", message: message, handler: nil)
            }
        }
        
        let gallery  = UIAlertAction(title: "Choose Photo", style: .default) { (galleryActn) in
            
            let status = PHPhotoLibrary.authorizationStatus()
            
            switch status{
                
            case .authorized:
                self.prsentImagePickerWith(option: "PhotoAlbum",delegate: delegate)
            case .notDetermined:
                PHPhotoLibrary.requestAuthorization({ (newStatus) in
                    if newStatus == PHAuthorizationStatus.authorized{
                        self.prsentImagePickerWith(option: "PhotoAlbum",delegate: delegate)
                        
                    }else{
                        
                    }
                })
            default:
                self.showAlertWith(title: "", message: "The access is not granted to use photo library. Go to Setting app to change the access for Medville", handler: nil)
                
            }
        }
        
        let delete = UIAlertAction(title: "Delete", style: .destructive) { (deleteAction) in
            
            self.showAlertWith(message: "This functionality is to be implemented.", title: "")
        }
        
        let cancel  = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        actionSheet.addAction(camera)
        actionSheet.addAction(gallery)
        actionSheet.addAction(delete)
        actionSheet.addAction(cancel)
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    private func prsentImagePickerWith(option:String,delegate:(UIImagePickerControllerDelegate & UINavigationControllerDelegate)?){
        let uiimagePicker = UIImagePickerController.init()
        uiimagePicker.delegate = delegate
        
        if option == "Camera" && UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
            uiimagePicker.sourceType = .camera
        }else {
            uiimagePicker.sourceType = .photoLibrary
        }
        
        DispatchQueue.main.async {
            self.present(uiimagePicker, animated: true, completion: nil)
        }
        
    }

}


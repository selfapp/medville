//
//  VerificationCodeController.swift
//  Medville
//
//  Created by Jitendra Solanki on 7/10/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

class VerificationCodeController: UIViewController {
    @IBOutlet weak var txtF_passcode: UITextField!

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var mobileNumber:String!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Action methods
    
    @IBAction func btnActn_submitPasscode(_ sender: Any) {
        
        guard let passCode = self.txtF_passcode.text, passCode.characters.count > 0 else {
              self.showAlertWith(title: "", message: "Please enter a valid OTP.", handler: nil)
            return
        }
        
        var deviceToken = DefaultParams.deviceToken
        if deviceToken.isEmpty{
            self.showAlertWith(title: "", message: "Unable to fetch token for video calling. Please go to your phone settings and allow application to send you remote notifications. Kill and restart your application again.", handler: { (alert) in
            })
            deviceToken = "howh62973ytrb7qw9rb6qwr668w9qe6r9bpcw6r"
        }
        let dict =  ["user":
                            ["country_code":(Locale.current as NSLocale).ISDCode(),
                              "phone_number": mobileNumber,
                              "otp":passCode,
                            ],
                    "device":
                        ["uuid":DefaultParams.uuid,
                         "token":deviceToken
                        ]
                    ]
        verifyPasscode(data: dict)

//        let appdelegate = UIApplication.shared.delegate as! AppDelegate
//        appdelegate.window?.rootViewController = doctorList
        
    }
    //MARK:- -------TextField Notifications observer.-------
    @objc func keyBoardWillShow(notification: NSNotification) {
//        print("Keyboard will appear")
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue{
            let keyboardY = keyboardSize.origin.y
            
            if let textField = self.view.firstResponder(){
                let textFieldOrigin = textField.superview?.convert((textField.frame.origin), to: self.view)
                let originPlusHeight = (textFieldOrigin?.y)!+(textField.frame.size.height)
//                print(originPlusHeight)
                
                if originPlusHeight > keyboardY - 120 {
                    var selfFrame = self.view.frame
                    selfFrame.origin.y = -(originPlusHeight - keyboardY + 120)
                    self.view.frame = selfFrame
                }
            }
        }
    }
    
    @objc func keyBoardWillHide(notification: NSNotification) {
//        print("Keyboard will dissapear")
        var selfFrame = self.view.frame
        selfFrame.origin.y = 0
        self.view.frame = selfFrame
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }

    func verifyPasscode(data:[String:Any]){
        self.txtF_passcode.resignFirstResponder()
        self.startIndicator()
        makeHttpPostRequestWith(url: "/sessions/verify_otp", dataDict: data, shouldAuthorize: false, successBlock: { (success, result, statusCode) in
            self.stopIndicator()
            //check here for type doctor and user
            if let dictResult = result as? [String:Any],var resource = dictResult["resource"]as? [String:Any]{
                let json = JSON(dictResult)
                let userType =  json["type"].stringValue
                let authToken =  json["auth_token"].stringValue
                resource["auth_token"] = authToken
                
                //set usertype
                AppUser.type = (userType.lowercased() == "doctor") ? kDOCTOR : kUSER
//                print(resource)
                //update user details
                AppUser().updateUserDetails(detailsDict: resource)
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                if AppUser.type == kUSER{
                    //get list of doctors 
                    //let arrDoctors = Doctor.doctorsWith(dict: resource)
                    //appDelegate.doctors  = arrDoctors
                    appDelegate.userLoggedInSetup()
                }else{
                    appDelegate.doctorLoggedInSetup()
                }
//                print("heondjfjf")
            }
            
        }) { (message, statusCode) in
            self.stopIndicator()
            self.showOkAlertWithTitle(title: "", message: message, okCompletion: nil)
        }
    }

    func startIndicator() {
        self.activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    func stopIndicator() {
        DispatchQueue.main.async() {
            if UIApplication.shared.isIgnoringInteractionEvents{
                UIApplication.shared.endIgnoringInteractionEvents()
            }
            self.activityIndicator.stopAnimating()
        }
    }
    
}

//
//  DoctorListTableViewCell.swift
//  Medville
//
//  Created by Jitendra Solanki on 10/5/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

class DoctorListTableViewCell: UITableViewCell {

    @IBOutlet weak var cyanView: UIView!
    @IBOutlet weak var doctorImageView: UIImageView!
    @IBOutlet weak var doctorNameLbl: UILabel!
    @IBOutlet weak var doctorSpecificationLbl: UILabel!
    @IBOutlet weak var doctorAddress: UILabel!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var doctorRating: UIButton!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        baseView.addShadowWith(opacity: 0.4, radius: 6, offset: CGSize(width:2,height:2), color: .lightGray)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func showDoctorSpecificData(doctor:Doctor){
        self.doctorNameLbl.text = doctor.first_name.capitalized
        self.doctorAddress.text = doctor.address
        self.doctorSpecificationLbl.text = doctor.specialization
        //self.doctorImageView.image = #imageLiteral(resourceName: "defualt-profile")
        self.doctorImageView.sd_setImage(with: doctor.avatarPath, placeholderImage: UIImage(named: "doctor"))

        self.doctorRating.setTitle(String(doctor.rating), for: .normal)
    }
    
}

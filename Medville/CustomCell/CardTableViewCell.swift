//
//  CardTableViewCell.swift
//  Medville
//
//  Created by Jitendra Solanki on 10/12/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit


class CardTableViewCell: UITableViewCell {

    //MARK:- outlets
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lbl_cardNumber: UILabel!
    @IBOutlet weak var imgV_checkMark: UIImageView!
    @IBOutlet weak var selectButton: UIButton!
    
    @IBOutlet weak var deleteButton: UIButton!
    //MARK:- iVar
    var isCardSelected = false{
        didSet{
            imgV_checkMark.image = isCardSelected ? #imageLiteral(resourceName: "Oval-selected") : #imageLiteral(resourceName: "Oval")
        }
    }
    
//    var cardType:CardType? {
//        didSet{
//            if let cardtype = cardType{
//
//                switch cardtype{
//                case .VISA:
//                    imgV_cardType.image = #imageLiteral(resourceName: "cc-visa")
//                case .MASTER_CARD:
//                    imgV_cardType.image =  #imageLiteral(resourceName: "cc-mastercard")
//                case .AMEX:
//                    imgV_cardType.image = #imageLiteral(resourceName: "stp_card_amex.png")
//                case .DINER_CLUB:
//                    imgV_cardType.image = #imageLiteral(resourceName: "stp_card_diners.png")
//                case .DISCOVER:
//                    imgV_cardType.image = #imageLiteral(resourceName: "stp_card_discover.png")
//                case .JCB:
//                    imgV_cardType.image = #imageLiteral(resourceName: "stp_card_jcb.png")
//                case .UNKNOWN:
//                    imgV_cardType.image = nil
//                }
//
//            }else{
//                imgV_cardType.image = nil
//            }
//        }
//    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.addBorderWith(borderWidth: 1.0, withBorderColor: UIColor(r:220,g:221,b:221,alpha:1.0), withCornerRadious: 3.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Set data
    func setDataFor(card:Card){
    
        self.lbl_cardNumber.text = "XXXX XXXX XXXX "+card.last4
        self.isCardSelected = false
        
    }
    
}

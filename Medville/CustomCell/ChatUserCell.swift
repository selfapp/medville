//
//  ChatUserCell.swift
//  Medville
//
//  Created by Durgesh on 06/02/18.
//  Copyright © 2018 jitendra. All rights reserved.
//

import UIKit

class ChatUserCell: UITableViewCell {

    @IBOutlet weak var avtorImageView: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var messagelbl: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        avtorImageView.layer.cornerRadius = avtorImageView.frame.height / 2
        avtorImageView.clipsToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

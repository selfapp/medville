//
//  MyPatientsCell.swift
//  Medville
//
//  Created by Jitendra Solanki on 10/4/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

class MyPatientsCell: UITableViewCell {

    @IBOutlet weak var lbl_name: UILabel!
    
    @IBOutlet weak var imgV_profilePic: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
         imgV_profilePic.layer.cornerRadius = imgV_profilePic.frame.size.width/2
        imgV_profilePic.layer.masksToBounds = true
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDataFor(patient:Patient){
        
    }
    
}

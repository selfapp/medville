//
//  DoctorProfileCommentCell.swift
//  Medville
//
//  Created by Sunder Singh on 10/12/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

class DoctorProfileCommentCell: UITableViewCell {

    //MARK:- Outlets
    
    @IBOutlet weak var patientCommentsLbl: UILabel!
    @IBOutlet weak var patientImageView: UIImageView!
    @IBOutlet weak var patientCommentTimeLbl: UILabel!
    @IBOutlet weak var patientNameLbl: UILabel!
    @IBOutlet weak var patientRatingView: HCSStarRatingView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        patientImageView.layer.cornerRadius = patientImageView.frame.size.height/2
        patientImageView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func showPatientCommentData(feedBack:Feedback){
        self.patientImageView.sd_setImage(with: feedBack.patient.avatarPath, placeholderImage: UIImage(named: "defualt-profile"))
        self.patientNameLbl.text = feedBack.patient.firstName
        self.patientRatingView.value = CGFloat(feedBack.rating)
        self.patientCommentTimeLbl.text = feedBack.created_at.getTimeDifferenceStringTillNowWith(currentFormat: DateFormat.kCurrentFormat.rawValue)
        self.patientCommentsLbl.text = feedBack.comment
    }
}

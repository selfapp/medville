//
//  AppointmentCell.swift
//  Medville
//
//  Created by Durgesh on 22/11/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

class AppointmentCell: UITableViewCell {

    @IBOutlet weak var appointmentStatus: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeDetailLabel: UILabel!
    @IBOutlet weak var avtorImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.avtorImage?.frame = CGRect(x: 10, y: 5, width: self.frame.size.height - 10, height: self.frame.size.height - 10)
        self.avtorImage?.layer.cornerRadius = (self.avtorImage?.frame.size.height)! / 2
        self.avtorImage?.clipsToBounds = true
        
        self.avtorImage?.contentMode = UIViewContentMode.scaleToFill
    }

}

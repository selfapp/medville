//
//  PatientNotificationCell.swift
//  Medville
//
//  Created by Sunder Singh on 10/11/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

class PatientNotificationCell: UITableViewCell {


    @IBOutlet weak var notificationMsg: UILabel!

    @IBOutlet weak var notificationTime: UILabel!
    @IBOutlet weak var notificationIcon: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        notificationIcon.layer.cornerRadius = notificationIcon.frame.size.height/2
        notificationIcon.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

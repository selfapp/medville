//
//  SearchController.swift
//  JustStuck
//
//  Created by Gyan Routray on 14/12/16.
//  Copyright © 2016 Headerlabs. All rights reserved.
//

import UIKit
//import MFSideMenu
class SearchController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var searchInputTextField: UITextField!

    @IBOutlet weak var searchBGView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Medville"
        let leftBarButton = UIBarButtonItem.init(image: UIImage(named: "menu_gray")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(SearchController.leftBarButtonAction))
        self.navigationItem.leftBarButtonItem = leftBarButton
        let line = UIView(frame: CGRect(x: 0, y:43, width: UIScreen.main.bounds.size.width, height: 1))
            line.backgroundColor = UIColor.lightGray
        self.navigationController?.navigationBar.addSubview(line)
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        //self.navigationController?.navigationBar.isTranslucent = false
        
        searchBGView.layer.cornerRadius = 3.0
        searchBGView.layer.borderColor = UIColor.lightGray.cgColor
        searchBGView.layer.borderWidth = 1
        searchBGView.layer.masksToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.tintColor = CustomColors.menuButtonTint
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    @IBAction func searchButtonAction(_ sender: Any) {
        guard let searchKey = self.searchInputTextField.text else {
            return
        }
        if searchKey.characters.count < 1{
//            print("Show alert to enter text.")
            self.showAlertWithTitle(title: "", message: "Please enter the key to search.")
            return
        }
        let searchResultVC: SearchResultController = SearchResultController(nibName:"SearchResultController", bundle: nil)
        searchResultVC.searchText = searchKey
        self.navigationController?.pushViewController(searchResultVC, animated: true)
    }
    
//MARK: - Menu Actions
    func leftBarButtonAction() {
        
        self.menuContainerViewController.toggleLeftSideMenuCompletion { }
 
     }
    
    func showAlertWithTitle(title: String, message: String ) {
        let alertVC = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (_ action) in
            
        })
        alertVC.addAction(alertAction)
        self.present(alertVC, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 }

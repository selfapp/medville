//
//  Constant.swift
//  JustStuck
//
//  Created by Gyan Routray on 20/12/16.
//  Copyright © 2016 Headerlabs. All rights reserved.
//

import Foundation
import UIKit

let kLoginStatusKey = "logged_in"
let kCurrentRoleKey = "current_role"
let kAuthTokenKey = "auth_token"
let kRefreshTokenKey = "refresh_token"
let kDeviceTokenTwilio = "deviceTokenTwilio"
let kUserDetailsKey = "user_details"
let kUSER = "normalUser"
let kDOCTOR = "M_doctor"
let kPATIENT = "M_patient"
let kPROVIDER = "provider"
let kHyphenate = "hyphnate"
let kChatUser = "chatUser"


//production: Apple Developer account
//email: info@medville.net
//password: Skymeadows18!

// Strip details
// Id: djchandok1@gmail.com
// password: Gmail18!

// Pusher --> https://dash.pusher.com
//email: info@medville.net
//password: Skymeadows18!

//https://app.medville.net/admin (edited)
// Use same url on TimeKit
// timekit.io    djchandok1@gmail.com    Gmail18!

//let kBaseURL = "http://staging.medville.net"// Staging
//let kBaseURL = "http://192.168.1.69:3000"// Local
let kBaseURL = "https://app.medville.net" //Production

let kSignUpURL =  kBaseURL + "/sign-up"
let kLoginURL =  kBaseURL + "/sign-in.json"
let kLogoutURL =  kBaseURL + "/sign-out.json"
let kUpdateProfileURL =  kBaseURL + "/profile-update.json"
let kUpdatePasswordURL =  kBaseURL + "/password-update.json"
let kAddServiceURL =  kBaseURL + "/add-service.json"

let kApiKey = "45914692"
// Replace with your generated session ID
let kSessionId = "1_MX40NTc3MjEwMn5-MTQ4NzIzODcwMDgzOH5qc3R0MDh2Zkt6Wk1OSTFsVEQ3TCtIcEJ-fg"
// Replace with your generated token
let kToken = "T1==cGFydG5lcl9pZD00NTc3MjEwMiZzaWc9YjQ0MmFiMTAxMGU2MDM0M2U3MzE3NmY4N2ZiMWU0MmEwOGY0ODMxODpyb2xlPW1vZGVyYXRvciZzZXNzaW9uX2lkPTFfTVg0ME5UYzNNakV3TW41LU1UUTROekl6T0Rjd01EZ3pPSDVxYzNSME1EaDJaa3Q2V2sxT1NURnNWRVEzVEN0SWNFSi1mZyZjcmVhdGVfdGltZT0xNDg3MjM4ODExJm5vbmNlPTAuNDQyMTU1NTEyNDU3Mjg3MjQmZXhwaXJlX3RpbWU9MTQ4Nzg0MzYxMSZjb25uZWN0aW9uX2RhdGE9bmFtZSUzREpvaG5ueQ=="

//MARK:-
struct CustomColors {
    static var menuButtonTint: UIColor = UIColor.init(red: 98/255, green: 98/255, blue: 98/255, alpha: 1)
    static var borderColor: UIColor = UIColor.init(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
    static var blueTheme: UIColor = UIColor.init(red: 100/255, green: 150/255, blue: 250/255, alpha: 1)
    static var searchBarColor: UIColor = UIColor.init(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
}








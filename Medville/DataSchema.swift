//
//  DataSchema.swift
//  Medville
//
//  Created by Jitendra Solanki on 7/10/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import Foundation

struct Doctor{
    
    var id: Int
    var name:String
    var address:String
    var phoneNumber:String
    var title:String
    var description:String
    var avatarPath:URL
    
    init?(json:[String:Any]){
       
        guard
        let dId = json["id"] as? Int,
        let name = json["name"]as? String,
        let address = json["address"]as? String,
        let phone =  json["phone_number"]as? String,
        let title = json["title"]as? String,
        let description = json["description"]as? String,
        let imageUrl = json["avatar_path_relative"]as? String,
        let fullImageUrl  = URL(string: kBaseURL+imageUrl)
        else {
             return nil
        }
        
        self.id = dId
        self.name = name
        self.address = address
        self.phoneNumber = phone
        self.title = title
        self.description = description
        self.avatarPath = fullImageUrl
    }
    
    //
    static func doctorsWith(arrDoctors:[[String:Any]])-> [Doctor]?{
        
            let allDoctors:[Doctor] =  arrDoctors.map({ (doctor) -> Doctor? in
                let newDoctor = Doctor(json: doctor)
                return newDoctor
            }).flatMap{$0}
            
            return allDoctors
            
        }
}

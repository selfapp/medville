//
//  AudioViewController.swift
//  Medville
//
//  Created by Durgesh on 28/02/18.
//  Copyright © 2018 jitendra. All rights reserved.
//

import UIKit
import PushKit
import TwilioVoice
import PushNotifications

let identity = "alice"
let twimlParamTo = "To"

class AudioViewController: UIViewController {
    
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var speakerButton: UIButton!
    @IBOutlet weak var muteButton: UIButton!
    @IBOutlet weak var nameLabel:UILabel!
    @IBOutlet weak var callingLabel: UILabel!
    @IBOutlet weak var ansButton: UIButton!
    @IBOutlet weak var rejectCallButton: UIButton!
    let pulsator = Pulsator()

    var deviceTokenString:String?
    
    var voipRegistry:PKPushRegistry
    
    var isSpinning: Bool
    var incomingAlertController: UIAlertController?
    
    var callInvite:TVOCallInvite?
    var call:TVOCall?
    
    var ringtonePlayer:AVAudioPlayer?
    var ringtonePlaybackCallback: (() -> ())?

    var callerIdentity:String?
    var isIncommingCall:Bool = false
    var callerName:String?
    var currentUserName:String = ""
    
    static let shared = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AudioViewController") as! AudioViewController

    required init?(coder aDecoder: NSCoder) {
        isSpinning = false
        voipRegistry = PKPushRegistry.init(queue: DispatchQueue.main)
        super.init(coder: aDecoder)
        if !isIncommingCall{
            voipRegistry.delegate = self
        }
        voipRegistry.desiredPushTypes = Set([PKPushType.voIP])
        TwilioVoice.logLevel = .error
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = ""
        if isIncommingCall{
            self.ansButton.isHidden = false
        }else{
            voipRegistry.delegate = self
            self.ansButton.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupInitialValues()
    }
    func setupInitialValues(){
        
        iconView.layer.superlayer?.insertSublayer(self.pulsator, below: iconView.layer)
        iconView.layer.cornerRadius = iconView.frame.size.width / 2
        iconView.clipsToBounds = true
        self.pulsator.backgroundColor = UIColor(
            red: CGFloat(0.976),
            green: CGFloat(0.976),
            blue: CGFloat(0.976),
            alpha: CGFloat(1.0)).cgColor
        self.pulsator.numPulse = Int(5)
        self.pulsator.radius = CGFloat(0.7) * 200
        self.pulsator.animationDuration = Double(0.5) * 10
    }
    override func viewDidLayoutSubviews() {
        view.layer.layoutIfNeeded()
        self.pulsator.position = iconView.layer.position
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isIncommingCall{
            self.callingLabel.text = "Voice Calling..."
            self.ansButton.isHidden = false
        }else{
            voipRegistry.delegate = self
            self.ansButton.isHidden = true
        }
        nameLabel.text = callerName
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissView(){
        self.muteButton.isSelected = false
        self.speakerButton.isSelected = false
        self.isIncommingCall = false
        self.iconView.isHidden = false
        self.pulsator.stop()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func ansCall(_ sender: Any) {
        
            self.stopIncomingRingtone()
            self.call = callInvite?.accept(with: self)
            self.callInvite = nil
            self.incomingAlertController = nil
            self.ansButton.isHidden = true
            self.speakerButton.isSelected = true
        self.pulsator.stop()
        iconView.isHidden = true
    }
    

    @IBAction func rejectCall(_ sender: Any) {
        if (self.call != nil) {
            self.call?.disconnect()
        }
        if callInvite != nil{
            self.stopIncomingRingtone()
            self.pulsator.stop()
            callInvite?.reject()
            self.callInvite = nil
            self.dismissView()
        }
        voipRegistry.delegate = UIApplication.shared.appDelegate
    }
    
    func placeCall(){
            guard let accessToken = AppUser.accessTokenForTwilio else {
                return
            }
            playOutgoingRingtone(completion: { [weak self] in
                if let strongSelf = self {
                    strongSelf.call = TwilioVoice.call(accessToken, params: [twimlParamTo : strongSelf.callerIdentity!,"callerId":"client:\(strongSelf.currentUserName)"], delegate: strongSelf)
                    strongSelf.callingLabel.text = "Voice Calling..."
                    strongSelf.ansButton.isHidden = true
                    strongSelf.speakerButton.isSelected = true
                    strongSelf.pulsator.start()
                }
            })
    }
    
    
    @IBAction func speakerButtonToggled(_ sender: UIButton){
        if self.speakerButton.isSelected{
            toggleAudioRoute(toSpeaker: false)
            self.speakerButton.isSelected = false
        }else{
            toggleAudioRoute(toSpeaker: true)
            self.speakerButton.isSelected = true
        }
    }
    
    @IBAction func muteButtonToggeled(_ sender: UIButton){
        if let call = call{
            if self.muteButton.isSelected{
                call.isMuted = false
                self.muteButton.isSelected = false
            }else{
                call.isMuted = true
                self.muteButton.isSelected = true
            }
        }else{
            NSLog("No active call to be muted")
        }
    }
    
    func callDisconnected() {
        self.call = nil
        playDisconnectSound()
       // stopSpin()
        self.pulsator.stop()
        self.dismissView()
    }
    
// MARK: AVAudioSession
    func toggleAudioRoute(toSpeaker: Bool) {
        // The mode set by the Voice SDK is "VoiceChat" so the default audio route is the built-in receiver. Use port override to switch the route.
        do {
            if (toSpeaker) {
                try AVAudioSession.sharedInstance().overrideOutputAudioPort(.speaker)
            } else {
                try AVAudioSession.sharedInstance().overrideOutputAudioPort(.none)
            }
        } catch {
            NSLog(error.localizedDescription)
        }
    }
// MARK: Ringtone player & AVAudioPlayerDelegate
    func playOutgoingRingtone(completion: @escaping () -> ()) {
        self.ringtonePlaybackCallback = completion
        
        let ringtonePath = URL(fileURLWithPath: Bundle.main.path(forResource: "outgoing", ofType: "wav")!)
        do {
            self.ringtonePlayer = try AVAudioPlayer(contentsOf: ringtonePath)
            self.ringtonePlayer?.delegate = self
            
            playRingtone()
        } catch {
            NSLog("Failed to initialize audio player")
            self.ringtonePlaybackCallback?()
        }
    }
    func playIncomingRingtone() {
        let ringtonePath = URL(fileURLWithPath: Bundle.main.path(forResource: "incoming", ofType: "wav")!)
        do {
            self.ringtonePlayer = try AVAudioPlayer(contentsOf: ringtonePath)
            self.ringtonePlayer?.delegate = self
            self.ringtonePlayer?.numberOfLoops = -1
            
            playRingtone()
        } catch {
            NSLog("Failed to initialize audio player")
        }
    }
    
    func stopIncomingRingtone() {
        if (self.ringtonePlayer?.isPlaying == false) {
            return
        }
        
        self.ringtonePlayer?.stop()
        self.iconView.isHidden = true
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord)
        } catch {
            NSLog(error.localizedDescription)
        }
    }
    
    func playDisconnectSound() {
        let ringtonePath = URL(fileURLWithPath: Bundle.main.path(forResource: "disconnect", ofType: "wav")!)
        do {
            self.ringtonePlayer = try AVAudioPlayer(contentsOf: ringtonePath)
            self.ringtonePlayer?.delegate = self
            self.ringtonePlaybackCallback = nil
            
            playRingtone()
        } catch {
            NSLog("Failed to initialize audio player")
        }
    }
    
    func playRingtone() {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        } catch {
            NSLog(error.localizedDescription)
        }
        
        self.ringtonePlayer?.volume = 1.0
        self.ringtonePlayer?.play()
    }
    
}

//MARK:- Pk push delegate
extension AudioViewController:PKPushRegistryDelegate{
    
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        NSLog("pushRegistry:didUpdatePushCredentials:forType:");
        
        if (type != .voIP) {
            return
        }
        
        guard let accessToken = AppUser.accessTokenForTwilio else {
            return
        }
        
        let deviceToken = (pushCredentials.token as NSData).description
        
        TwilioVoice.register(withAccessToken: accessToken, deviceToken: deviceToken) { (error) in
            if let error = error {
                NSLog("An error occurred while registering: \(error.localizedDescription)")
            }
            else {
                NSLog("Successfully registered for VoIP push notifications.")
            }
        }
        
        self.deviceTokenString = deviceToken
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType) {
        NSLog("pushRegistry:didInvalidatePushTokenForType:")
        
        if (type != .voIP) {
            return
        }
        
        guard let deviceToken = deviceTokenString, let accessToken = AppUser.accessTokenForTwilio else {
            return
        }
        
        TwilioVoice.unregister(withAccessToken: accessToken, deviceToken: deviceToken) { (error) in
            if let error = error {
                NSLog("An error occurred while unregistering: \(error.localizedDescription)")
            }
            else {
                NSLog("Successfully unregistered from VoIP push notifications.")
            }
        }
        self.deviceTokenString = nil
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {
        NSLog("pushRegistry:didReceiveIncomingPushWithPayload:forType:")
        
        if (type == PKPushType.voIP) {
            TwilioVoice.handleNotification(payload.dictionaryPayload, delegate: self)
        }
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType) {
        NSLog("pushRegistry:didReceiveIncomingPushWithPayload:forType:\(payload.dictionaryPayload)")
        if (type == PKPushType.voIP) {
            TwilioVoice.handleNotification(payload.dictionaryPayload, delegate: self)
        }
    }
}

//MARK:- TVO Notification delegate
extension AudioViewController:TVONotificationDelegate{
    
    func callInviteReceived(_ callInvite: TVOCallInvite) {
        if (callInvite.state == .pending) {
            handleCallInviteReceived(callInvite)
        } else if (callInvite.state == .canceled) {
            handleCallInviteCanceled(callInvite)
        }else if (callInvite.state == .accepted)
        {
            
        }
    }
    func notificationError(_ error: Error) {
        NSLog("notificationError: \(error.localizedDescription)")
    }
    
    
    func handleCallInviteReceived(_ callInvite: TVOCallInvite) {
        NSLog("callInviteReceived:")
        
        if (self.callInvite != nil && self.callInvite?.state == .pending) {
            NSLog("Already a pending call invite. Ignoring incoming call invite from \(callInvite.from)")
            return
        } else if (self.call != nil && self.call?.state == .connected) {
            NSLog("Already an active call. Ignoring incoming call invite from \(callInvite.from)");
            return;
        }
        
        self.callInvite = callInvite;
        playIncomingRingtone()
         DispatchQueue.main.async {
            self.pulsator.start()
        }
        let from = callInvite.from
        let alertMessage = "From: \(from)"
        self.callerName = alertMessage
        isIncommingCall = true
        
        if let thePresentedVC : UIViewController =  UIApplication.shared.keyWindow?.rootViewController?.presentedViewController as UIViewController?{
            if (thePresentedVC.isKind(of: UIAlertController.self)){
                thePresentedVC.dismiss(animated: true, completion: {
                    UIApplication.shared.keyWindow?.rootViewController?.present(self, animated: true, completion: nil)
                })
            }else{
                
                UIApplication.shared.keyWindow?.rootViewController?.present(self, animated: true, completion: nil)
            }
        }else{
            UIApplication.shared.keyWindow?.rootViewController?.present(self, animated: true, completion: nil)
            
        }
        // If the application is not in the foreground, post a local notification
        if (UIApplication.shared.applicationState != UIApplicationState.active) {
            let notification = UILocalNotification()
            notification.alertBody = "Incoming Call From \(from)"
            
            UIApplication.shared.presentLocalNotificationNow(notification)
        }
    }
    
    func handleCallInviteCanceled(_ callInvite: TVOCallInvite) {
        NSLog("callInviteCanceled:")
        
        if (callInvite.callSid != self.callInvite?.callSid) {
            NSLog("Incoming (but not current) call invite from \(callInvite.from) canceled. Just ignore it.");
            return;
        }
        
        self.stopIncomingRingtone()
        playDisconnectSound()
        if isIncommingCall{
            self.dismissView()
        }
            
        self.callInvite = nil
        UIApplication.shared.cancelAllLocalNotifications()
    }

}

//MARK:- TVO call delegate
extension AudioViewController:TVOCallDelegate{
    func callDidConnect(_ call: TVOCall) {
        NSLog("callDidConnect:")
        
        self.call = call
        self.rejectCallButton.isHidden = false
        self.ansButton.isHidden = true
        //self.pulsator.stop()
        //stopSpin()
        toggleAudioRoute(toSpeaker: true)
    }
    
    func call(_ call: TVOCall, didFailToConnectWithError error: Error) {
        callDisconnected()
    }
    
    func call(_ call: TVOCall, didDisconnectWithError error: Error?) {
        if let error = error {
            NSLog("Call failed: \(error.localizedDescription)")
        } else {
            NSLog("Call disconnected")
        }
        callDisconnected()
    }
    
}


//MARK:- AV audio delegate
extension AudioViewController:AVAudioPlayerDelegate{
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if (self.ringtonePlaybackCallback != nil) {
            DispatchQueue.main.async {
                self.ringtonePlaybackCallback!()
            }
        }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord)
        } catch {
            NSLog(error.localizedDescription)
        }
    }

}




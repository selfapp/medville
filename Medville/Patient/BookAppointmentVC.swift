//
//  BookAppointmentVC.swift
//  Medville
//
//  Created by Durgesh on 17/11/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

class BookAppointmentVC: UIViewController,UIWebViewDelegate {

    var appointment_html :String?
    
    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.navigationItem.title = "Book"
        webView.delegate = self
        webView.loadHTMLString(appointment_html!, baseURL: nil)        
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
//        print("Loading finish")
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
//        print(error)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonTap(_ sender: Any) {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
        self.tabBarController?.selectedIndex = 1
    }
    
    

}

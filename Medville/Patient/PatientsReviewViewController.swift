//
//  PatientsReviewViewController.swift
//  Medville
//
//  Created by Sunder Singh on 10/9/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

class PatientsReviewViewController: UIViewController {

    //MARK:- Outlets
//    @IBOutlet weak var patientCmntTxtView: UITextView!
    @IBOutlet weak var patientCmntTxtView: GRFloatingTextView!
    @IBOutlet weak var placeHolderLbl: UILabel!
    @IBOutlet weak var doctorRatingView: HCSStarRatingView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var appointmentId:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initializeUI()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide),
                                               name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillHide() {
        self.view.frame.origin.y = 64
    }
    
    @objc func keyboardWillChange(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if patientCmntTxtView.isFirstResponder {
                self.view.frame.origin.y = -(keyboardSize.height / 2 - 64)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Private helper methods
    private func initializeUI(){
        self.navigationItem.title = "Review"
        let button =  UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(backBtnTapped))
        self.navigationItem.leftBarButtonItem = button
        edgesForExtendedLayout = []
        patientCmntTxtView.addInputAccessoryViewWith(target: self, selector: #selector(doneBtnTappedOn_Keyboard))

        patientCmntTxtView.delegate = self
    }
    
    //MARK:- Custom Action and IBAction Based methods
    @objc private func backBtnTapped(){
        self.navigationController?.popViewController(animated: true)
    }
    @objc private func doneBtnTappedOn_Keyboard(){
        view.endEditing(true)
    }
    func textViewDidChange(_ textView: UITextView) {
        placeHolderLbl.isHidden = !textView.text.isEmpty
    }
    
    @IBAction func submitReviewForDoctor(_ sender: UIButton) {
        view.endEditing(true)
        guard let commentForDoctor:String = (patientCmntTxtView.text).trim, commentForDoctor.count > 0  else {
         self.showAlertWith(title: "", message: "Please enter the comment", handler: nil)
         return
        }
        guard let doctorRating : Float = Float(doctorRatingView.value), doctorRating > 0 else {
         self.showAlertWith(title: "", message: "Please select the Rating", handler: nil)
         return
        }
       if Reachability.isConnectedToNetwork(){
        if AppUser.patient != nil{
            self.activityIndicator.show(allowInteraction: false)
            Patient.submitFeedbackDetail(patientComment: commentForDoctor, ratingForDoctor: doctorRating, with: appointmentId!, completion:{[unowned self] (success, Feedback) in
                self.activityIndicator.hide()
                if success{
                    DispatchQueue.main.async {
                        self.showAlertWith(title: "", message: "Your messsage has been successfully submitted", handler: { (action) in
                            self.navigationController?.popViewController(animated: true)
                        })
                 }
              }
            }){[unowned self] (message) in
   self.activityIndicator.hide()
                self.showAlertWith(message: message, title: "")
            }
         }else{
           self.showAlertWith(message: ErrorMessages.kPateintNotFound, title:  "")
         }
        }else{
         self.showAlertWith(message: ErrorMessages.kNetworkError, title: "")
       }
    }
}

extension PatientsReviewViewController : UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text.count > 0{
            placeHolderLbl.isHidden=true
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
           placeHolderLbl.isHidden=false
        }
    }
}

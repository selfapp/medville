//
//  SearchDoctorViewController.swift
//  Medville
//
//  Created by Jitendra Solanki on 9/28/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

class SearchDoctorViewController: ViewController {

    
    //MARK:- Outlets
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var txtF_search: UITextField!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initializeUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //txtF_search.addShadowWith(opacity: 1.0, radius: 3.0, offset: CGSize(width:2,height:2), color: UIColor(r:242,g:244,b:246,alpha:1.0))
        addShadowOnSearchField()
    }
    //MARK:- Private helper methods
    private func initializeUI(){
        self.navigationItem.title = "Search Doctor"
        
        txtF_search.addLeftViewOf(width: 10)
//        let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: txtF_search.frame.size.height))
//        txtF_search.leftView = leftView
//        txtF_search.leftViewMode = .always
        
        //txtF_search.layer.cornerRadius =
    }
    private func addShadowOnSearchField(){
        
        //add shadow and border to view_profileData
        txtF_search.layer.cornerRadius = 5.0
        txtF_search.layer.masksToBounds = true
        
        searchView.layer.shadowColor = UIColor(r:229,g:231,b:231,alpha:1.0).cgColor
       // txtF_search.layer.shadowColor =  UIColor
        searchView.layer.shadowOffset = CGSize(width: 2, height: 2)
        searchView.layer.shadowRadius = 10.0
        searchView.layer.shadowOpacity = 1
        searchView.layer.shadowPath = UIBezierPath(rect: searchView.bounds).cgPath
       // txtF_search.layer.shouldRasterize = true
        
    }
    
    //MARK: UI Baseed Method
    func startIndicator() {
        self.activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    func stopIndicator() {
        DispatchQueue.main.async() {
            if UIApplication.shared.isIgnoringInteractionEvents{
                UIApplication.shared.endIgnoringInteractionEvents()
            }
            self.activityIndicator.stopAnimating()
        }
    }
   
    //MARK:- Action methods
    @IBAction func btnActn_search(_ sender: UIButton) {
        let getValidDoctorNameStr = self.txtF_search.text?.replacingOccurrences(of: " ", with: "")
        guard let searchDoctorStr = getValidDoctorNameStr, searchDoctorStr.characters.count > 0 else {
            self.showAlertWith(title: "", message: "Please enter a Doctor specialization.", handler: nil)
            return
        }
        
        self.view.endEditing(true)
        
//        if Reachability.isConnectedToNetwork(){
//            activityIndicator.show(allowInteraction: false)
//            Patient.searchDoctorForPatient(specialization: searchDoctorStr, with: "1", hotDoc: true, completion: {[unowned self] (success, doctorList) in
//                self.activityIndicator.hide()
//                if success{
//                    DispatchQueue.main.async {
////                        if doctorList.count>0 {
////                         self.navigateToDoctorProfileController(doctorList:doctorList)
////                        }else{
////                           self.showAlertWith(message: "No Doctor found", title: "")
////                        }
//                    }
//                }
//            }){[unowned self] (message) in
//                self.activityIndicator.hide()
//                self.showAlertWith(message: message, title: "")
//            }
//
//        }else{
//            self.showAlertWith(message: ErrorMessages.kNetworkError, title: "")
//        }
    }
    
    func navigateToDoctorProfileController(doctorList:[Doctor]){
        let doctorListVC = viewControllerFor(identifier: VCIdentifier.kDoctorListVC) as! DoctorListViewController
        doctorListVC.allDoctorList = doctorList
        self.navigationController?.pushViewController(doctorListVC, animated: true)
    }
}

extension SearchDoctorViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

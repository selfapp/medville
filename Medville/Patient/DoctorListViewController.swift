//
//  DoctorListViewController.swift
//  Medville
//
//  Created by Jitendra Solanki on 7/10/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

class DoctorListViewController: UIViewController {
    //MARK:- Outlets
    @IBOutlet weak var maintableView: UITableView!
    @IBOutlet weak var doctorSearchBar: UISearchBar!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    //MARK: iVar
    var isSearchedData = false
    let cellIdentifier =  "DoctorListTableViewCellIdentifier"
    var allDoctorList:[Doctor]!
    var searchedDoctorList:[Doctor] = []
    var isLazyLoading = false
    var doctorListObj : DoctorList?
    
    //MARK:- Pull to refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(DoctorListViewController.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.black
        refreshControl.attributedTitle = NSAttributedString(string: "Pull-to-Refresh")

        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        getDoctorList(hotDoc: true, specialization: "", pageNo: 1)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeUI()
        tableViewUIRelated()
        activityIndicator.show(allowInteraction: false)
        getDoctorList(hotDoc: true, specialization: "ort", pageNo: 1)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Private helper methods
    private func initializeUI(){
        self.navigationItem.title = "Doctors List"
       // let button =  UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(backBtnTapped))
       // self.navigationItem.leftBarButtonItem = button
        for childView in doctorSearchBar.subviews {
            for viewObj in childView.subviews {
                if let textField = viewObj as? UITextField {
                    textField.backgroundColor = UIColor(r:94, g:120, b:208, alpha: 1.0)
                    textField.textColor = UIColor.white
                    let searchIconView = textField.leftView as! UIImageView
                    searchIconView.image = searchIconView.image?.withRenderingMode(.alwaysTemplate)
                    searchIconView.tintColor = UIColor(r:150, g:170, b:240, alpha: 1.0)
                    
                    let placeholderLbl = textField.value(forKey: "placeholderLabel") as? UILabel
                    placeholderLbl?.textColor = UIColor(r:150, g:170, b:240, alpha: 1.0)
                    
                    textField.addInputAccessoryViewWith(target: self, selector: #selector(doneBtnTappedOn_Keyboard))
                    //textField.delegate = self
                }
            }
        }
        doctorSearchBar.delegate = self
    }
    
    func tableViewUIRelated(){
        let nib = UINib(nibName: "DoctorListTableViewCell", bundle: Bundle.main)
        maintableView.register(nib, forCellReuseIdentifier: cellIdentifier)
        maintableView.contentInset = UIEdgeInsetsMake(8, 0, 8, 0)
        self.maintableView.addSubview(self.refreshControl)

    }
    //MARK:- Get HotDoctors
    func getDoctorList(hotDoc:Bool,specialization:String,pageNo:Int) {
        
        if Reachability.isConnectedToNetwork(){
            Patient.searchDoctorForPatient(specialization: specialization, with: pageNo, hotDoc: hotDoc, completion: {[unowned self] (success, doctorList) in
                self.activityIndicator.hide()
                if (self.refreshControl.isRefreshing)
                {
                    self.refreshControl.endRefreshing()
                }
                if success{
                    DispatchQueue.main.async {
                        self.doctorListObj = doctorList
                        var listCount = 0
                        listCount = (doctorList?.doctors?.count)!
                        if listCount > 0 {
                            if self.isLazyLoading{
                                self.lazyLoadingData(doctorData: (doctorList?.doctors)!)
                          }else{
                                if(self.allDoctorList != nil){
                                    self.allDoctorList.removeAll()
                                }
                                self.allDoctorList = doctorList?.doctors
                            self.maintableView.reloadData()
                            }
                        }else{
                            self.showAlertWith(message: "No Doctor found", title: "")
                        }
                    }
                }
            }){[unowned self] (message) in
                self.activityIndicator.hide()
                if (self.refreshControl.isRefreshing)
                {
                    self.refreshControl.endRefreshing()
                }
                self.showAlertWith(message: message, title: "")
            }

        }else{
            self.showAlertWith(message: ErrorMessages.kNetworkError, title: "")
        }
    }
    
    func lazyLoadingData(doctorData:[Doctor]) {
        allDoctorList.append(contentsOf: doctorData)
        maintableView.reloadData()
    }
    
    //MARK:- Custom Action and IBAction Based methods
    @objc private func backBtnTapped(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc private func doneBtnTappedOn_Keyboard(){
        view.endEditing(true)
        isLazyLoading = false
        isSearchedData = true
        activityIndicator.show(allowInteraction: false)
        getDoctorList(hotDoc: false, specialization: doctorSearchBar.text!, pageNo: 1)
    }
    
}
//MARK:- TableView Delegate And Data Source
extension DoctorListViewController: UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        if allDoctorList != nil {
            maintableView.separatorStyle = .singleLine
            maintableView.backgroundView?.isHidden = true
            return 1
        }else{
           let message = UILabel()
            message.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height)
            message.text = "Loading.."
            message.numberOfLines = 0
            message.sizeToFit()
            message.textAlignment = .center
            maintableView.backgroundView = message
            maintableView.separatorStyle = .none
            maintableView.backgroundView?.isHidden = false
            
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return isSearchedData ? searchedDoctorList.count : allDoctorList.count
        if allDoctorList != nil {
            return allDoctorList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)as! DoctorListTableViewCell
        //let doctor =  isSearchedData ? searchedDoctorList[indexPath.row] : allDoctorList[indexPath.row]
        cell.selectionStyle = .none
        cell.showDoctorSpecificData(doctor: allDoctorList[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let doctorProfile = viewControllerFor(identifier: VCIdentifier.kDoctorProfileDetails) as! DoctorProfileDetailViewController
       doctorProfile.doctorDetailObj = allDoctorList[indexPath.row]
        doctorProfile.doctorProfileType = DoctorProfile.kDoctorDetail
     self.navigationController?.pushViewController(doctorProfile, animated:true)
    }
    
   // For Lazy Loading
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let translation = scrollView.panGestureRecognizer.translation(in: scrollView.superview)
        if translation.y <= 0 {
            let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
            if bottomEdge >= (scrollView.contentSize.height - 200)
            {
                if !(doctorListObj?.is_last_page)!{
                isLazyLoading = true
                if isSearchedData{
                    activityIndicator.show(allowInteraction: false)
                    getDoctorList(hotDoc: false, specialization: doctorSearchBar.text!, pageNo: (doctorListObj?.current_page)! + 1)
                }else{
                    activityIndicator.show(allowInteraction: false)
                getDoctorList(hotDoc: true, specialization: "", pageNo: (doctorListObj?.current_page)! + 1)
                }
                }
            }
        }

    }
}

extension DoctorListViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        isLazyLoading = false
        isSearchedData = true
        activityIndicator.show(allowInteraction: false)
        getDoctorList(hotDoc: false, specialization: doctorSearchBar.text!, pageNo: 1)
        return true
    }
    
}

//MARK:- Search Bar Delegate And Data Source
extension DoctorListViewController: UISearchBarDelegate{
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
      //  searchBar.showsCancelButton = true
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
       // searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//        if isSearchedData{
//            isSearchedData = false
//            maintableView.reloadData()
//        }
//        searchedDoctorList.removeAll()
        searchBar.text = nil
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        if searchText.characters.count != 0{
//            searchedDoctorList = allDoctorList.filter({ (doctor) -> Bool in
//                return doctor.first_name.lowercased().contains(searchText.lowercased()) || doctor.specialization.contains(searchText.lowercased())
//            })
//            isSearchedData = true
//            maintableView.reloadData()
//        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return true
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        isLazyLoading = false
        isSearchedData = true
        activityIndicator.show(allowInteraction: false)
        getDoctorList(hotDoc: false, specialization: doctorSearchBar.text!, pageNo: 1)
    }

}


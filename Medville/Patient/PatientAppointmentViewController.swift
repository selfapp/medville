//
//  PatientAppointmentViewController.swift
//  Medville
//
//  Created by Jitendra Solanki on 9/28/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

class PatientAppointmentViewController: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var segmentView: JKSegmentView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tableViewCompleted: UITableView!
    @IBOutlet weak var tableviewUpcoming: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    //MARK:- iVar
    //let upComingCell = "upcomingPatientCellIdentifier"
   // let completedCell = "completedPatientCellIdentifier"
    let cellIdentifer = "AppointmentCell"
    var arr_upcomingAppointments:[Appointment] = []
    var arr_completedAppointments:[Appointment] = []
   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initializeUI()
        configureTableView()
        getAppointmentsForPatients()
    }
    override func viewDidAppear(_ animated: Bool) {
        getAppointmentsForPatients()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Private helper methods
    private func initializeUI(){
        self.navigationItem.title = "Appointments"
        //customize  segment view
        segmentView.setupViewWithSegments(segments: ["Upcoming","Completed"])
        segmentView.delegate = self
    }
    private func configureTableView(){
        let nib = UINib(nibName: "AppointmentCell", bundle: Bundle.main)
        tableviewUpcoming.register(nib, forCellReuseIdentifier: cellIdentifer)
        tableViewCompleted.register(nib, forCellReuseIdentifier: cellIdentifer)
        tableViewCompleted.tableFooterView = UIView(frame: CGRect.zero)
        tableviewUpcoming.tableFooterView = UIView(frame: CGRect.zero)
        tableviewUpcoming.delegate = self
        tableViewCompleted.delegate = self
        
    }
    
    private func getAppointmentsForPatients(){
        if Reachability.isConnectedToNetwork(){
            if let patient = AppUser.patient{
                spinner.show(allowInteraction: false)
                Patient.getAppointmentsForPatients(id: patient.id, {[weak self] (upcoming, completed) in
                    self?.spinner.hide()
                    self?.arr_upcomingAppointments.removeAll()
                    self?.arr_completedAppointments.removeAll()
    
                    self?.arr_upcomingAppointments = upcoming
                    self?.arr_completedAppointments = completed
                    if ((self?.arr_upcomingAppointments.count)! < 1) && ((self?.arr_completedAppointments.count)! < 1) {
                      self?.showAlertWith(message:"No record found", title:  "")
                    }else{
                        DispatchQueue.main.async {
                            (self?.segmentView.selectedIndex == 0) ? self?.tableviewUpcoming.reloadData() :self?.tableViewCompleted.reloadData()
                        }
                     }
                    }, failure: {[weak self] (message) in
                        self?.spinner.hide()
                        self?.showAlertWith(message: message, title: "")
                })
            }else{
                self.showAlertWith(message: ErrorMessages.kDoctorNotFound, title:  "")
            }
        }else{
            self.showAlertWith(message:ErrorMessages.kNetworkError, title: "")
        }
    }
    
    // change date format
     func convertStringToDate(dateStr:String) ->String{
       
            let dateFormat = DateFormatter()
            dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            
            let date = dateFormat.date(from: dateStr)
        dateFormat.dateFormat =  "dd MMM yyyy HH:mm a"
        dateFormat.timeZone = NSTimeZone(name: "GMT") as TimeZone!

            let  newDate =  dateFormat.string(from: date!)
            return newDate
        
    }
    
}

extension PatientAppointmentViewController:JKSegmentViewDelegate{
    func didSelectSegmentAt(index segmentIndex: Int, view segmentView: JKSegmentView) {
        var x:CGFloat = 0
        if segmentIndex == 0{
            self.tableviewUpcoming.reloadData()
        }else{
            x = UIScreen.main.bounds.size.width
            self.tableViewCompleted.reloadData()
        }
        UIView.animate(withDuration: 0.3) {
            self.scrollView.contentOffset.x = x
        }
    }
}

extension PatientAppointmentViewController:UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.segmentView.selectedIndex == 0 ? arr_upcomingAppointments.count :   arr_completedAppointments.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.backgroundColor = indexPath.row % 2 == 0 ? UIColor.white : UIColor(r:241,g:241,b:241,alpha:1.0)
        cell.accessoryType = .disclosureIndicator
        cell.selectionStyle = .none
        cell.textLabel?.textColor =  UIColor(r:32,g:32,b:32,alpha:1.0)
        cell.textLabel?.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.bold)
        cell.detailTextLabel?.textColor = UIColor(r:150,g:150,b:150,alpha:1.0)
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView == self.tableviewUpcoming ? tableView.dequeueReusableCell(withIdentifier: cellIdentifer, for: indexPath) as! AppointmentCell : tableView.dequeueReusableCell(withIdentifier: cellIdentifer, for: indexPath) as! AppointmentCell
        
        //set data for cell
        let appointment:Appointment  = tableView == self.tableviewUpcoming ? arr_upcomingAppointments[indexPath.row] : arr_completedAppointments[indexPath.row]
       // cell.imageView?.image = #imageLiteral(resourceName: "defualt-profile")
        cell.avtorImage?.sd_setImage(with: appointment.doctor?.avatarPath, placeholderImage: UIImage(named: "defualt-profile"))
        cell.nameLabel?.text = appointment.doctor?.first_name.capitalized
        cell.timeDetailLabel?.text = appointment.start_time.convertDateStringFrom(currentFormat: DateFormat.kCurrentFormat.rawValue, toRequiredFormat: DateFormat.kRequireFormat.rawValue)
        
            cell.appointmentStatus.text = appointment.state.capitalized
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let appointment:Appointment  = tableView == self.tableviewUpcoming ? arr_upcomingAppointments[indexPath.row] : arr_completedAppointments[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: false)
        let doctorProfile = viewControllerFor(identifier: VCIdentifier.kDoctorProfileDetails) as! DoctorProfileDetailViewController
        doctorProfile.doctorDetailObj = appointment.doctor
        doctorProfile.appointment = appointment
        doctorProfile.doctorProfileType = tableView == self.tableviewUpcoming ? DoctorProfile.kUpcomingAppointment : DoctorProfile.kCompletedAppointment
        self.navigationController?.pushViewController(doctorProfile, animated:true)
        
        
//        let patientDetails = viewControllerFor(identifier: VCIdentifier.kPatientDetails)as! PatientDetailsViewController
//
//        patientDetails.patient =
//            tableView == self.tableV_upcoming ? arr_upcomingAppointments[indexPath.row].patient : arr_completedAppointments[indexPath.row].patient
//        self.navigationController?.pushViewController(patientDetails, animated: true)
    }
}

//
//  DoctorProfileDetailViewController.swift
//  Medville
//
//  Created by Jitendra Solanki on 10/5/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

class DoctorProfileDetailViewController: UIViewController {

    //MARK:- iVar
    let cardCellIdentifier = "savedCardCellIdentifier"
    let commentCell = "DoctorCommentCellIdentifier"
    var collapseHeightOfLabel : CGFloat = 0
    //MARK:- Outlets
    
    @IBOutlet weak var profileImagebaseView: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var doctorNameLbl: UILabel!
    @IBOutlet weak var doctorRatingLbl: UILabel!
    @IBOutlet weak var doctorStateLbl: UILabel!
    @IBOutlet weak var noOfCallBtn: UIButton!
    @IBOutlet weak var segmentView: UIView!
    
    @IBOutlet weak var bookButton: UIButton!
    @IBOutlet weak var contactButton: UIButton!
    
    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var doctorDescription: UILabel!
    @IBOutlet weak var doctorFullAddress: UILabel!
    @IBOutlet weak var doctorMobileNumber: UILabel!
    @IBOutlet weak var profileCommentsScrollView: UIScrollView!
    @IBOutlet weak var commentTableView: UITableView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var bottomCommentConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var bottomProfileViewConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var cmtTableViewHeightConstraint: NSLayoutConstraint!
    var doctorDetailObj : Doctor!
    var selectedCardIndex = 0
    var doctorProfileType:String?
    var appointment:Appointment?
    // Card Data
    var cardDetailArray : [Card] = []

    override func viewDidLoad() {
        
        super.viewDidLoad()
        initializeUI()
        configureCommentTableView()
        setDoctorDataInUI()
        collapseHeightOfLabel = doctorDescription.frame.size.height
        heightOfDoctorDescriptionTextLbl(isExpand: true)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Private helper methods
    private func initializeUI(){
        self.navigationItem.title = "Profile"
        let button =  UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(backBtnTapped))
        self.navigationItem.leftBarButtonItem = button
        edgesForExtendedLayout = []
        
        profileImagebaseView.addBorderWith(borderWidth: 0.5, withBorderColor:UIColor(red:110.0/255.0, green: 140.0/255.0, blue:240.0/255.0, alpha: 0.2 ), withCornerRadious:profileImagebaseView.frame.size.width/2)
        self.view.bringSubview(toFront:profileImagebaseView)
        profileImage.makeCornerRound(cornerRadius:profileImage.frame.size.width/2)
        segmentController.layer.cornerRadius = segmentController.frame.size.height/2;
        segmentController.layer.borderColor = UIColor.white.cgColor;
        segmentController.layer.borderWidth = 1.0;
        segmentController.layer.masksToBounds = true;
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tabBarController?.tabBar.backgroundColor = UIColor.white
    }
    override func viewDidLayoutSubviews() {
        segmentView.roundCorners([.bottomLeft, .bottomRight], radius: 6)
    }
    
    private func configureCommentTableView(){
        commentTableView.tableFooterView = UIView(frame: CGRect.zero)
        commentTableView.delegate = self
        let nib = UINib(nibName: "DoctorProfileCommentCell", bundle: Bundle.main)
        commentTableView.register(nib, forCellReuseIdentifier: commentCell)
        commentTableView.estimatedRowHeight = 150
        commentTableView.rowHeight = UITableViewAutomaticDimension
    }
    
   
    private func setDoctorDataInUI(){
//        var countyCode:String?
//        var specialization:String
        //profileImage.image = #imageLiteral(resourceName: "defualt-profile")
        profileImage.sd_setImage(with: doctorDetailObj.avatarPath, placeholderImage: UIImage(named: "defualt-profile"))

        doctorNameLbl.text = doctorDetailObj.first_name
        //if let getRating = doctorDetailObj.rating{}
        doctorRatingLbl.text = String(doctorDetailObj.rating)
        doctorDescription.text = doctorDetailObj.description
        doctorFullAddress.text = doctorDetailObj.address
        doctorMobileNumber.text = doctorDetailObj.phoneNumber

        let addressArray = doctorDetailObj.address.components(separatedBy: ",")
        if addressArray.count > 1 {
            doctorStateLbl.text = addressArray[1]
        }else if addressArray.count > 0 {
            doctorStateLbl.text = addressArray.last
        }else{
            doctorStateLbl.text = doctorDetailObj.address
        }
        
        switch doctorProfileType {
        case DoctorProfile.kUpcomingAppointment?:
            if appointment?.state == "confirmed"{
            bookButton.setTitle("Cancel \nAppointment", for: .normal)
                bookButton.isHidden = false
            }else{
                bookButton.isHidden = true
            }
            bookButton.setImage(nil, for: .normal)
            bookButton.setTitleColor(UIColor.red, for: .normal)
            break
        case DoctorProfile.kCompletedAppointment?:
            contactButton.isHidden = true
            bookButton.setTitle("Feedback", for: .normal)
            bookButton.setImage(nil, for: .normal)
            bookButton.setTitleColor(UIColor.init(r: 24, g: 186, b: 88, alpha: 1), for: .normal)
            break
        case DoctorProfile.kDoctorDetail?:
            contactButton.isHidden = true
            getPatientAllCardrelatedDetail()

            break
            
        default:
            break
        }
        
    }
    
  private func heightOfDoctorDescriptionTextLbl(isExpand:Bool){ //for less-false, expand-true
    let heightofLbl = doctorDescription.text?.height(withConstrainedWidth: doctorDescription.frame.size.width, font:UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.regular))
    let finalHeight = isExpand ? (heightofLbl ?? 0) : collapseHeightOfLabel
    UIView.animate(withDuration: 0.3) {
        self.doctorDescription.translatesAutoresizingMaskIntoConstraints = false
        self.doctorDescription.numberOfLines = isExpand ? 0 : 3;
        self.doctorDescription.heightAnchor.constraint(equalToConstant: finalHeight).isActive = true
    }
  }
    
//MARK:- Custom Action and IBAction Based methods
    @objc private func backBtnTapped(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func selectProfileOrCommentsSegment(_ sender: UISegmentedControl) {
        let x = sender.selectedSegmentIndex == 0 ? 0 : UIScreen.main.bounds.size.width
        UIView.animate(withDuration: 0.3) {
            self.profileCommentsScrollView.contentOffset.x = x
        }
        
//        if sender.selectedSegmentIndex == 1 {
//             heightOfDoctorDescriptionTextLbl(isExpand: false)
//        }
    }
    @IBAction func patientBookAppointment(_ sender: Any) {
     
        switch doctorProfileType {
        case DoctorProfile.kUpcomingAppointment?:
            showAlertWith(message: "Are you sure ?", title: "Alert", okCompletion: { (action) in
//                print("ok action")
                self.cancelAppointment()
            }, cancelCompletion: { (action) in
//                print("Cancel action")

            })
            break
        case DoctorProfile.kCompletedAppointment?:
             let vc = PatientsReviewViewController(nibName: "PatientsReviewViewController", bundle: nil)
             vc.appointmentId = self.appointment?.id
                self.navigationController!.pushViewController(vc, animated: true)
            break
        case DoctorProfile.kDoctorDetail?:
            if cardDetailArray.count > 0{
            showActionSheetForCardList()
            }else{
                let addCard = self.viewControllerFor(identifier: VCIdentifier.kAddCard)
                self.navigationController?.pushViewController(addCard, animated:true)
            }
            break
            
        default:
            break
        }
    }
   
//Start chat
    @IBAction func patientContactToDoctor(_ sender: Any) {
        activityIndicator.show(allowInteraction: false)
        let doctorUUid = doctorDetailObj.uuid
        let patientUUid:String = (AppUser.patient?.uuid)!
        let channelName = "\(doctorUUid)/\(patientUUid)"
        let doctofName:String = doctorDetailObj.first_name
        let patientFname:String = (AppUser.patient?.firstName)!
        let friendlyName = "\(doctofName)/\(patientFname)"
        
        ChatManager._sharedManager.isChannelExist(channelName: channelName) { (oldChannel) in
            if oldChannel != nil{
                self.activityIndicator.hide()
                ChatManager._sharedManager.addUserinChannel(userName: doctorUUid, channel: oldChannel!)
                if let chatVC  = self.viewControllerFor(identifier: VCIdentifier.kChatViewController)as? ChatViewController{
                    chatVC.channel = oldChannel!
                    chatVC.chatUserName = doctofName
                    chatVC.chatUserIdentity = doctorUUid
                    chatVC.currentIdentity = patientUUid
                    chatVC.currentUserName = patientFname
                    self.navigationController?.pushViewController(chatVC, animated: true)
                    
                }
            }else{
                ChatManager._sharedManager.createChannel(chanelName: channelName, friendlyName: friendlyName, completion: { (newChannel) in
                    ChatManager._sharedManager.addUserinChannel(userName: doctorUUid, channel: newChannel)
                    self.activityIndicator.hide()
                    if let chatVC  = self.viewControllerFor(identifier: VCIdentifier.kChatViewController)as? ChatViewController{
                        chatVC.channel = newChannel
                        chatVC.chatUserName = doctofName
                        chatVC.chatUserIdentity = doctorUUid
                        chatVC.currentIdentity = patientUUid
                        chatVC.currentUserName = patientFname
                        self.navigationController?.pushViewController(chatVC, animated: true)
                    }
                })
            }
        }
        
    }
    
    
    
//action sheet
    private func showActionSheetForCardList(){
        let optionMenu = UIAlertController(title: "Book @ $" + String(doctorDetailObj.appointment_fee), message: "Choose a card", preferredStyle: .actionSheet)

        for card in cardDetailArray {
            
            let deleteAction = UIAlertAction(title: "XXXX XXXX XXXX "+card.last4, style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                //
//                print(card.last4)
                self.submtSelectedCardDetails(card: card)
            })
            optionMenu.addAction(deleteAction)
        }

        let saveAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
//
        })
        
        optionMenu.addAction(saveAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
//Show time slot in webview
    
    private func moveToWebView(appointment_html:String){
        let bookAppointment = viewControllerFor(identifier: VCIdentifier.kBookAppointment) as! BookAppointmentVC
        bookAppointment.appointment_html = appointment_html
        self.navigationController?.pushViewController(bookAppointment, animated:true)
        
    }
    //Call api to cancel appointment
    private func cancelAppointment(){
        let appointmet:[String:Int] = ["id":(appointment?.id)!]
        
        if Reachability.isConnectedToNetwork(){
            activityIndicator.show(allowInteraction: false)
            makePutHttpRequestWith(url: "/appointments/cancel", dataDict: appointmet, successBlock: { (success, response, statusCode) in
                self.activityIndicator.hide()
                if statusCode == 200{
                    self.showAlertWith(title: "", message: "Appointment cancelled", handler: { (action) in
                        self.backBtnTapped()
                    })
                }
            }) { (message, statusCode) in
               // failure(message)
            }
        }else{
            self.showAlertWith(message: ErrorMessages.kNetworkError, title: "")
        }
    }
    
// Get all card details that are saved
    private func getPatientAllCardrelatedDetail(){
        if let patient = AppUser.patient{
            if Reachability.isConnectedToNetwork(){
                activityIndicator.show(allowInteraction: false)
                Card.getAllCartDetailForPatient(id: patient.id, completion:{[unowned self] (success,arrCardDetail) in
                    self.activityIndicator.hide()
                    if success{
                        if arrCardDetail.count > 0 {
                            self.cardDetailArray = arrCardDetail
                        }
                    }
                }) {[unowned self] (message) in
                    self.activityIndicator.hide()
                    self.showAlertWith(message: message, title: "")
                }
            }else{
                self.showAlertWith(message: ErrorMessages.kNetworkError, title: "")
            }
        }else{
            self.showAlertWith(message: ErrorMessages.kPateintNotFound, title:  "")
        }
    }
    
    //MARK:- Submit selected card details
    private func submtSelectedCardDetails(card : Card){
        
        if Reachability.isConnectedToNetwork(){
            activityIndicator.show(allowInteraction: false)
            let url = "/doctors/\(doctorDetailObj.id)/appointments/new?payment_source_id=" + String(card.id)

            makeGetHttpRequestWith(url: url, dataDict: nil, successBlock: { (success, result, statusCode) in
                self.activityIndicator.hide()
                if let dictResult = result as? [String:Any],
                let appointment_html = dictResult["appointment_html"]as? String{
                    self.moveToWebView(appointment_html: appointment_html)
                }else{
                    
                }
                
            }) { (message, statusCode) in
                self.activityIndicator.hide()
                self.showAlertWith(message: message, title:  "")
            }
            }
        }
    }


extension DoctorProfileDetailViewController:UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.selectionStyle = .none
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension;
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numberOfRow:Int = (doctorDetailObj.feedbacks?.count)!
        return numberOfRow
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: commentCell, for: indexPath)as! DoctorProfileCommentCell
        cell.showPatientCommentData(feedBack: doctorDetailObj.feedbacks![indexPath.row])
        return cell
    }
    
    
    
}



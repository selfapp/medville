//
//  PatientSettingViewController.swift
//  Medville
//
//  Created by Jitendra Solanki on 9/28/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit
import Stripe
import SDWebImage
import FSPicker

class PatientSettingViewController: ViewController {
    
    //MARK:- outlets
    @IBOutlet weak var segmentView: JKSegmentView!
    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    //MARK:- Profile outlets
    
    @IBOutlet weak var view_profileData: UIView!
    @IBOutlet weak var imgV_profilePic: UIImageView!
    @IBOutlet weak var circleViewForImage: UIView!
   
    @IBOutlet weak var txtF_name: GRFloatingTextField!
    
    @IBOutlet weak var txtF_age: GRFloatingTextField!
    
    @IBOutlet weak var txtF_bloodGroup: GRFloatingTextField!
    
    @IBOutlet weak var txtF_weight: GRFloatingTextField!
    
    @IBOutlet weak var txtF_height: GRFloatingTextField!
    
    
    
    @IBOutlet weak var txtF_attachDoc: UITextField!
    
    @IBOutlet weak var attachedDoc_CollectionView: UICollectionView!
    //MARK:- Payment outlets
    @IBOutlet weak var addCartActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var lbl_cardDescription: UILabel!
    
    @IBOutlet weak var tf_cardNumber: GRFloatingTextField!
    
    @IBOutlet weak var tf_cardExpiryDate: GRFloatingTextField!
    
    @IBOutlet weak var tf_cardCVV: GRFloatingTextField!
    
    @IBOutlet weak var tableV_cardList: UITableView!
    @IBOutlet weak var constaint_tableV_height: NSLayoutConstraint!
    
    @IBOutlet weak var saveBtn: UIButton!
    
    @IBOutlet weak var addCardBtn: UIButton!
    lazy var datePicker:UIDatePicker  = {
        let picker = UIDatePicker()
        picker.datePickerMode =  UIDatePickerMode.date
        picker.minimumDate = self.setMinimumDate()
        picker.addTarget(self, action: #selector(didValueChangedIn(picker:)), for: .valueChanged)
        return picker
    }()
    
    //MARK:- iVar
    
    var userImage:UIImage!
    let cardCellIdentifier = "savedCardCellIdentifier"
    var cardDetailArray : [Card] = []
    var attachmentArray : [UIImage] = []
    var patient : Patient?
    var deleteIndex:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initializeUI()
        configureCardTableView()
        showUserData()
        getPatientAllCardrelatedDetail()
         self.attachedDoc_CollectionView.register(UINib(nibName: "AttachmentCell", bundle: nil), forCellWithReuseIdentifier: "AttachmentCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //addShadowOnProfileView
    
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        restPatientDetails()
    }
    
    func restPatientDetails(){
        txtF_name.resetText(txt: (patient?.firstName)!)
        if let age = patient?.age{
        txtF_age.resetText(txt: String(age))
        }
        if let height = patient?.height{
        txtF_height.resetText(txt: String(height))
        }
        if let weight = patient?.weight{
        txtF_weight.resetText(txt: String(weight))
        }
        if let bloodGroup = patient?.bloodGroup {
            txtF_bloodGroup.resetText(txt: bloodGroup)
        }
        self.attachedDoc_CollectionView.reloadData()

    }
    
    //MARK:- Private helper methods
    private func initializeUI(){
        self.navigationItem.title = "Settings"
        
        //add bar button
        let barBtn = UIBarButtonItem.init(image: #imageLiteral(resourceName: "logout"), style: .plain, target: self, action: #selector(logOutBtn_tapped))
        self.navigationItem.rightBarButtonItem = barBtn
        tf_cardNumber.tag = 555
        tf_cardCVV.tag = 555
        tf_cardExpiryDate.tag = 555
        imgV_profilePic.addBorderWith(borderWidth: 1.0, withBorderColor: UIColor.white, withCornerRadious: imgV_profilePic.frame.size.height/2)
        txtF_name.addInputAccessoryViewWith(target: self, selector: #selector(doneBtnTappedOn_Keyboard))
        txtF_age.addInputAccessoryViewWith(target: self, selector: #selector(doneBtnTappedOn_Keyboard))
        txtF_height.addInputAccessoryViewWith(target: self, selector: #selector(doneBtnTappedOn_Keyboard))
        txtF_weight.addInputAccessoryViewWith(target: self, selector: #selector(doneBtnTappedOn_Keyboard))
        txtF_bloodGroup.addInputAccessoryViewWith(target: self, selector: #selector(doneBtnTappedOn_Keyboard))

        
        
        //imgV_profilePic.layer.cornerRadius = imgV_profilePic.frame.size.height/2
        //imgV_profilePic.layer.masksToBounds = true
        //customize  segment view
        segmentView.setupViewWithSegments(segments: ["Profile","Payment"])
        segmentView.delegate = self
        
        self.viewDidLayoutSubviews()
        //round imageview
        circleViewForImage.addBorderWith(borderWidth: 0, withBorderColor: .clear, withCornerRadious: circleViewForImage.frame.size.width/2)
         //to stop scolling in either side when scrolling other side
        scrollView.isDirectionalLockEnabled = true
       
        tf_cardNumber.addInputAccessoryViewWith(target: self, selector: #selector(doneBtnTappedOn_Keyboard))
        tf_cardNumber.delegate = self
        tf_cardCVV.delegate = self
        tf_cardCVV.addInputAccessoryViewWith(target: self, selector: #selector(doneBtnTappedOn_Keyboard))
        tf_cardExpiryDate.addInputAccessoryViewWith(target: self, selector: #selector(doneBtnTappedOn_Keyboard))
        //tf_cardExpiryDate.inputView = datePicker
        let expiryDatePicker = MonthYearPickerView()
        tf_cardExpiryDate.inputView = expiryDatePicker
        expiryDatePicker.onDateSelected = { (month: Int, year: Int) in
            let string = String(format: "%02d/%d", month, year)
            self.tf_cardExpiryDate.place_holder = ""
            self.tf_cardExpiryDate.text = string
            NSLog(string) // should show something like 05/2015
        }
        //activityIndicator.translatesAutoresizingMaskIntoConstraints = false
//        activityIndicator.heightAnchor.constraint(equalToConstant: 37).isActive = true
//        activityIndicator.widthAnchor.constraint(equalToConstant: 37).isActive = true
        
        imgV_profilePic.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOn_imageView(sender:))))
        imgV_profilePic.isUserInteractionEnabled = true
        
    }
    
    private func configureCardTableView(){
        let nib = UINib(nibName: "CardTableViewCell", bundle: Bundle.main)
        tableV_cardList.register(nib, forCellReuseIdentifier: cardCellIdentifier)
        tableV_cardList.tableFooterView = UIView()
        
        //set height constriants for cardTableView
        constaint_tableV_height.constant = CGFloat(min(60*3, 60 * cardDetailArray.count))
        
    }
    private func setMinimumDate()-> Date?{
        var components = DateComponents()
        components.day = 0
        let minDate = Calendar.current.date(byAdding: components, to: Date())
        return minDate
    }
    
    @objc private func didTapOn_imageView(sender:UITapGestureRecognizer){
        openImagePicker()
        //self.openImagePicker()
    }
    
    @objc private func didValueChangedIn(picker:UIDatePicker!){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/YY"
        tf_cardExpiryDate.text = dateFormatter.string(from: datePicker.date)
        tf_cardExpiryDate.textFieldDidChange(notification: Notification.init(name: .UITextFieldTextDidChange) as NSNotification)
        //NotificationCenter.default.post(Notification.init(name: .UITextFieldTextDidChange))
    }
    
//    @objc private func didTapOn_imageView(sender:UITapGestureRecognizer){
//        openImagePicker()
//    }
    
    private func showUserData(){
         patient = AppUser.patient
        txtF_name.text = patient?.firstName
        if let age = patient?.age{
            txtF_age.text = String(age)
        }
        txtF_bloodGroup.text = patient?.bloodGroup
        if let weight =  patient?.weight{
            txtF_weight.text = String(weight)
        }
        if let height =  patient?.height{
            txtF_height.text = String(height)
        }
        
        imgV_profilePic.image = #imageLiteral(resourceName: "defualt-profile")
        
        if let avatarPath = patient?.avatarPath {
            imgV_profilePic.sd_setImage(with:  avatarPath) { (image, error, cacheType, url) in
                
                
                if image != nil{
                    self.userImage = image
                }
            }
        }
    }
    //call api
    private func addCartApiForPatient(stipeToken:String) {
        if Reachability.isConnectedToNetwork(){
            if let patient = AppUser.patient{
                Patient.addPatientsCardDetail(stripeToken: stipeToken, patientId:patient.id, completion: {[unowned self] (succeed) in
                    self.addCartActivityIndicator.hide()
                    self.showAlertWith(message: "Card added successfully", title: "")
                    if succeed{
                        DispatchQueue.main.async {
                            //clear card fields
                            self.tf_cardExpiryDate.place_holder = "MM/YY"
                            self.tf_cardExpiryDate.text = ""
                            self.tf_cardCVV.text = ""
                            self.tf_cardNumber.text = ""
                           self.getPatientAllCardrelatedDetail()
                        }
                    }
                }) {[unowned self] (message) in
                    self.addCartActivityIndicator.hide()
                    self.showAlertWith(message: message, title: "")
                }
            }else{
                self.showAlertWith(message: ErrorMessages.kPateintNotFound, title:  "")
            }
        }
        else{
            self.showAlertWith(message: ErrorMessages.kNetworkError, title: "")
        }
    }
    
  private func getPatientAllCardrelatedDetail(){
    if let patient = AppUser.patient{
      if Reachability.isConnectedToNetwork(){
        activityIndicator.show(allowInteraction: true)
        Card.getAllCartDetailForPatient(id: patient.id, completion:{[unowned self] (success,arrCardDetail) in
        self.activityIndicator.hide()
        if success{
            if arrCardDetail.count > 0 {
                self.cardDetailArray.removeAll()
                self.cardDetailArray = arrCardDetail
                self.constaint_tableV_height.constant = CGFloat(min(60*3, 60 * self.cardDetailArray.count))
                self.tableV_cardList.reloadData()
            }
          }
        }) {[unowned self] (message) in
            self.activityIndicator.hide()
            self.showAlertWith(message: message, title: "")
        }
        }else{
        self.showAlertWith(message: ErrorMessages.kNetworkError, title: "")
        }
     }else{
        self.showAlertWith(message: ErrorMessages.kPateintNotFound, title:  "")
      }
    }
    
//    private func addShadowOnProfileView(){
//
//        //add shadow and border to view_profileData
//        view_profileData.layer.cornerRadius = 5.0
//
//        view_profileData.layer.shadowColor = UIColor(r:242,g:244,b:246,alpha:1.0).cgColor
//        view_profileData.layer.shadowOffset = CGSize(width: 2, height: 2)
//        view_profileData.layer.shadowRadius = 5.0
//        view_profileData.layer.shadowOpacity = 1
//        view_profileData.layer.shadowPath = UIBezierPath(rect: view_profileData.bounds).cgPath
//        //view_profileData.layer.shouldRasterize = true
//
//    }

    
    //MARK:- Action methods
    
    //log out
    @objc func logOutBtn_tapped(){
        
       // let patient = AppUser.patient
       // Patient.logoutPatient(id: (patient?.id)!, { (status) in
            UIApplication.shared.appDelegate.logoutForUser(type: .kPatient)
            
//        }) { (status) in
//            self.showAlertWith(message: "something went wrong try again later", title: "")
//        }
        
        //UIApplication.shared.appDelegate.logoutForUser(type: .kPatient)
    }
    
    @objc private func doneBtnTappedOn_Keyboard(){
        view.endEditing(true)
    }
    //Attach documents
    
    @IBAction func btnActn_attachDocument(_ sender: Any) {
       fsPickerSetup()
//        let caMediaManager = CAMediaManager.sharedMediaManager(withRootController: self)
//        caMediaManager?.mediaPickerSource = .photoTypeAlbum
//        caMediaManager?.mediaPickerDelegate = self
//        caMediaManager?.presentMediaPickerController(with: .photoTypeAlbum)
    }
    
    func fsPickerSetup(){
        let config = FSConfig.init(apiKey: "ANMiLnrhoTYWMhL8dRxEkz")
        let storeOptions = FSStoreOptions.init()
        storeOptions.location = FSStoreLocationS3
        config?.storeOptions = storeOptions
        let theme = FSTheme.init()
        let fsPickerController = FSPickerController.init(config: config, theme: theme)
        fsPickerController?.fsDelegate = self as! FSPickerDelegate
        self.present(fsPickerController!, animated: true) {
        }
        
    }
    
    
    @IBAction func savePatientData(_ sender: Any) {
        //var dict : [String:Any]
        
        guard let firstname = (txtF_name.text)?.trim,firstname.count > 0 else {
            self.showAlertWith(title: "", message: "Please enter a Patient Name.", handler: nil)
            return
        }
        guard let age = (txtF_age.text)?.trim, age.count > 0 else {
            self.showAlertWith(title: "", message: "Please enter a Patient Age.", handler: nil)
            return
        }
        guard let bloodGroup = (txtF_bloodGroup.text)?.trim, bloodGroup.count > 0 else {
            self.showAlertWith(title: "", message: "Please enter a Patient Blood group.", handler: nil)
            return
        }
        guard let weight = (txtF_weight.text)?.trim, weight.count > 0 else {
            self.showAlertWith(title: "", message: "Please enter a Patient Weight.", handler: nil)
            return
        }
        guard let height = (txtF_height.text)?.trim,height.count > 0 else {
            self.showAlertWith(title: "", message: "Please enter a Patient height.", handler: nil)
            return
        }
        
        var patientHash:[String:Any] = [
            "first_name" : firstname,
            "age" : age,
            "blood_group" : bloodGroup,
            "weight" : weight,
            "height" : height
           ]
        
        if let profileImage = userImage {
            let imgData = UIImageJPEGRepresentation(profileImage, 0.0)
            let base64 = "data:image/jpeg;base64," + (imgData?.base64EncodedString())!
            
            patientHash["avatar"] = base64
        }
                
        let dict:[String:Any] = ["patient": patientHash]
        
        guard let id = AppUser.patient?.id else{
//            print("User not found.")
            return
        }
        //call api
        
     if Reachability.isConnectedToNetwork(){
        activityIndicator.show(allowInteraction: false)
        Patient.setProfileDataForPatient(id:id,patientDataDict: dict, completion:{[unowned self] (succeed) in
            self.activityIndicator.hide()
            if succeed{
                DispatchQueue.main.async {
                    self.showAlertWith(message: "Your profile data submitted successfully", title: "")
                    self.patient = AppUser.patient
                    self.attachmentArray.removeAll()
                    self.restPatientDetails()
                }
            }
        }) {[unowned self] (message) in
            self.activityIndicator.hide()
            self.showAlertWith(message: message, title: "")
        }
     }else{
        self.showAlertWith(message: ErrorMessages.kNetworkError, title: "")
        }
    }
    
    func saveAttachmentData(attachedDoc:NSSet){
         var attachedElement = [String:Any]()
        var index:Int = 0
        for blobData in attachedDoc{
            let blobObj : FSBlob = blobData as! FSBlob
            let urlString : String = blobObj.url
            let avtor:[String : Any] = ["avatar" : "\(urlString)"]
            attachedElement["\(index)"] = avtor
                index += 1
        }
        
        let patientHash:[String:Any] = [
            "documents_attributes":attachedElement
        ]
        let dict:[String:Any] = ["patient": patientHash]
        guard let id = AppUser.patient?.id else{
//            print("User not found.")
            return
        }
        if Reachability.isConnectedToNetwork(){
            activityIndicator.show(allowInteraction: false)
            Patient.setProfileDataForPatient(id:id,patientDataDict: dict, completion:{[unowned self] (succeed) in
                self.activityIndicator.hide()
                if succeed{
                    DispatchQueue.main.async {
                        self.patient = AppUser.patient
                        self.attachmentArray.removeAll()
                        self.restPatientDetails()
                    }
                }
            }) {[unowned self] (message) in
                self.activityIndicator.hide()
                self.showAlertWith(message: message, title: "")
            }
        }else{
            self.showAlertWith(message: ErrorMessages.kNetworkError, title: "")
        }
        
    }
    
    
//    func getSourceDetails(blob:FSBlob) -> NSDictionary {
//
//        let dict : NSDictionary = ["url":blob.url,
//                "filename":blob.fileName,
//                "mimeType":blob.mimeType,
//                "size":blob.size,
//                "key":((blob.key==nil) ? "" : blob.key),
//                "container":((blob.container==nil) ? "" : blob.container),
//                "isWriteable": ((blob.writeable==nil) ? "" : blob.writeable)
//        ]
//        return dict as NSDictionary
//    }
    
    //MARK:- Add Card Action
    
    @IBAction func btnActn_addCard(_ sender: UIButton) {
        view.endEditing(true)
        guard let cardNumber = tf_cardNumber.text, cardNumber.count == 16  else {
            self.showAlertWith(title: "", message: "Please enter your valid card number", handler: nil)
            return
        }
        guard let cardExpiryDate = tf_cardExpiryDate.text, cardExpiryDate.count > 0 else {
            self.showAlertWith(title: "", message: "Please select your card expiry date", handler: nil)
            return
        }
        guard let cardCvvNumber = tf_cardCVV.text, cardCvvNumber.count > 0, cardCvvNumber.count <= 5 else {
            self.showAlertWith(title: "", message: "Please enter your card CVV number", handler: nil)
            return
        }
        let expirationDate = cardExpiryDate.components(separatedBy: "/")
        let cardParams = STPCardParams()
        cardParams.number = cardNumber
        cardParams.cvc = cardCvvNumber
        cardParams.expMonth = UInt(expirationDate[0])!
        cardParams.expYear = UInt(expirationDate[1])!
        addCartActivityIndicator.show(allowInteraction: false)
        STPAPIClient.shared().createToken(withCard: cardParams) { (token: STPToken?, error: Error?) in
            DispatchQueue.main.async {
                self.addCartActivityIndicator.hide()
            }
            guard let token = token, error == nil else {
                self.showAlertWith(title: "", message: error?.localizedDescription ?? "Card is not valid.", handler: nil)
                return
            }
            self.addCartApiForPatient(stipeToken: token.tokenId)
        }
    }
    
    //MARK:- Delete attached Doc
    @IBAction func deleteButtonTap(_ sender: UIButton){
        
        if sender.tag < (patient?.patientsDoc?.count)!{
        let attachedDocId:Int = (AppUser.patient?.patientsDoc![sender.tag].id)!
        let dict:[String:Any] = ["patient": [
            "documents_attributes":["0":["id":attachedDocId,"_destroy":true]]
            ]]
        guard let id = AppUser.patient?.id else{
//            print("User not found.")
            return
        }
        
        if Reachability.isConnectedToNetwork(){
            activityIndicator.show(allowInteraction: false)
            Patient.setProfileDataForPatient(id:id,patientDataDict: dict, completion:{[unowned self] (succeed) in
                self.activityIndicator.hide()
                if succeed{
                    DispatchQueue.main.async {
                        self.patient = AppUser.patient
                        self.restPatientDetails()
                    }
                }
            }) {[unowned self] (message) in
                self.activityIndicator.hide()
                self.showAlertWith(message: message, title: "")
            }
        }else{
            self.showAlertWith(message: ErrorMessages.kNetworkError, title: "")
        }
        }else{
            let previousDoc:Int = (patient?.patientsDoc?.count)!
            attachmentArray.remove(at: sender.tag - previousDoc)
            attachedDoc_CollectionView.reloadData()
        }
    }

}
extension PatientSettingViewController : FSPickerDelegate{
    func fsPicker(_ picker: FSPickerController!, didFinishPickingMediaWith blobs: [FSBlob]!) {        
        let blobSet = NSSet.init(array: blobs)
        self.saveAttachmentData(attachedDoc: blobSet)
        picker.dismiss(animated: true, completion: nil)
    }
    func fsPickerDidCancel(_ picker: FSPickerController!) {
        
    }
    
}

extension PatientSettingViewController : JKSegmentViewDelegate , UITextFieldDelegate{
    func didSelectSegmentAt(index segmentIndex: Int, view segmentView: JKSegmentView) {
        let x = segmentIndex == 0 ? 0 : UIScreen.main.bounds.size.width

        UIView.animate(withDuration: 0.3) { 
           self.scrollView.contentOffset.x = x
        }
        if segmentIndex == -1 {
            constaint_tableV_height.constant = CGFloat(min(60*3, 60 * cardDetailArray.count))
            tableV_cardList.reloadData()
        }
    }
    
    //MARK:- TextField delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return textField != txtF_attachDoc
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.tf_cardNumber{
            if string == ""{
                return true
            }
        
            if let str = textField.text, str.count > 15   {
                return false
            }
                return true
        }
        if textField == tf_cardCVV {
            if string == ""{
                return true
            }
            
            if let str = textField.text, str.count > 2   {
                return false
            }
            return true
        }
        return true
    }
    
}

extension PatientSettingViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cardDetailArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cardCellIdentifier, for: indexPath)as! CardTableViewCell
        cell.selectionStyle = .none
        cell.setDataFor(card: cardDetailArray[indexPath.row])
        cell.deleteButton.tag = indexPath.row
        cell.deleteButton.addTarget(self, action: #selector(cardDeleteButtonTap), for: UIControlEvents.touchUpInside)

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//         if let cell = tableView.cellForRow(at: indexPath)as? CardTableViewCell{
//            cell.isCardSelected = true
//
//        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        if let cell = tableView.cellForRow(at: indexPath)as? CardTableViewCell{
//            cell.isCardSelected = false
//            
//        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    @objc func cardDeleteButtonTap(sender:UIButton){
        deleteIndex = sender.tag
        if let patient = AppUser.patient{
            let card = self.cardDetailArray[deleteIndex]
            DispatchQueue.main.async {
                self.showAlertWith(message: "Are you sure, you want to delete the card?", title: "Alert", okCompletion: { (action) in
                    DispatchQueue.main.async {
                        self.activityIndicator.show(allowInteraction: true)
                        Card.deleteSavedCard(id: patient.id, sourceId: card.id, completion: { (success, message) in
                            self.cardDetailArray.remove(at: self.deleteIndex)
                            self.activityIndicator.hide()
                            self.showAlertWith(message: message, title: "")
                            DispatchQueue.main.async {
                                self.constaint_tableV_height.constant = CGFloat(min(60*3, 60 * self.cardDetailArray.count))
                                self.tableV_cardList.reloadData()
                            }
                        }, failure: { (message) in
                            self.activityIndicator.hide()
                            self.showAlertWith(message: message, title: "")
                        })
                    }
                }, cancelCompletion: { (cancel) in
                    
                })
        }
    }
    }
}
//extension PatientSettingViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate{
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {



extension PatientSettingViewController {
    
    override func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        self.imgV_profilePic.layer.cornerRadius = imgV_profilePic.frame.size.width/2
        self.imgV_profilePic.layer.masksToBounds = true
        
        picker.dismiss(animated: true, completion: nil)
        if let image = info["UIImagePickerControllerOriginalImage"] as? UIImage{
//            print("image size== \(image.size)")
            
            if  let scaledImage = image.scaleImage(targetSize: CGSize(width: 200, height: 200)){
                self.imgV_profilePic.image = scaledImage
                self.userImage = scaledImage
                //self.selectedImage = scaledImage
            }
            SDImageCache.shared().clearMemory()
            SDImageCache.shared().clearDisk(onCompletion: {
            })
        }
    }
    
    override func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
//MARK:- Media picker delegate
//extension PatientSettingViewController : MediaPickerDelegate {
//
//    func mediaPicker(_ mediaPicker: CAMediaManager!, didSelectedAssets assetList: [Any]!) {
//        attachmentArray = assetList as! [UIImage]
//        self.attachedDoc_CollectionView.reloadData()
//    }
//    func mediaPicker(_ mediaPicker: CAMediaManager!, didFailWith status: PHAuthorizationStatus) {
//        if (status == PHAuthorizationStatus.restricted) {
//            showAlertWith(message: "Application not having access to use photo album and camera", title: "")
//        }
//    }
//
//    func didCancelMediaPicker(_ mediaPicker: CAMediaManager!) {
//
//    }
//}

//MARK:- Collection viewCAMediaManager

extension PatientSettingViewController : UICollectionViewDataSource,UICollectionViewDelegate{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return attachmentArray.count + (patient?.patientsDoc?.count)!
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cellOfCollection = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentCell", for: indexPath) as! AttachmentCell
        
        cellOfCollection.deleteButton.tag = indexPath.row
        cellOfCollection.deleteButton.addTarget(self, action:#selector(deleteButtonTap(_:)), for:.touchUpInside)
        cellOfCollection.deleteButton.isUserInteractionEnabled = true
        
        if indexPath.row < (patient?.patientsDoc?.count)! {
            cellOfCollection.attachedImage.sd_setImage(with: patient?.patientsDoc?[indexPath.row].thumbnail_url, placeholderImage: UIImage(named: "Icon_Image"))
            return cellOfCollection
        }
        let previousDoc:Int = (patient?.patientsDoc?.count)!
        
        cellOfCollection.attachedImage.image = attachmentArray[indexPath.row - previousDoc]
        
        return cellOfCollection
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vcFullImageView = viewControllerFor(identifier: VCIdentifier.kAttachmentDoc) as! AttachedDocViewController
        vcFullImageView.delegate = self
        vcFullImageView.attechedDoc = (patient?.patientsDoc)!
        vcFullImageView.localAttachment = attachmentArray
        vcFullImageView.index = indexPath.row
        vcFullImageView.modalPresentationStyle = .overFullScreen
        vcFullImageView.modalTransitionStyle = .crossDissolve
        self.present(vcFullImageView, animated: true, completion: nil)
    }
    
}


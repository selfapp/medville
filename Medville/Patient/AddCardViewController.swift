//
//  AddCardViewController.swift
//  Medville
//
//  Created by Jitendra Solanki on 9/29/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit
import Stripe

class AddCardViewController: UIViewController {

    //MARK:- outlets
    
    @IBOutlet weak var viewCardDetails: UIView!
    
    @IBOutlet weak var txtF_cardNumber: GRFloatingTextField!
    
    @IBOutlet weak var txtF_monthDetails: GRFloatingTextField!
    
    @IBOutlet weak var txtF_cvv: GRFloatingTextField!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    lazy var datePicker:UIDatePicker  = {
        let picker = UIDatePicker()
        picker.datePickerMode =  UIDatePickerMode.date
        picker.minimumDate = self.setMinimumDate()
        picker.addTarget(self, action: #selector(didValueChangedIn(picker:)), for: .valueChanged)
        return picker
    }()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initializeUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide),
                                               name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        addShadowOnCardView()
    }
    @objc func keyboardWillHide() {
        self.view.frame.origin.y = 0
    }
    
    @objc func keyboardWillChange(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
           
                self.view.frame.origin.y = -(keyboardSize.height / 2)
            
        }
    }
    //MARK:- Private helper
    private func initializeUI(){
        self.navigationItem.title = "Add Card"

        txtF_cardNumber.addInputAccessoryViewWith(target: self, selector: #selector(doneBtnTappedOn_Keyboard))
        txtF_cvv.addInputAccessoryViewWith(target: self, selector: #selector(doneBtnTappedOn_Keyboard))
        txtF_monthDetails.addInputAccessoryViewWith(target: self, selector: #selector(doneBtnTappedOn_Keyboard))
        //txtF_monthDetails.inputView = datePicker
        txtF_cardNumber.delegate = self
        txtF_cvv.delegate = self
        let expiryDatePicker = MonthYearPickerView()
        txtF_monthDetails.inputView = expiryDatePicker
        expiryDatePicker.onDateSelected = { (month: Int, year: Int) in
            let string = String(format: "%02d/%d", month, year)
            self.txtF_monthDetails.place_holder = ""
            self.txtF_monthDetails.text = string
            NSLog(string) // should show something like 05/2015
        }
    }
    private func addShadowOnCardView(){
        //add shadow and border to view_profileData
        viewCardDetails.layer.cornerRadius = 5.0
        
        viewCardDetails.layer.shadowColor = UIColor(r:242,g:244,b:246,alpha:1.0).cgColor
        viewCardDetails.layer.shadowOffset = CGSize(width: 2, height: 2)
        viewCardDetails.layer.shadowRadius = 5.0
        viewCardDetails.layer.shadowOpacity = 1
        viewCardDetails.layer.shadowPath = UIBezierPath(rect: viewCardDetails.bounds).cgPath
        //viewCardDetails.layer.shouldRasterize = true
    }
    
    private func setMinimumDate()-> Date?{
        var components = DateComponents()
        components.day = 0
        let minDate = Calendar.current.date(byAdding: components, to: Date())
        return minDate
    }
    //MARK:- Action methods
    @objc private func doneBtnTappedOn_Keyboard(){
        view.endEditing(true)
    }
    @objc private func didValueChangedIn(picker:UIDatePicker!){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/YY"
        txtF_monthDetails.text = dateFormatter.string(from: datePicker.date)
        txtF_monthDetails.textFieldDidChange(notification: Notification.init(name: .UITextFieldTextDidChange) as NSNotification)
    }
    
    //call api
    private func addCartApiForPatient(stipeToken:String) {
        if Reachability.isConnectedToNetwork(){
         if let patient = AppUser.patient{
            Patient.addPatientsCardDetail(stripeToken: stipeToken, patientId:patient.id, completion: {[unowned self] (succeed) in
                self.activityIndicator.hide()
                if succeed{
                    DispatchQueue.main.async {
                        // Continue with payment...
                        UIApplication.shared.appDelegate.setHomePageForUser(type: .kPatient)
                    }
                }
                }) {[unowned self] (message) in
                    self.activityIndicator.hide()
                    self.showAlertWith(message: message, title: "")
                }
         }else{
             self.showAlertWith(message: ErrorMessages.kPateintNotFound, title:  "")
            }
        }
        else{
            self.showAlertWith(message: ErrorMessages.kNetworkError, title: "")
        }
    }
    
    @IBAction func btnActn_addCard(_ sender: Any) {
    view.endEditing(true)
    
    guard let cardNumber = self.txtF_cardNumber.text, cardNumber.count == 16  else {
        self.showAlertWith(title: "", message: "Please enter your valid card number", handler: nil)
        return
    }
    guard let cardExpiryDate = txtF_monthDetails.text, cardExpiryDate.count > 0 else {
        self.showAlertWith(title: "", message: "Please select your card expiry date", handler: nil)
        return
    }
    guard let cardCvvNumber = txtF_cvv.text, cardCvvNumber.count > 0, cardCvvNumber.characters.count <= 5 else {
        self.showAlertWith(title: "", message: "Please enter your card CVV number", handler: nil)
        return
    }
           let expirationDate = cardExpiryDate.components(separatedBy: "/")
            let cardParams = STPCardParams()
            cardParams.number = cardNumber
            cardParams.cvc = cardCvvNumber
            cardParams.expMonth = UInt(expirationDate[0])!
            cardParams.expYear = UInt(expirationDate[1])!
            activityIndicator.show(allowInteraction: false)
            STPAPIClient.shared().createToken(withCard: cardParams) { (token: STPToken?, error: Error?) in
                DispatchQueue.main.async {
                    self.activityIndicator.hide()
                }
                guard let token = token, error == nil else {
                    
                    self.showAlertWith(title: "", message: error?.localizedDescription ?? "Card is not valid.", handler: nil)
                    return
                }
                self.addCartApiForPatient(stipeToken: token.tokenId)
            }
     }
}

extension AddCardViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.txtF_cardNumber{
            if string == ""{
                return true
            }
            
            if let str = textField.text, str.count > 15   {
                return false
            }
            return true
        }
        if textField == txtF_cvv {
            if string == ""{
                return true
            }
            
            if let str = textField.text, str.count > 2   {
                return false
            }
            return true
        }
        return true
    }
    
}

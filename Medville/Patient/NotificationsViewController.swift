//
//  NotificationsViewController.swift
//  Medville
//
//  Created by Jitendra Solanki on 9/28/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit
import TwilioChatClient


class NotificationsViewController: UIViewController {
 
    //MARK:- Outlets
    @IBOutlet weak var tableViewNotification: UITableView!
    
    @IBOutlet weak var chatTableView: UITableView!
    @IBOutlet weak var hscrollView: UIScrollView!
    @IBOutlet weak var segmentView: JKSegmentView!
    //MARK:- iVar
    let notificationCell = "NotificationPatientCellIdentifier"
    let cellIdentifier = "chatUserCellIdentifier"
    var notifications:[LocalNotification] = []
    
    var messages:[ChannelListModel] = []
    var sortedArray:[ChannelListModel] = []
    var chatUsers:[ChatUser] = []
    var isTwilioConnected:Bool = false
    var msg:String = "Loading..."
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeUI()
        getNotificationForPatient()
        configureTableView()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if ChatManager._sharedManager.isConnected {
            twilioSetUp()
        }else{
            perform(#selector(twilioCall), with: nil, afterDelay: 0.5)
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func twilioCall(){
        if ChatManager._sharedManager.isConnected && !isTwilioConnected{
            twilioSetUp()
        }else{
            perform(#selector(twilioCall), with: nil, afterDelay: 0.5)
        }
    }
    
    //MARK:- Twilio Setup
    func twilioSetUp(){
        
        let client:TwilioChatClient = ChatManager.sharedManager().client!
        
        client.delegate = self
        if (client.synchronizationStatus == TCHClientSynchronizationStatus.completed){
            populateChannelList()
        }
    }
    
    func populateChannelList(){
        let channelList: TCHChannels = (ChatManager._sharedManager.client?.channelsList())!
        let newChannles = NSMutableOrderedSet(array: channelList.subscribedChannels())
        //sortChannels(channls: newChannles)
        DispatchQueue.main.async {
            self.loadInitialMessage(channels: newChannles)
//            self.chatTableView.reloadData()
        }
    }
   
    
    func loadInitialMessage(channels:NSMutableOrderedSet){
        var noOfChannel = 0
        msg = (noOfChannel == 0) ? "" :"Loading..."
        self.messages.removeAll()
        var chatUser:[String] = []
        for channel in channels{
            let channel1 = channel as! TCHChannel
            let uuid = channel1.uniqueName?.components(separatedBy: "/")
            chatUser.append(uuid![0])
            channel1.delegate = self
//            channel1.destroy(completion: { (result) in
//                print("channel deleted ")
//            })
            channel1.messages?.getLastWithCount(1, completion: { (result, message) in
                noOfChannel += 1
                let messageCount:Int = (message?.count)!
                if result.isSuccessful() && (messageCount > 0){

                    let objt = ChannelListModel()
                    objt.channel = channel1
                    objt.message = message?[0]
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = DateFormat.kCurrentFormat.rawValue
                    if let date = dateFormatter.date(from: (objt.message?.timestamp)!) {
                        objt.timeStamp = date
                    }
                    self.messages.append(objt)
                    //self.messages.add(objt)
                }
                if noOfChannel == channels.count{
                    self.sortedArray.removeAll()
                    self.sortCHannel()
                    self.chatTableView.reloadData()
                }
            })
        }
        self.getChatUser(uuids: chatUser)
    }
    
    
    // Sort Channles by friendly name
    func sortCHannel(){
        self.sortedArray = self.messages.sorted(by: {
            $0.timeStamp?.compare($1.timeStamp!) == .orderedDescending
        })
    }
    
    
    //MARK:- Private helper methods
    private func initializeUI(){
        self.navigationItem.title = "Notifications"
        segmentView.setupViewWithSegments(segments: ["Chat","Notification"])
        segmentView.delegate = self
    }

    private func configureTableView(){
        
        tableViewNotification.tableFooterView = UIView(frame: CGRect.zero)
        tableViewNotification.delegate = self
        let nib = UINib(nibName: "PatientNotificationCell", bundle: Bundle.main)
        tableViewNotification.register(nib, forCellReuseIdentifier: notificationCell)
        
        // Chat List
        let nib1 = UINib(nibName: "ChatUserCell", bundle: Bundle.main)
        chatTableView.register(nib1, forCellReuseIdentifier: cellIdentifier)
        chatTableView.tableFooterView = UIView(frame: CGRect.zero)
        chatTableView.delegate = self
        chatTableView.dataSource = self
        
    }
    

    
    
    //Get Notification
    private func getNotificationForPatient(){
        if Reachability.isConnectedToNetwork(){
            if let patient = AppUser.patient{
                //spinner.show(allowInteraction: false)
                Patient.getNotificationForPatient(id: patient.id,  {[weak self] (notification) in
                    self?.notifications.removeAll()
                    self?.notifications = notification
                    DispatchQueue.main.async {
                        self?.tableViewNotification.reloadData()
                    }
                    }, failure: {[weak self] (message) in
                        //  self?.showAlertWith(message: message, title: "")
                })
            }else{
                //self.showAlertWith(message: ErrorMessages.kDoctorNotFound, title:  "")
            }
        }else{
            self.showAlertWith(message:ErrorMessages.kNetworkError, title: "")
        }
    }
    
    // Get Chat User Details
    private func getChatUser(uuids:[String]){
        if Reachability.isConnectedToNetwork(){
            Patient.getChatsForPatient(uuids: uuids, { (chatUser) in
                print("Response --> \(chatUser)")
                self.chatUsers.removeAll()
                self.chatUsers = chatUser
                self.chatTableView.reloadData()
            }, failure: { (message) in
                print("message")
            })
        }else{
            self.showAlertWith(message:ErrorMessages.kNetworkError, title: "")
        }
    }
    
    func getChatUserDetails(uuid:String) -> ChatUser? {
        
                let user = self.chatUsers.filter( { return $0.uuid == uuid } )
                if user.count > 0{
                    return user.last!
                    }
                return nil
            }
    
}
    extension NotificationsViewController:UITableViewDelegate, UITableViewDataSource{
        
        func numberOfSections(in tableView: UITableView) -> Int {
            if chatTableView == tableView{
                if self.sortedArray.count > 0 {
                    chatTableView.separatorStyle = .singleLine
                    chatTableView.backgroundView?.isHidden = true
                    return 1
                }else{
                    let message = UILabel()
                    message.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height)
                    message.text = msg
                    message.numberOfLines = 0
                    message.sizeToFit()
                    message.textAlignment = .center
                    chatTableView.backgroundView = message
                    chatTableView.separatorStyle = .none
                    chatTableView.backgroundView?.isHidden = false
                    return 0
                }
            }
            return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if chatTableView == tableView{
              return  self.sortedArray.count
            }
            return self.notifications.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            if tableView == chatTableView{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)as! ChatUserCell
                let object = self.sortedArray[indexPath.row]
                let users = object.channel?.friendlyName?.components(separatedBy: "/")
                let uuid = object.channel?.uniqueName?.components(separatedBy: "/")
                if let chat = self.getChatUserDetails(uuid: (uuid?.first)!){
                    cell.avtorImageView.sd_setImage(with: chat.avatar, placeholderImage: UIImage(named: "defualt-profile"))
                }
            
                cell.userName.text = users?[0]
                cell.messagelbl.text = object.message?.body
                cell.messagelbl.textColor = (object.unreadMessage > 0) ? UIColor(red: 128/255, green: 156/255, blue: 237/255, alpha: 1) : UIColor.gray
                cell.timeLabel.text = object.message?.timestamp?.getTimeDifferenceStringTillNowWith(currentFormat: DateFormat.kCurrentFormat.rawValue)

                return cell
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: notificationCell, for: indexPath)as! PatientNotificationCell
            cell.notificationMsg.text = notifications[indexPath.row].message
            let notificationImage : UIImage = indexPath.row % 2 == 0 ?#imageLiteral(resourceName: "notification-icon-1") :#imageLiteral(resourceName: "notification-icon-2")
            cell.notificationIcon.setImage(notificationImage, for: UIControlState.normal)
            return cell
        }

        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 95
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            self.tabBarController?.viewControllers?[2].tabBarItem.badgeValue = nil
            if chatTableView == tableView {
                let object = self.sortedArray[indexPath.row]
                let channel:TCHChannel = object.channel!
                if let chatVC  = viewControllerFor(identifier: VCIdentifier.kChatViewController)as? ChatViewController{
                    chatVC.channel = channel 
                    let users = channel.friendlyName?.components(separatedBy: "/")
                    let uuids = channel.uniqueName?.components(separatedBy: "/")
                    chatVC.chatUserName = users?[0]
                    chatVC.chatUserIdentity = uuids?[0]
                    chatVC.currentIdentity = uuids![1]
                    chatVC.currentUserName = users?[1]
                    
                    self.navigationController?.pushViewController(chatVC, animated: true)
                    
                }
            }

            }
        
    }


extension NotificationsViewController:JKSegmentViewDelegate{
    func didSelectSegmentAt(index segmentIndex: Int, view segmentView: JKSegmentView) {
        var x:CGFloat = 0
        if segmentIndex == 0{
            self.chatTableView.reloadData()
        }else{
            x = UIScreen.main.bounds.size.width
            self.tableViewNotification.reloadData()
        }
        
        UIView.animate(withDuration: 0.3) {
            self.hscrollView.contentOffset.x = x
        }
        
    }
}

//MARK:- Twilio Delegate
extension NotificationsViewController:TwilioChatClientDelegate{
    
    func chatClient(_ client: TwilioChatClient, channelAdded channel: TCHChannel) {
        
    }
    func chatClient(_ client: TwilioChatClient, synchronizationStatusUpdated status: TCHClientSynchronizationStatus) {
        if (status == TCHClientSynchronizationStatus.completed){
            populateChannelList()
        }
        
    }
    func chatClient(_ client: TwilioChatClient, notificationAddedToChannelWithSid channelSid: String) {
        
    }
    
}

extension NotificationsViewController:TCHChannelDelegate{
    func chatClient(_ client: TwilioChatClient, channel: TCHChannel, messageAdded message: TCHMessage) {
        self.addMessageTo(channel: channel, message: message)
    }
    
    func addMessageTo(channel:TCHChannel,message:TCHMessage){
        
        let noOfValue = self.messages.count
        for index in 0...noOfValue{
            let object = self.messages[index]
            if object.channel?.uniqueName == channel.uniqueName{
                object.message = message
                object.unreadMessage += 1
                self.messages.remove(at: index)
                self.messages.insert(object, at: index)
                self.sortCHannel()
                break
            }
        }
        self.chatTableView.reloadData()
    }
    
}

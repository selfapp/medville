//
//  PatientTabBarController.swift
//  Medville
//
//  Created by Jitendra Solanki on 9/26/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

class PatientTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.shared.appDelegate.registerOnPusher(identity: (AppUser.patient?.uuid)!)

        if !ChatManager._sharedManager.isConnected{
        twillioSetup()
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func twillioSetup(){
        
        if let patient = AppUser.patient{
            ChatManager._sharedManager.loginWithidentity(identity: patient.uuid, completion: { (status) in
                print("is Twilio Connected - \(status)")
                let viewcon = AudioViewController.shared
                if !status{
                    self.twillioSetup()
                }
            })
        }
    }

}



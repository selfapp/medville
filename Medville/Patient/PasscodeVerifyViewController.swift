//
//  PasscodeVerifyViewController.swift
//  Medville
//
//  Created by Jitendra Solanki on 9/25/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

class PasscodeVerifyViewController: ViewController,PasswordInputViewDelegate {

    //MARK:- outlets
    @IBOutlet weak var passcodeView: PasswordInputView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var mobileNumber:String!
    
    let toolBar:UIToolbar = {
        let bar = UIToolbar()
        bar.sizeToFit()
        let doneButton = UIBarButtonItem.init(title: "Done", style: UIBarButtonItemStyle.plain, target: target, action: #selector(toolBar_doneBtnTapped))
        bar.setItems([doneButton], animated: true)
        return bar
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        passcodeView.delegate = self
        passcodeView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOnPasscodeView(sender:))))
        //passcodeView.becomeFirstResponder()
        
        // Do any additional setup after loading the view.
    }
    
    override var inputAccessoryView: UIView?{
        get{
            return toolBar
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc private func didTapOnPasscodeView(sender:UITapGestureRecognizer){
        if !passcodeView.isFirstResponder{
            passcodeView.becomeFirstResponder()
        }
    }
   
  
    @objc func toolBar_doneBtnTapped(){
        passcodeView.resignFirstResponder()
    }
    
    func navigateToAddCartScreen(isCardAdded:Bool)  {
        if isCardAdded {
           UIApplication.shared.appDelegate.setHomePageForUser(type: .kPatient)
        }else{
            let addCard = viewControllerFor(identifier: VCIdentifier.kAddCard)
            let navVC = UINavigationController(rootViewController: addCard)
            self.present(navVC, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func btnActn_resendCode(_ sender: UIButton) {
        if Reachability.isConnectedToNetwork(){
            activityIndicator.show(allowInteraction: false)
            Patient.sendOTPFor(mobileNumber: mobileNumber, completion: {[unowned self] (succeed) in
                self.activityIndicator.hide()
                 
            }) {[unowned self] (message) in
                self.activityIndicator.hide()
                self.showAlertWith(message: message, title: "")
            }
        }else{
            self.showAlertWith(message: ErrorMessages.kNetworkError, title: "")
        }
    }
    
    
    @IBAction func btnActn_back(_ sender: UIButton) {
          self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnActn_verifyPasscode(_ sender: JKRoundButton) {
       passcodeView.resignFirstResponder()
        if passcodeView.hasEmptySlot{
            let alertController = UIAlertController.init(title: "", message: "Please enter a valid OTP", preferredStyle: .alert)
            let okActn = UIAlertAction.init(title: "Ok", style: .default, handler: { (okAction) in
                self.passcodeView.becomeFirstResponder()
            })
            alertController.addAction(okActn)
            self.present(alertController, animated: true, completion: nil)
            
//            self.showAlertWith(message: "Please enter a valid OTP", title: "")
//            self.passcodeView.becomeFirstResponder()
            return
            
        }
        
        
        if Reachability.isConnectedToNetwork(){
            
            activityIndicator.show(allowInteraction: false)
            Patient.verifyPasscodeFor(mobileNumber: mobileNumber, with: passcodeView.code, completion: {[unowned self] (success, pateint, isCardAdded) in
                if success{
                    self.activityIndicator.hide()
                    UIApplication.shared.appDelegate.registerOnPusher(identity: (pateint?.uuid)!)
                    ChatManager._sharedManager.loginWithidentity(identity: (pateint?.uuid)!, completion: { (status) in
                        if status{
                            print("Twilio Login success")
                            let viewcon = AudioViewController.shared
                        }else{
                            self.showAlertWith(message: "Something went wrong", title: "")
                        }
                    })
                    DispatchQueue.main.async {
                        self.navigateToAddCartScreen(isCardAdded:isCardAdded)
                    }
                }else{
                    self.activityIndicator.hide()
                    self.showAlertWith(message: "Something went wrong", title: "")
                }
                
            }) {[unowned self] (message) in
                self.activityIndicator.hide()
                self.showAlertWith(message: message, title: "")
                self.passcodeView.clear()
            }
            
        }else{
            self.showAlertWith(message: ErrorMessages.kNetworkError, title: "")
        }
    }
    
    //MARK:- PasscodeInputViewDelegate
    func PassWordInputDidFinishWith(code: String) {
         
    }
    
 
}

//extension PasscodeVerifyViewController:UITextFieldDelegate{
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        return false
//    }
//}

//
//  PatientLogInViewController.swift
//  Medville
//
//  Created by Jitendra Solanki on 9/25/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

class PatientLogInViewController: ViewController {

    
    //MARK:- outlets
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var viewMobileInput: UIView!
    
    @IBOutlet weak var tf_mobileNumber: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        intializeUI()
        
    }
    
    //MARK:- Private helper methods
    private func intializeUI(){
        //force setup constraints
        self.viewDidLayoutSubviews()
        
        let color = UIColor.init(r: 244, g: 247, b: 254, alpha: 1.0)

        //round view
        // viewMobileInput.makeCornerRound(cornerRadius: viewMobileInput.frame.size.height/2)
         viewMobileInput.addBorderWith(borderWidth: 1.0, withBorderColor:color , withCornerRadious: viewMobileInput.frame.size.height/2)
        //set placeholder color
         tf_mobileNumber.setPlaceholderColorWith(color: color)
        tf_mobileNumber.delegate = self
        //add toolbar
        tf_mobileNumber.addInputAccessoryViewWith(target: self, selector: #selector(doneBtnTapped_onKeyboard))
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Private methods
    @objc private func doneBtnTapped_onKeyboard(){
        tf_mobileNumber.resignFirstResponder()
    }
    
    
    //MARK:- Action methods
    @IBAction func btnActn_continue(_ sender: UIButton) {
         self.tf_mobileNumber.resignFirstResponder()
        let getValidMobileNo = self.tf_mobileNumber.text?.replacingOccurrences(of: " ", with: "")
        guard let mobileNumberStr = getValidMobileNo, mobileNumberStr.count == 10  else {
           // self.showAlertWith(title: "", message: "Please enter a valid mobile number.", handler: nil)3
            let alertController = UIAlertController.init(title: "", message: "Please enter a valid mobile number.", preferredStyle: .alert)
            let okActn = UIAlertAction.init(title: "Ok", style: .default, handler: { (okAction) in
               // handler?(okAction)
                self.tf_mobileNumber.becomeFirstResponder()
            })
            alertController.addAction(okActn)
            self.present(alertController, animated: true, completion: nil)
            return
        }
        view.endEditing(true)
       //call api
       if Reachability.isConnectedToNetwork(){
        activityIndicator.show(allowInteraction: false)
        Patient.sendOTPFor(mobileNumber: mobileNumberStr, completion: {[unowned self] (succeed) in
            self.activityIndicator.hide()
            if succeed{
                DispatchQueue.main.async {
                    self.navigateToOtpVerifyController()
                }
            }
        }) {[unowned self] (message) in
            self.activityIndicator.hide()
             self.showAlertWith(message: message, title: "")
        }
       }else{
         self.showAlertWith(message: ErrorMessages.kNetworkError, title: "")
        }
    }
    
    @IBAction func btnActn_doctorLogIn(_ sender: UIButton) {
        (UIApplication.shared.delegate as! AppDelegate).logInForUser(type: .kDocotor)
    }

   private func navigateToOtpVerifyController() {
        view.endEditing(true)
        let getMobileNumber = self.tf_mobileNumber.text?.replacingOccurrences(of: " ", with: "")
        let vc = viewControllerFor(identifier: VCIdentifier.kPasscodeVerify) as! PasscodeVerifyViewController
        vc.mobileNumber=getMobileNumber
        self.present(vc, animated: true, completion: nil)
    }
    
 
}
extension PatientLogInViewController:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == ""{
            return true
        }
        
        if let str = textField.text, str.count > 9   {
            return false
        }
        return true
     }
}

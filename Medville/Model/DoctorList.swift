//
//  DoctorList.swift
//  Medville
//
//  Created by Durgesh on 05/12/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import Foundation

struct DoctorList:Codable {
    var doctors : [Doctor]?
    var current_page : Int
    var total_pages : Int
    var is_first_page : Bool
    var is_last_page : Bool
    var records_count : Int

    init?(json:[String:Any]){
        guard let current_page = json["current_page"]as? Int,
            let total_pages = json["total_pages"]as? Int,
            let is_first_page =  json["is_first_page"]as? Bool,
            let is_last_page =  json["is_last_page"]as? Bool,
            let records_count =  json["records_count"]as? Int
            else{
                return nil
        }
        self.current_page = current_page
        self.total_pages = total_pages
        self.is_first_page = is_first_page
        self.is_last_page = is_last_page
        self.records_count = records_count
        
        
        if let doctors = json["doctors"]as? [[String:Any]] {
            let doctors = doctors.map({ (element) ->Doctor? in Doctor(json: element)
                
            }).flatMap {$0}
            self.doctors = doctors
        }
    }
}


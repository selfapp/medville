//
//  Card.swift
//  Medville
//
//  Created by Jitendra Solanki on 10/12/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import Foundation
enum CardType : String{
    case VISA
    case MASTER_CARD
    case AMEX
    case DISCOVER
    case JCB
    case DINER_CLUB
    case UNKNOWN
}

 

struct Card {
    var cardSourceId:String
    var id:Int
    var patientId:Int
    var last4:String
    var type:CardType?
 
    init?(json:[String:Any]){
        guard
            let cardSourceId = json["source"]as? String,
            let id = json["id"]as? Int,
            let patientId =  json["patient_id"]as? Int,
            let cardSourceJson = json["source_object"]as? [String:Any],
            let last4 =  cardSourceJson["last4"]as? String,
            let type = cardSourceJson["brand"]as? String
         else{
                return nil
        }
        self.cardSourceId = cardSourceId
        self.id = id
        self.patientId = patientId
        self.last4 = last4
        
        let cardType =  CardType(rawValue: type)
        self.type = cardType
    }
    
    //MARK:- Get All Payment Source
    static func getAllCartDetailForPatient(id:Int, completion:@escaping (_ success:Bool, _ cardArr:[Card])->Void, failure:@escaping (_ message:String)-> Void){
        let url = "/patients/\(id)/payment_sources"
        makeGetHttpRequestWith(url: url, dataDict: nil, successBlock: { (success, result, statusCode) in
            
            if let dictResult = result as? [String:Any],
                let cardHash = dictResult["payment_sources"]as? [[String:Any]]{
                
                let arrCardDetail = cardHash.map({ (element) -> Card? in
                    Card(json: element)
                }).flatMap{$0}
                completion(success,arrCardDetail)
                
            }else{
                completion(false, [])
            }
            
        }) { (message, statusCode) in
            failure(message)
        }
    }
    
    //Mark:- Delete save card
    static func deleteSavedCard(id:Int,sourceId:Int, completion:@escaping (_ success:Bool, _ message:String)->Void, failure:@escaping (_ message:String)-> Void){
        let url = "/patients/\(id)/payment_sources/\(sourceId)"
        makeDeleteHttpRequestWith(url: url, dataDict: nil, successBlock: { (success, result, statusCode) in
            if let dictResult = result as? [String:Any]{
                let message = dictResult["message"]as! String
                completion(true,message)
            }else{
                completion(false,"")
            }
        }) { (message, statusCode) in
            failure(message)
        }
    }
    
}

//
//  ChatUser.swift
//  Medville
//
//  Created by Durgesh on 14/12/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import Foundation

//struct ChatUser:Codable{
class ChatUser:NSObject, Codable,NSCoding{

    let name:String
    let uuid:String
    let avatar:URL
    
    init?(json:[String:Any]){
        guard let name = json["name"]as? String,
            let uuid = json["uuid"]as? String,
            let imageUrl =  json["avatar"]as? String,
            let fullImageUrl  = URL(string: kBaseURL+imageUrl)
            else{
                return nil
        }
        
        self.name = name
        self.uuid = uuid
        self.avatar = fullImageUrl
        
    }
    init(name:String,uuid:String,avatar:URL) {
        self.name = name
        self.uuid = uuid
        self.avatar = avatar
    }
    
    convenience required init(coder aDecoder: NSCoder) {
        let name = aDecoder.decodeObject(forKey: "name") as! String
        let uuid = aDecoder.decodeObject(forKey: "uuid") as! String
        let avatar = aDecoder.decodeObject(forKey: "avatar") as! URL
        self.init(name: name, uuid: uuid, avatar: avatar)
    }

    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "name")
        aCoder.encode(uuid, forKey: "uuid")
        aCoder.encode(avatar, forKey: "avatar")
    }
}


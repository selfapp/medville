//
//  LocalNotification.swift
//  Medville
//
//  Created by Durgesh on 14/12/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import Foundation

struct LocalNotification:Codable{
    
    let id:Int
    let message:String
    let created_at:String
    init?(json:[String:Any]){
        guard let id = json["id"]as? Int,
            let message = json["message"]as? String,
        let created_at = json["created_at"]as? String
            else{
                return nil
        }
        self.id = id
        self.message = message
        self.created_at = created_at
    }
    
}

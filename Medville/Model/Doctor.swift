//
//  Doctor.swift
//  Medville
//
//  Created by Jitendra Solanki on 10/5/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import Foundation

struct Doctor:Codable{
    
    var id: Int
    var first_name:String
    var last_name:String?
    var email:String
    var address:String
    var phoneNumber:String
    var countyCode:String?
    var specialization:String
    var description:String
    var avatarPath:URL
    var appointment_fee:Int
    var uuid:String
    var rating:Int
    var feedbacks:[Feedback]?
    
    
    init?(json:[String:Any]){
        
        guard
        //let doctorHash = json["doctor"]as? [String:Any],
            let dId = json["id"] as? Int,
            let fname = json["first_name"]as? String,
            //let lname = doctorHash["last_name"]as? String,
            let email = json["email"]as? String,
            let address = json["address"]as? String,
            let phone =  json["phone_number"]as? String,
            let specialization = json["specialization"]as? String,
            let description = json["description"]as? String,
            let imageUrl = json["image"]as? String,
            let uuid = json["uuid"]as? String,
            let rating = json["rating"]as? Int,
            let fullImageUrl  = URL(string: kBaseURL+imageUrl),
            let appointment_fee = json["appointment_fee"] as? Int
            else {
                return nil
        }
        
        self.countyCode = json["country_code"]as? String
        self.last_name = json["last_name"]as? String
        self.id = dId
        self.first_name =  fname
        self.uuid = uuid
       // self.last_name = lname
        self.email = email
        self.address = address
        self.phoneNumber = phone
        self.specialization = specialization
        self.description = description
        self.avatarPath = fullImageUrl
        self.appointment_fee = appointment_fee
        self.rating = rating
        if let feedbackArr = json["feedbacks"]as? [[String:Any]] {
            let feedBacks = feedbackArr.map({ (element) ->Feedback? in Feedback(json: element)
                
            }).flatMap {$0}
            self.feedbacks = feedBacks
        }
    }
    
    //
    static func doctorsWith(arrDoctors:[[String:Any]])-> [Doctor]?{
        
        let allDoctors:[Doctor] =  arrDoctors.map({ (doctor) -> Doctor? in
            let newDoctor = Doctor(json: doctor)
            return newDoctor
        }).flatMap{$0}
        
        return allDoctors
        
    }
    
    
    
    //MARK: login doctor
    static func loginWith(email:String, password:String,completion:@escaping (_ doctor:Doctor?, _ message:String?)->Void){
        
    let dict:[String:Any] = ["doctor":["email":email,"password":password],
                             "device":["uuid":DefaultParams.uuid,"token":DefaultParams.deviceToken]
                            ]
        
        makeHttpPostRequestWith(url: "/doctors/sign_in", dataDict: dict, shouldAuthorize: false, successBlock: { (success, json, statusCode) in
            
             if  let dict = json as? [String:Any],
                let docDict = dict["doctor"]as? [String:Any],
                let authToken = docDict["auth_token"]as? String,
                let refreshToken = docDict["refresh_token"]as? String{
                
                if let doctor = Doctor(json: docDict){
                    
                    /*setup login environment for doctor,like save auth token and refresh token*/
                
                    AppUser.isLoggedIn = true
                    AppUser.authToken = authToken
                    AppUser.refreshToken = refreshToken
                    AppUser.userType = UserType.kDocotor
                    AppUser.doctor = doctor
                    
                    completion(doctor,nil)
                    return
                }
            }
            completion(nil,"Unable to login. Please try later.")
            
        }) { (message, statusCode) in
            completion(nil, message)
        }
    }
    
    
    //MARK:- Get Patients List
    
    static func getPatientsListForDocotor(id:Int,_ completion:@escaping (_ active:[Patient], _ inactive:[Patient])-> Void, failure:@escaping(_ message: String)-> Void){
        
        makeGetHttpRequestWith(url: "/doctors/\(id)/patients", dataDict: nil, successBlock: { (success, json, statusCode) in
            
            if let dict = json as? [String:Any],
                let activePatient = dict["patients"]as? [[String:Any]]
               // let inActivePatient = dict["inactive_patients"]as? [[String:Any]]
            {
                let upcomingPatients =  activePatient.map({ (element) -> Patient? in
                     Patient(json: element)
                }).flatMap{$0}
                
               // let completedPatients = inActivePatient.map({ (element) -> Patient? in
                //     Patient(json: element)
              //  }).flatMap{$0}
                
                completion(upcomingPatients, [])
            }else{
                completion([], [])
            }
        }) { (message, statusCode) in
             failure(message)
        }
        
    }
    //MARK: Logout
    
    static func logoutDoctor(id:Int,_ completion:@escaping (_ status:Bool)-> Void, failure:@escaping(_ status:Bool)-> Void){
        
        makeGetHttpRequestWith(url: "/doctors/\(id)/logout", dataDict: nil, successBlock: { (success, json, statusCode) in
                completion(true)
        }) { (message, statusCode) in
            failure(false)
        }
        
    }
    
    //Notify server for push notification to the other user for video calling
    static func sendRoomNameForNotification(dict:[String:Any], completion:@escaping(_ response:String)->Void, failure:@escaping(_ message:String)->Void) {
        
        makeHttpPostRequestWith(url: "/calls/notify", dataDict: dict, shouldAuthorize: true, successBlock: { (success, response, statusCode) in
            guard let json = response as? [String:Any],
                let successMessage = json["message"] as? String
                else {
                    return
            }
            completion(successMessage)

        }) { (message, statusCode) in
            failure(message)

        }
    }
    
    
    //MARK:- Get Appointments for loggedIn doctor
    //Upcoming and completed
    
    static func getAppointmentsForDoctor(id:Int, _ completion:@escaping (_ upcoming:[Appointment], _ completed:[Appointment])-> Void, failure:@escaping (_ message:String)->Void){
        
        //get the doctor id
        
        //call api
        makeGetHttpRequestWith(url: "/doctors/\(id)/appointments", dataDict: nil, successBlock: { (success, json, statusCode) in
            if let dict = json as? [String:Any],
            let activeAppointments = dict["active_appointments"]as? [[String:Any]],
            let inActiveAppointments = dict["inactive_appointments"]as? [[String:Any]]
            {
                let upcomingAppointments = activeAppointments.map({ (element:[String:Any]) -> Appointment? in
                    Appointment(json: element)
                }).flatMap{$0}
                
                
                let completedAppointments = inActiveAppointments.map({ (element:[String:Any]) -> Appointment? in
                    Appointment(json: element)
                }).flatMap{$0}
                
                completion(upcomingAppointments, completedAppointments)
            }else{
                completion([],[])
            }
            
        }) { (message, statusCode) in
             failure(message)
        }
    }
    
    // Chat list
    static func getChatsForDoctor(uuids:[String], _ completion:@escaping (_ chatList:[ChatUser])-> Void, failure:@escaping (_ message:String)->Void){
       
        let join = uuids.joined(separator: ",")
        
        makeGetHttpRequestWith(url: "/calls/contacts?uuid=\(join)", dataDict: nil, successBlock: { (success, json, statusCode) in
            
            if let dict = json as? [String:Any],
                let patientChatList = dict["contacts"]as? [[String:Any]]
            {
                let chatUsers = patientChatList.map({ (element:[String:Any]) -> ChatUser? in
                    ChatUser(json: element)
                }).flatMap{$0}
                
                completion(chatUsers)
            }else{
                completion([])
            }
            
        }) { (message, statusCode) in
            failure(message)
        }
    }
    
    // Notification List
    static func getNotificationForDoctor(id:Int, _ completion:@escaping (_ notification:[LocalNotification])-> Void, failure:@escaping (_ message:String)->Void){
        
        makeGetHttpRequestWith(url: "/doctors/\(id)/notifications", dataDict: nil, successBlock: { (success, json, statusCode) in
            
            if let dict = json as? [String:Any],
                let notifications = dict["notifications"]as? [[String:Any]]
            {
                let notification = notifications.map({ (element:[String:Any]) -> LocalNotification? in
                    LocalNotification(json: element)
                }).flatMap{$0}
                
                completion(notification)
            }else{
                completion([])
            }
            
        }) { (message, statusCode) in
            failure(message)
        }
    }
    
    //MARK:- Update Profile
    static func updateProfileFor(id:Int, dict:[String:Any], completion:@escaping (_ doctor:Doctor?,_ message:String?)-> Void){
        
        makePutHttpRequestWith(url: "/doctors/\(id)", dataDict: dict, successBlock: { (success, json, statusCode) in
            if  let dict = json as? [String:Any],
                let docDict = dict["doctor"]as? [String:Any]{
                
                if let doctor = Doctor(json: docDict){
                    
                    /*setup login environment for doctor,like save auth token and refresh token*/
                    
                    AppUser.isLoggedIn = true
                    //AppUser.authToken = authToken
                    //AppUser.refreshToken = refreshToken
                    AppUser.userType = UserType.kDocotor
                    AppUser.doctor = doctor
                    
                    completion(doctor,"Profile updated successfully")
                    return
                }
            }else{
                completion(nil,"Profile updation failed. Please try later.")
            }
        }) { (message, statusCode) in
             completion(nil,message)
        }
        
    }
    
    //MARK:- Change Password
    static func changePasswordFor(id:Int, dict:[String:Any], completion:@escaping (_ message:String,_ success:Bool)->Void, failure:@escaping (_ message:String)->Void){
        
        makePutHttpRequestWith(url: "/doctors/\(id)", dataDict: dict, successBlock: { (success, response, statusCode) in
            
            guard let json =  response as? [String:Any] else{
                completion("Unable to change password now. Please try later.",false)
                return
            }
            
            completion("Password changed successfully.",true)
            
        }) { (message, statusCode) in
             failure(message)
        }
        
    }
}

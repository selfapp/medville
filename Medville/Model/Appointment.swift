//
//  Appointment.swift
//  Medville
//
//  Created by Jitendra Solanki on 10/5/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import Foundation

struct Appointment:Codable{
    
    let id:Int
    let start_time:String
    let end_time:String
    let created_at:String
    let updated_at:String
    let state:String
    let payment_status:Bool
    var doctor:Doctor?
    var patient:Patient?
    
    init?(json:[String:Any]){
        guard let id = json["id"]as? Int,
        let start_time = json["start_time"]as? String,
        let end_time =  json["end_time"]as? String,
        let created_at =  json["created_at"]as? String,
        let updated_at =  json["updated_at"]as? String,
        let state = json["state"] as? String,
        let paymentStatus = json["payment_status"]as? Bool
        else{
                return nil
         }
        
        self.id = id
        self.start_time = start_time
        self.end_time = end_time
        self.created_at = created_at
        self.updated_at = updated_at
        self.state = state
        self.payment_status = paymentStatus
        
        self.doctor = nil
        self.patient = nil
        if let doctorJson = json["doctor"]as? [String:Any]{
            self.doctor =  Doctor(json: doctorJson)
            
        }
        if let pateintJson = json["patient"] as? [String:Any]{
            self.patient = Patient(json: pateintJson)
        }
        
    }
    
}




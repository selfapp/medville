//
//  Patient.swift
//  Medville
//
//  Created by Jitendra Solanki on 10/3/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import Foundation

struct Patient:Codable {
    var id:Int
    var uuid:String
    var firstName:String?
    var lastname:String?
    var age:Int?
    var bloodGroup:String?
    var weight:Int?
    var height:Int?
    var phoneNumber:String
    var stripeCustomerId:String?
    var patientsDoc:[Attachment]?
    var avatarPath:URL?
    var hyphenate_username:String?
    var hyphenate_password:String?
    var state:String?
    
    var inactive_appointments:[Appointment]?
    
    //var authToKen:String
   // var refreshToken:String
    
    init?(json:[String:Any]) {
        guard
       let pId = json["id"]as? Int,
       let p_PhoneNumber = json["phone_number"]as? String,
        let uuid = json["uuid"]as? String
           
       //let p_AuthToKen = json["auth_token"]as? String,
       //let p_RefreshToken = json["refresh_token"]as? String
        else {
            return nil
        }
        let hyphenate_username = json["hyphenate_username"]as? String
        let hyphenate_password = json["hyphenate_password"]as? String
        let state = json["state"]as? String
        if let imageurl = json["image"] as? String,
            let fullImageUrl  = URL(string: kBaseURL + imageurl){
            self.avatarPath = fullImageUrl
        }
        self.id = pId
        self.phoneNumber = p_PhoneNumber
        
        //self.authToKen = p_AuthToKen
        //self.refreshToken = p_RefreshToken
        self.uuid = uuid
        self.firstName = json["first_name"]as? String
        self.lastname = json["last_name"]as? String
        self.age = json["age"]as? Int
        self.bloodGroup = json["blood_group"]as? String
        self.weight = json["weight"]as? Int
        self.height = json["height"]as? Int
        self.hyphenate_username = hyphenate_username
        self.hyphenate_password = hyphenate_password
        self.state = state
        //self.patientsDoc = json["documents"]as? [String]
        if let arrAttachment = json["documents"]as? [[String:Any]] {
            let attacments = arrAttachment.map({ (element) ->Attachment? in Attachment(json: element)
                
            }).flatMap {$0}
            self.patientsDoc = attacments
        }
        
        //init Appoinments
        if let arrAppoints = json["inactive_appointments"]as? [[String:Any]]{
            
            let appointments = arrAppoints.map({ (element) -> Appointment? in
                Appointment(json: element)
            }).flatMap {$0}
            
            self.inactive_appointments = appointments
            
        }
        self.stripeCustomerId  = json["stripe_customer_id"]as? String
    }

    
    static func patientsWith(arrPatients:[[String:Any]])-> [Patient]?{
        
        let allPatients:[Patient] =  arrPatients.map({ (patient) -> Patient? in
            let newPatient = Patient(json: patient)
            return newPatient
        }).flatMap{$0}
        
        return allPatients
    }
    
    //MARK:- Get OTP
    static func sendOTPFor(mobileNumber:String, completion:@escaping (_ succeed:Bool)->Void, failure:@escaping (_ message:String)->Void){
        let dict =  ["patient":
                        ["country_code":(Locale.current as NSLocale).ISDCode(), //"+91",
                          "phone_number": mobileNumber,
                        ]
                     ]
        makeHttpPostRequestWith(url: "/patients/send_otp", dataDict: dict, shouldAuthorize: false, successBlock: { (success, response, statusCode) in
            if let dictResult = response as? [String:Any],
                let _ = dictResult["response"]as? [String:Any]{
                    completion(success)
            }
            completion(false)
        }) { (message, statusCode) in
             failure(message)
        }
    }
    
    //MARK:- Verify Passcode
    static func verifyPasscodeFor(mobileNumber:String, with code:String, completion:@escaping (_ success:Bool, _ patient:Patient?, _ isCardShow:Bool)->Void, failure:@escaping (_ message:String)-> Void){
        //let deviceToken = "asdasdasdasdasdadasda66bnnbnbnnbnbnjbhhhasdasd"
//        print("Token- \(DefaultParams.deviceToken)")
        let dict =  ["patient":
            ["country_code":(Locale.current as NSLocale).ISDCode(),//"+91",
             "phone_number": mobileNumber,
             "otp": code,
            ],
                     "device":
                        ["uuid":DefaultParams.uuid,
                         "token":DefaultParams.deviceToken
            ]
        ]
        makeHttpPostRequestWith(url: "/patients/verify_otp", dataDict: dict, shouldAuthorize: false, successBlock: { (success, result, statusCode) in
            //check here for type doctor and user
            if let dictResult = result as? [String:Any],
                let json = dictResult["patient"]as? [String:Any],
                let authToken = json["auth_token"]as? String,
                let refreshTokne = json["refresh_token"]as? String,
                let paymentSource = json["payment_sources"]as? [[String:Any]]
                {
                
                if let pateint = Patient(json: json){
                    AppUser.authToken = authToken
                    AppUser.refreshToken = refreshTokne
                    AppUser.isLoggedIn = true
                    AppUser.patient = pateint
                    AppUser.userType = UserType.kPatient
                    
                    //let savedPatient = AppUser.patient
                    let isCardAdded = paymentSource.count > 0
                    completion(true,pateint,isCardAdded)
                }
                
            }else{
                completion(false,nil,false)
            }
        }) { (message, statusCode) in
           failure(message)
        }
    }
    //MARK: Logout
    
    static func logoutPatient(id:Int,_ completion:@escaping (_ status:Bool)-> Void, failure:@escaping(_ status:Bool)-> Void){
        
        makeGetHttpRequestWith(url: "/patients/\(id)/logout", dataDict: nil, successBlock: { (success, json, statusCode) in
            completion(true)
        }) { (message, statusCode) in
            failure(false)
        }
        
    }
    
    //MARK:- Add Patients Card Detail
    
    static func addPatientsCardDetail(stripeToken:String,patientId:Int, completion:@escaping (_ success:Bool)->Void, failure:@escaping (_ message:String)-> Void){
        let dict =  ["patient":
              [
                "token":stripeToken
              ]
         ]
        makeHttpPostRequestWith(url: "/patients/\(patientId)/payment_sources", dataDict: dict, shouldAuthorize: true, successBlock: { (success, result, statusCode) in
            if let dictResult = result as? [String:Any]{
             //save payments related detail
//                print(dictResult)
                completion(true)
            }else{
                completion(false)
            }
        }) { (message, statusCode) in
            failure(message)
        }
    }
    
    
    //MARK:- Get Doctor List
    static func searchDoctorForPatient(specialization:String, with pageIndix:Int, hotDoc:Bool, completion:@escaping (_ success:Bool, _ doctor:DoctorList?)->Void, failure:@escaping (_ message:String)-> Void){
        
        let url = "/doctors?search=\(specialization)&page=\(pageIndix)&hot_list=\(hotDoc)"
        makeGetHttpRequestWith(url: url, dataDict: nil, successBlock: { (success, result, statusCode) in
            
            if let dictResult = result as? [String:Any]
                //let doctorHash = dictResult["doctors"]as? [[String:Any]]
            {
                let doctorList = DoctorList(json:dictResult)
                completion(success,doctorList!)
                
//                    let arrDoctor = doctorHash.map({ (element) -> Doctor? in
//                        Doctor(json: element)
//                    }).flatMap{$0}
//                    completion(success,arrDoctor)
 
            }else{
                completion(false, nil)
            }
        
        }) { (message, statusCode) in
            failure(message)
        }
    }
    
    
    
    
    //MARK:- Set Profile for Patient Data
    static func setProfileDataForPatient(id:Int,patientDataDict:[String:Any], completion:@escaping (_ succeed:Bool)->Void, failure:@escaping (_ message:String)->Void){
        
        makePutHttpRequestWith(url: "/patients/" + String(id), dataDict: patientDataDict, successBlock: { (success, response, statusCode) in
            
            if let dictResult = response as? [String:Any],
                let json = dictResult["patient"]as? [String:Any]{
                if let pateint = Patient(json: json){
                    AppUser.patient = pateint
                    completion(success)
                }
            }else{
                completion(false)
            }
            
        }) { (message, statusCode) in
            failure(message)

        }
             
    }
    
    //MARK:- Get Appointments for Patients (Upcoming and Completed case)
    static func getAppointmentsForPatients(id:Int, _ completion:@escaping (_ upcoming:[Appointment], _ completed:[Appointment])-> Void, failure:@escaping (_ message:String)->Void){

        makeGetHttpRequestWith(url: "/patients/\(id)/appointments", dataDict: nil, successBlock: { (success, json, statusCode) in
            
            if let dict = json as? [String:Any],
                let activeAppointments = dict["active_appointments"]as? [[String:Any]],
                let inActiveAppointments = dict["inactive_appointments"]as? [[String:Any]]
            {
                let upcomingAppointments = activeAppointments.map({ (element:[String:Any]) -> Appointment? in
                    Appointment(json: element)
                }).flatMap{$0}
                
                let completedAppointments = inActiveAppointments.map({ (element:[String:Any]) -> Appointment? in
                    Appointment(json: element)
                }).flatMap{$0}
                
                completion(upcomingAppointments, completedAppointments)
            }else{
                completion([],[])
            }
            
        }) { (message, statusCode) in
            failure(message)
        }
    }
    
    // Chat list
    static func getChatsForPatient(uuids:[String], _ completion:@escaping (_ chatList:[ChatUser])-> Void, failure:@escaping (_ message:String)->Void){
        
        let join = uuids.joined(separator: ",")
        
        makeGetHttpRequestWith(url: "/calls/contacts?uuid=\(join)", dataDict: nil, successBlock: { (success, json, statusCode) in
            
            if let dict = json as? [String:Any],
                let patientChatList = dict["contacts"]as? [[String:Any]]
            {
                let chatUsers = patientChatList.map({ (element:[String:Any]) -> ChatUser? in
                    ChatUser(json: element)
                }).flatMap{$0}
                
                completion(chatUsers)
            }else{
                completion([])
            }
            
        }) { (message, statusCode) in
            failure(message)
        }
    }
    
    // Notification List
    static func getNotificationForPatient(id:Int, _ completion:@escaping (_ notification:[LocalNotification])-> Void, failure:@escaping (_ message:String)->Void){
        
        makeGetHttpRequestWith(url: "/patients/\(id)/notifications", dataDict: nil, successBlock: { (success, json, statusCode) in
            
            if let dict = json as? [String:Any],
                let notifications = dict["notifications"]as? [[String:Any]]
            {
                let notification = notifications.map({ (element:[String:Any]) -> LocalNotification? in
                    LocalNotification(json: element)
                }).flatMap{$0}
                
                completion(notification)
            }else{
                completion([])
            }
            
        }) { (message, statusCode) in
            failure(message)
        }
    }
    //MARK:- Submit Feedback and Review for Doctor
    static func submitFeedbackDetail(patientComment:String, ratingForDoctor:Float , with appointmentId:Int, completion:@escaping (_ success:Bool, _ feedback:Feedback?)->Void, failure:@escaping (_ message:String)-> Void){

        let dict =  ["feedback":
            ["comment":patientComment,
             "rating": ratingForDoctor,
            ]
        ]
        makeHttpPostRequestWith(url: "/appointments/\(appointmentId)/feedbacks", dataDict: dict, shouldAuthorize: true, successBlock: { (success, result, statusCode) in
            if let dictResult = result as? [String:Any],
                let json = dictResult["feedback"]as? [String:Any]{
                if let feedback = Feedback(json: json){
                    completion(success,feedback)
                }else{
                    completion(true,nil)
                }
            }else{
                completion(false,nil)
            }
        }) { (message, statusCode) in
            failure(message)
        }
    }
    
 static func getChatUserDetails(uuid:String) -> ChatUser? {
    
    let chatUser = AppUser.chatUser
    let user = chatUser?.filter( { return $0.uuid == uuid } )
    if user != nil{
        let noOfUser:Int = (user?.count)!
        if noOfUser > 0{
            return user?.last!
        }else{
            return nil
        }
        }
        return nil
    }
    
}



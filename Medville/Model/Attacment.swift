//
//  Attacment.swift
//  Medville
//
//  Created by Durgesh on 01/12/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import Foundation

struct Attachment:Codable {
    var id : Int
    var url : URL?
    var mime_type : String
    var thumbnail_url : URL?
    
    init?(json:[String:Any]){
        guard
            let id = json["id"]as? Int,
            let mime_type = json["mime_type"]as? String
            else{
                return nil
        }
        
        if let imageurl = json["url"]as? String,
            let fullImageUrl  = URL(string: kBaseURL + imageurl){
            self.url = fullImageUrl
        }
        if let thumbnail = json["thumbnail_url"]as? String,
            let fullUrl = URL(string: kBaseURL + thumbnail){
            self.thumbnail_url = fullUrl
        }
        self.id = id
        self.mime_type = mime_type
    }
}

//
//  Feedback.swift
//  Medville
//
//  Created by Sunder Singh on 10/13/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import Foundation

struct Feedback:Codable{
    var id:Int
    var rating:Float
    var comment:String
    var created_at:String
    var patient:Patient
    
    init?(json:[String:Any]){
        guard let id = json["id"]as? Int,
            let rating = json["rating"]as? Float,
            let comment =  json["comment"]as? String,
            let created_at = json["created_at"]as? String,
           // let doctorJson = json["doctor"]as? [String:Any],
          //  let doctor = Doctor(json: doctorJson),
            let pateintJson = json["patient"] as? [String:Any],
            let pateint = Patient(json: pateintJson)
            else{
                return nil
        }
        self.id = id
        self.rating = rating
        self.comment = comment
        self.created_at = created_at
//        self.doctor = doctor
        self.patient = pateint
    }
}

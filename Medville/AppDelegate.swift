 //
//  AppDelegate.swift
//  Medville
//
//  Created by Jitendra Solanki on 7/7/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit
import Stripe
import UserNotifications
import PushNotifications
import TwilioVoice
import PushKit

//import MFSideMenu
//import OpenTok
//import Stripe

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    let pushNotifications = PushNotifications.shared

    var window: UIWindow?
    var userDefault = UserDefaults.standard
    //var container = MFSideMenuContainerViewController()
   // var currentCall: VideoCall?
    //var session: OTSession?
    var deviceToken:Data?
    var identity:String?
    
    // Video Chat
    //Create Room Name
    var roomName = ""
    var videoCallerName = ""
    var twilioTokenForVideo = ""
    var isVideoCall:Bool = false
    
    //Check video started from calling state
    var checkVideoFromTerminationState = false
    
    
    
    private(set) var UUID: String = ""{
        didSet{
            if UUID.count<32 {
                self.UUID = oldValue
            }
        }
    }
    //MARK:- App Setup
    private func setupApp(){
        UIApplication.shared.statusBarStyle = .lightContent
        UINavigationBar.appearance().barTintColor = UIColor(r: 114, g: 139, b: 228, alpha: 1.0)
        UINavigationBar.appearance().tintColor = UIColor(r: 247, g: 248, b: 253, alpha: 1.0)
        UINavigationBar.appearance().titleTextAttributes  = [NSAttributedStringKey.foregroundColor:UIColor(r: 247, g: 248, b: 253, alpha: 1.0),NSAttributedStringKey.font: UIFont.systemFont(ofSize: 20.0, weight: UIFont.Weight.bold)]
    
    }
    
    //MARK:-
    func logInForUser(type:UserType){
        
        let vcidentifier = type == .kDocotor ? VCIdentifier.kDoctorLogIn : VCIdentifier.kPatientLogIn
        let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: vcidentifier)
        self.window?.rootViewController = vc
        self.window?.makeKeyAndVisible()

    }
    
    func logoutForUser(type:UserType){
        let identifier = type == .kDocotor ? VCIdentifier.kDoctorLogIn : VCIdentifier.kPatientLogIn
        
        ChatManager._sharedManager.logout()
        AppUser.isLoggedIn = false
        AppUser.authToken = nil
        
        let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: identifier)
        self.window?.rootViewController = vc
        self.window?.makeKeyAndVisible()

    }
    
    func setHomePageForUser(type:UserType){
        let vcidentifier = type == .kDocotor ? VCIdentifier.kDoctorTabBar : VCIdentifier.kPatientTabBar
        let tabBarVc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: vcidentifier)
        self.window?.rootViewController = nil
        self.window?.rootViewController = tabBarVc
        self.window?.makeKeyAndVisible()
    }
    
    //subscribe for notification
    func registerOnPusher(identity:String){
        self.identity = identity
        if (self.deviceToken != nil) {
            self.pushNotifications.unsubscribeAll()
            self.pushNotifications.registerDeviceToken(self.deviceToken!) {
                try? self.pushNotifications.subscribe(interest: identity)
                
            }
        }
        
    }
    
    func configureUserNotifications() {
        
        let rejectAction = UNNotificationAction(identifier: "reject",
                                                title: "Reject",
                                                options: UNNotificationActionOptions(rawValue: 0))
        let acceptAction = UNNotificationAction(identifier: "accept",
                                              title: "Accept",
                                              options: .foreground)
        
        let actionCategory = UNNotificationCategory(identifier: "ACTIONABLE",
                                                     actions: [rejectAction,acceptAction],
                                                     intentIdentifiers: [],
                                                     options: .customDismissAction)
        
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
        }
            center.setNotificationCategories([actionCategory])
    }
    
    
//MARK:-
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // PushNotification by pusher
        self.pushNotifications.start(instanceId: "5fca81ce-ef08-464a-8014-a285b706f5dc")
        self.pushNotifications.registerForRemoteNotifications()
        
//        // For Voice notification
        var voipRegistry:PKPushRegistry = PKPushRegistry.init(queue: DispatchQueue.main)
        voipRegistry.delegate = self
        voipRegistry.desiredPushTypes = Set([PKPushType.voIP])

        self.configureUserNotifications()
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
       
        //register for push notification
        self.registerForPushNotification()
        
        setupApp()
        
        // Check login status.
        if let userType = AppUser.userType {
            AppUser.isLoggedIn ? setHomePageForUser(type: userType) : logInForUser(type: userType)
        }else{
            let patientLogIn = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: VCIdentifier.kPatientLogIn)
            self.window?.rootViewController = patientLogIn
        }
        
//        if userDefault.bool(forKey: kLoginStatusKey) {
//            if let userDict = userDefault.value(forKey: kUserDetailsKey) as? [String: Any] {
//                AppUser().updateUserDetails(detailsDict: userDict)
//            }
//            // Check role.
//            if (AppUser.type == kUSER) {
//                userLoggedInSetup()
//            
//            }else if  (AppUser.type == kDOCTOR) {
//                doctorLoggedInSetup()
//            }else{
//                logoutLocally()
//            }
//        }
//        else{
//            logoutLocally()
//        }
        //old key
        //pk_test_6pRNASCoBOKtIshFeQd4XMUh
        //secret_key: 'sk_live_kPA8hzldrXcjaFXZuBwMCJsy'
        self.window?.makeKeyAndVisible()
        // Production
        STPPaymentConfiguration.shared().publishableKey = "pk_live_cg8Cl0NfNQBZhaAuF9WFTH44"
// Staging
//        STPPaymentConfiguration.shared().publishableKey = "pk_test_a32u7WleRjl8hMjeyq6OioxY"
        return true
    }
    
//    fileprivate func processError(_ error: OTError?) {
//        if let err = error {
//            print("\(err.localizedDescription)")
//        }
//    }

//MARK:- -------------- Login and Logout setup ---------------
// Setup for 'USER' functionalities and screens.
    func userLoggedInSetup() {
        userDefault.set(true, forKey: kLoginStatusKey)
        userDefault.synchronize()
        let doctorVC = DoctorListViewController(nibName: "DoctorListViewController", bundle: Bundle.main)
        let navVC = UINavigationController(rootViewController: doctorVC)
        self.window?.rootViewController = navVC;
    }
    
// Setup for 'Doctor' functionalities and screens.
    func doctorLoggedInSetup() {
        userDefault.set(true, forKey: kLoginStatusKey)
        userDefault.synchronize()
        let doctorProfile = DoctorProfileController(nibName: "DoctorProfileController", bundle: Bundle.main)
        self.window?.rootViewController = doctorProfile;
    }
    
// Setup Logout screens and data without signing out from backend.
    func logoutLocally(){
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        self.userDefault.set(nil, forKey: kUserDetailsKey)
        self.userDefault.set(false, forKey: kLoginStatusKey)
        self.userDefault.synchronize()
        let login = LoginController(nibName: "LoginController", bundle: Bundle.main)
        self.window?.rootViewController = login
    }
    
//MARK:- --------- Register and Handle remot enotifications ----------
    func registerForPushNotification(){
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
            if granted {
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
    }
    
    //---------UNUserNotificationCenter Delegates-----------
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
      
        self.deviceToken = deviceToken
            ChatManager._sharedManager.updateToken(token: deviceToken)
        if self.identity != nil{
            self.registerOnPusher(identity: self.identity!)
        }
        
        let token = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        // Second format: format: "%02.2hhx"
        self.UUID = token
        debugPrint(token)
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
//        print(error.localizedDescription)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if let userInformation = response.notification.request.content.userInfo as? [String: Any]
        {
            if userInformation["room"] != nil{
                if !self.isVideoCall {
            self.roomName = userInformation["room"] as! String
            self.videoCallerName = userInformation["sender_name"] as! String
            if let destinationVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: VCIdentifier.kVideoVC) as? VideoViewController {
                AppUser.accessTokenForTwilio = userInformation["video_chat_token"] as? String
                self.checkVideoFromTerminationState = true
                self.isVideoCall = true
                self.window?.rootViewController  = destinationVC
                    }
                }
            }else{
                if let _: UIViewController = UIApplication.topViewController(){
                                }
                        (window?.rootViewController as! UITabBarController).selectedIndex = 2
            }
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        print("Handle push from foreground")
        print("\(notification.request.content.userInfo)")
        if let notificationInfo = notification.request.content.userInfo as? [String: Any]{
            print("\(notificationInfo)")
            
            if notificationInfo["room"] != nil{
            self.roomName = notificationInfo["room"] as! String
            self.videoCallerName = notificationInfo["sender_name"] as! String
            
            if let pokeType = notificationInfo["poke_type"] as? String {
                
                if pokeType == "request_disconnect" {
                    if self.isVideoCall{
                    self.isVideoCall = false
                    let top = self.topViewController(controller: UIApplication.shared.keyWindow?.rootViewController)
                    
                    if (top?.isKind(of: VideoViewController.self))!{
                        
                        let destinationVC = top as! VideoViewController
                        if (destinationVC.timer.isValid) {
                            destinationVC.timer.invalidate()
                                    }
                        //destinationVC.callKitCompletionHandler = nil

                            top?.dismiss(animated: true, completion: nil)
                        if checkVideoFromTerminationState {
                            exit(0)
                        }
                    }
                    }
                }
                else {
                    if let destinationVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: VCIdentifier.kVideoVC) as? VideoViewController {
                        if !isVideoCall{
                            self.isVideoCall = true
                        AppUser.accessTokenForTwilio = notificationInfo["video_chat_token"] as? String
                        if let window = self.window , let rootViewController = window.rootViewController {
                            var currentController = rootViewController
                            while let presentedController = currentController.presentedViewController {
                                currentController = presentedController
                            }
                            currentController.present(destinationVC, animated: true, completion: nil)
                        }
                        }
                    }
                }
            }
        }
        }
    }
    
     func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
       
        // Get remote message
        if let aps = userInfo[AnyHashable("aps")] as? [AnyHashable: Any] {
            let alert = aps[AnyHashable("alert")] != nil ? aps[AnyHashable("alert")] as! NSDictionary : [:] as NSDictionary
           let top = self.topViewController(controller: UIApplication.shared.keyWindow?.rootViewController)
            
            if (top?.isKind(of: ChatViewController.self))!{
                
            }else{
                if var topController = UIApplication.shared.keyWindow?.rootViewController {
                    while let presentedViewController = topController.presentedViewController {
                        topController = presentedViewController
                    }
                    if topController.isKind(of: UITabBarController.self)
                    {
                    let tab = topController as! UITabBarController
                    let item = tab.tabBar.items![2]
                    item.badgeValue = "1"
                    }else{
                        let persent = UIApplication.shared.keyWindow?.rootViewController
                        if (persent?.isKind(of: UITabBarController.self))!
                        {
                            let tab = persent as! UITabBarController
                            let item = tab.tabBar.items![2]
                            item.badgeValue = "1"
                        }
                    }
                }
            }
        }
        
    }
    
    func showAlertWithTitle(title: String, message: String ) {
        if let vc: UIViewController = UIApplication.topViewController(){
            let alertVC = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (_ action) in
            })
            alertVC.addAction(alertAction)
            vc.present(alertVC, animated: true, completion: nil)
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {

    }
    func applicationDidBecomeActive(_ application: UIApplication) {
        application.applicationIconBadgeNumber = 0
    }

    func applicationWillTerminate(_ application: UIApplication) {
        
    }
}
 
 // Push Handle
 extension AppDelegate:PKPushRegistryDelegate{
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType) {
        if (type == PKPushType.voIP) {
            let audioClass = AudioViewController.shared
            TwilioVoice.handleNotification(payload.dictionaryPayload, delegate: audioClass)
        }
    }
    
 }
 
 /*
 extension AppDelegate: OTSessionDelegate {
    func sessionDidConnect(_ session: OTSession) {
        print("Session connected")
        print("Connection: \(String(describing: session.connection))")
    }
    
    func sessionDidDisconnect(_ session: OTSession) {
        print("Session disconnected")
    }
    
    func session(_ session: OTSession, streamCreated stream: OTStream) {
        print("Session streamCreated: \(stream.streamId)")
        print("Session created: \(stream.creationTime)")
        print("Session name: \(String(describing: stream.name))")
    }
    
    func session(_ session: OTSession, streamDestroyed stream: OTStream) {
        print("Session streamDestroyed: \(stream.streamId)")
    }
    
    func session(_ session: OTSession, didFailWithError error: OTError) {
        print("session Failed to connect: \(error.localizedDescription)")
    }
    
    func session(_ session: OTSession, connectionCreated connection: OTConnection) {
        print("Connection id: \(connection.connectionId)")
        print("Connection data: \(String(describing: connection.data))")
    }
    
    func session(_ session: OTSession, connectionDestroyed connection: OTConnection) {
        print("Connection id: \(connection.connectionId)")
        print("Connection data: \(String(describing: connection.data))")
    }
    
    func session(_ session: OTSession, receivedSignalType type: String?, from connection: OTConnection?, with string: String?) {
        print("Connection id: \(String(describing: connection?.connectionId))")
        print("Connection data: \(String(describing: connection?.data))")
        print("Signal type: \(String(describing: type))")
        print("Signal String: \(String(describing: string))")
    }
 }
 
 */


//
//  DoctorProfileController.swift
//  Medville
//
//  Created by Jitendra Solanki on 7/10/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

class DoctorProfileController: UIViewController {
    
    //Profile
    
    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var lbl_doctorName: UILabel!
    @IBOutlet weak var lbl_specialistityTitle: UILabel!
    @IBOutlet weak var view_rating: HCSStarRatingView!
    @IBOutlet weak var tableView_profile: UITableView!
    //History
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var videoCallHistroy_tableView:UITableView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var roleLabel: UILabel!
    
    @IBOutlet weak var myVideosButton: UIButton!
    
    @IBOutlet weak var reviewButton: UIButton!
    
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var buttonBGView: UIView!
    
    let historyCellIdentifier = "history_cell"
    let profileCellIdentifier = "profile_cell"
    override func viewDidLoad() {
        super.viewDidLoad()
        self.profileImageView.layer.cornerRadius = UIScreen.main.bounds.size.width * 0.14
        self.profileImageView.layer.masksToBounds = true
        setupUI()
    }
   @IBAction func logoutButtonAction(_ sender: UIButton){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.logoutLocally()
    }
    //MARK:- Private helper methods
    private func setupUI(){
        nameLabel.text = AppUser.firstname
        roleLabel.text = AppUser.title.capitalized
        ratingLabel.text = "Rating: 4.3"
//        print(AppUser.imageUrlString)
        let url = URL(string: AppUser.imageUrlString)
        self.profileImageView.sd_setImage(with: url)
        lbl_doctorName.text = AppUser.firstname
        lbl_specialistityTitle.text = AppUser.title.capitalized
        
        configureTableView()
    }
    
    private func configureTableView(){
        let nib = UINib(nibName: "HistoryCustomCell", bundle: Bundle.main)
        videoCallHistroy_tableView.register(nib, forCellReuseIdentifier: historyCellIdentifier)
        
        let profileNib = UINib(nibName: "ProfileTableViewCell", bundle: Bundle.main)
        tableView_profile.register(profileNib, forCellReuseIdentifier: profileCellIdentifier)
    }

    //MARK:- Action methods
    @IBAction func segmentControllerValueChanged(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0{
            viewProfile.isHidden = true
        }else{
            viewProfile.isHidden = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension DoctorProfileController:UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
          if tableView == self.tableView_profile{
            return 3
         }else{
            return 10
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableView_profile {
            let cell = tableView.dequeueReusableCell(withIdentifier: profileCellIdentifier, for: indexPath)as! ProfileTableViewCell
            switch indexPath.row {
            case 0:
                cell.imgV.image = UIImage(named: "location")
                cell.lbl_title.text = "Vally Neurosurgical Inc. 501, S Buena Vista St urbank, CA 9150"
            case 1:
                cell.imgV.image = UIImage(named: "mobile_no")
                cell.lbl_title.text = "(818) 839-2411"
            case 2:
                cell.imgV.image = UIImage(named: "doctor")
                cell.lbl_title.text = "Male | 39 y/o"
            default:
                cell.lbl_title.text = ""
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: historyCellIdentifier, for: indexPath)as! HistoryCustomCell
            return cell
        }
    }
}

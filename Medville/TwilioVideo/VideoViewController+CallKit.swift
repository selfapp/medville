//
//  VideoViewController+CallKit.swift
//  Poached
//
//  Created by Rajeev on 16/02/18.
//  Copyright © 2018 Rajeev. All rights reserved.
//

import UIKit
import TwilioVideo
import CallKit
import AVFoundation

extension VideoViewController : CXProviderDelegate {
    
    func providerDidReset(_ provider: CXProvider) {
        
        // AudioDevice is enabled by default
        self.audioDevice.isEnabled = true
        
        room?.disconnect()
    }
    
    func providerDidBegin(_ provider: CXProvider) {
       // logMessage(messageText: "providerDidBegin")
    }
    
    func provider(_ provider: CXProvider, didActivate audioSession: AVAudioSession) {
      //  logMessage(messageText: "provider:didActivateAudioSession:")
        
        self.audioDevice.isEnabled = true
    }
    
    func provider(_ provider: CXProvider, didDeactivate audioSession: AVAudioSession) {
//        logMessage(messageText: "provider:didDeactivateAudioSession:")
    }
    
    func provider(_ provider: CXProvider, timedOutPerforming action: CXAction) {
//        logMessage(messageText: "provider:timedOutPerformingAction:")
    }
    
    func provider(_ provider: CXProvider, perform action: CXStartCallAction) {
//        logMessage(messageText: "provider:performStartCallAction:")
        
        /*
         * Configure the audio session, but do not start call audio here, since it must be done once
         * the audio session has been activated by the system after having its priority elevated.
         */
        
        // Stop the audio unit by setting isEnabled to `false`.
        self.audioDevice.isEnabled = false;
        
        // Configure the AVAudioSession by executign the audio device's `block`.
        self.audioDevice.block()
        
        callKitProvider.reportOutgoingCall(with: action.callUUID, startedConnectingAt: nil)
        
        performRoomConnect(uuid: action.callUUID, roomName: action.handle.value) { (success) in
            if (success) {
                
                //Call API for send room name to the user whom you want to call
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
//Doctor
                if AppUser.userType == UserType.kDocotor {
                    let arrUdid = appDelegate.roomName.components(separatedBy: "_")

                    let receiverUdis = arrUdid[1]
                    let receiverName:String = (AppUser.doctor?.first_name)!
                    let dictParams = ["receiver_uuid": receiverUdis,"payload":["poke_type":"request_connect","message":"\(receiverName) is calling you"]] as [String : Any]

                    Doctor.sendRoomNameForNotification(dict: dictParams, completion: { (message) in
                       // self.showAlertWithTitle(title: "", message: message)
                    }, failure: { (message) in
                       // self.showAlertWithTitle(title: "", message: message)
                    })
                }
               
                provider.reportOutgoingCall(with: action.callUUID, connectedAt: Date())
                action.fulfill()
            } else {
                action.fail()
            }
        }
    }
    
    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
//        logMessage(messageText: "provider:performAnswerCallAction:")
        
        /*
         * Configure the audio session, but do not start call audio here, since it must be done once
         * the audio session has been activated by the system after having its priority elevated.
         */
        
        // Stop the audio unit by setting isEnabled to `false`.
        self.audioDevice.isEnabled = false;
        
        // Configure the AVAudioSession by executign the audio device's `block`.
        self.audioDevice.block()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        performRoomConnect(uuid: action.callUUID, roomName:  appDelegate.roomName) { (success) in
            if (success) {
                action.fulfill(withDateConnected: Date())
            } else {
                action.fail()
            }
        }
    }
    
    func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        NSLog("provider:performEndCallAction:")
        
        // AudioDevice is enabled by default
        self.audioDevice.isEnabled = true
        room?.disconnect()
        action.fulfill()
    }
    
    func provider(_ provider: CXProvider, perform action: CXSetMutedCallAction) {
        NSLog("provier:performSetMutedCallAction:")
        
        toggleMic(sender: self)
        
        action.fulfill()
    }
    
    func provider(_ provider: CXProvider, perform action: CXSetHeldCallAction) {
        NSLog("provier:performSetHeldCallAction:")
        
        let cxObserver = callKitCallController.callObserver
        let calls = cxObserver.calls
        
        guard let call = calls.first(where:{$0.uuid == action.callUUID}) else {
            action.fail()
            return
        }
        
        if call.isOnHold {
            holdCall(onHold: false)
        } else {
            holdCall(onHold: true)
        }
        action.fulfill()
    }
}

// MARK: Call Kit Actions
extension VideoViewController {
    
    func performStartCallAction(uuid: UUID, roomName: String?) {
        let callHandle = CXHandle(type: .generic, value: roomName ?? "")
        let startCallAction = CXStartCallAction(call: uuid, handle: callHandle)
        
        startCallAction.isVideo = true
        
        let transaction = CXTransaction(action: startCallAction)
        
        callKitCallController.request(transaction)  { error in
            if let error = error {
                NSLog("StartCallAction transaction request failed: \(error.localizedDescription)")
                return
            }
            NSLog("StartCallAction transaction request successful")
        }
    }
    
    func performEndCallAction(uuid: UUID) {
        let endCallAction = CXEndCallAction(call: uuid)
        let transaction = CXTransaction(action: endCallAction)
        
        callKitCallController.request(transaction) { error in
            if let error = error {
                NSLog("EndCallAction transaction request failed: \(error.localizedDescription).")
                return
            }
            NSLog("EndCallAction transaction request successful")
        }
        // self.stopRing()
    }
    
    func performRoomConnect(uuid: UUID, roomName: String? , completionHandler: @escaping (Bool) -> Swift.Void) {
        // Configure access token either from server or manually.    
        // Prepare local media which we will share with Room Participants.
        self.prepareLocalMedia()
        
        // Preparing the connect options with the access token that we fetched (or hardcoded).
        let connectOptions = TVIConnectOptions.init(token: AppUser.accessTokenForTwilio!) { (builder) in

            // Use the local media that we prepared earlier.
            builder.audioTracks = self.localAudioTrack != nil ? [self.localAudioTrack!] : [TVILocalAudioTrack]()
            builder.videoTracks = self.localVideoTrack != nil ? [self.localVideoTrack!] : [TVILocalVideoTrack]()
            
            // The name of the Room where the Client will attempt to connect to. Please note that if you pass an empty
            // Room `name`, the Client will create one for you. You can get the name or sid from any connected Room.
            builder.roomName = roomName
            
            // The CallKit UUID to assoicate with this Room.
            builder.uuid = uuid
        }
        
        // Connect to the Room using the options we provided.
        room = TwilioVideo.connect(with: connectOptions, delegate: self)
        
//        logMessage(messageText: "Attempting to connect to room \(String(describing: roomName))")
        
    self.showRoomUI(inRoom: true)
        
        self.callKitCompletionHandler = completionHandler
    }
}

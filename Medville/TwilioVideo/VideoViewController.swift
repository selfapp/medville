//
//  VideoViewController.swift
//  Poached
//
//  Created by Rajeev on 16/02/18.
//  Copyright © 2018 Rajeev. All rights reserved.
//

import UIKit
import TwilioVideo
import CallKit

class VideoViewController: UIViewController {

  
    // Configure remote URL to fetch token from
    var tokenUrl = "http://localhost:8000/token.php"
    
    // Video SDK components
    var room: TVIRoom?
    /**
     * We will create an audio device and manage it's lifecycle in response to CallKit events.
     */
    var audioDevice: TVIDefaultAudioDevice = TVIDefaultAudioDevice()
    var camera: TVICameraCapturer?
    var localVideoTrack: TVILocalVideoTrack?
    var localAudioTrack: TVILocalAudioTrack?
    var remoteParticipant: TVIRemoteParticipant?
    //var remoteView: TVIVideoView?
     @IBOutlet weak var remoteView: TVIVideoView!
    // CallKit components
    let callKitProvider: CXProvider
    let callKitCallController: CXCallController
    var callKitCompletionHandler: ((Bool)->Swift.Void?)? = nil
    
    //For audio ringing
    var ringPlayer: AVAudioPlayer?

    //Timer for controlling video calling duration
    var timer = Timer()
    
    // MARK: UI Element Outlets and handles
    @IBOutlet weak var btn_rejectCall:UIButton!
    @IBOutlet weak var btn_acceptCall:UIButton!
    @IBOutlet weak var lbl_connecting: UILabel!
    @IBOutlet weak var lbl_userName: UILabel!
    @IBOutlet weak var micButton: UIButton!
    // `TVIVideoView` created from a storyboard
    @IBOutlet weak var previewView: TVIVideoView!
    @IBOutlet weak var fullScreenPreviewView: TVIVideoView!
    
    required init?(coder aDecoder: NSCoder) {
        let configuration = CXProviderConfiguration(localizedName: "CallKit Quickstart")
        configuration.maximumCallGroups = 1
        configuration.maximumCallsPerCallGroup = 1
        configuration.supportsVideo = true
        
        callKitProvider = CXProvider(configuration: configuration)
        callKitCallController = CXCallController()
        super.init(coder: aDecoder)
        callKitProvider.setDelegate(self, queue: nil)
    }
    
    deinit {
        // CallKit has an odd API contract where the developer must call invalidate or the CXProvider is leaked.
        callKitProvider.invalidate()
    }
    
    // MARK: UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*
         * The important thing to remember when providing a TVIAudioDevice is that the device must be set
         * before performing any other actions with the SDK (such as creating Tracks, or connecting to a Room).
         * In this case we've already initialized our own `TVIDefaultAudioDevice` instance which we will now set.
         */

       TwilioVideo.audioDevice = self.audioDevice
        
        if PlatformUtils.isSimulator {
            self.fullScreenPreviewView.removeFromSuperview()
           // self.previewView.removeFromSuperview()
        } else {
            // Preview our local camera track in the local video preview view.
            self.startPreview()
        }
        
        // Disconnect and mic button will be displayed when the Client is connected to a Room.
        
        self.micButton.isHidden = true
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        // Patient
        if AppUser.userType == UserType.kPatient{
            self.btn_rejectCall.setTitle("Reject", for: .normal)
            self.btn_acceptCall.isHidden = false
            self.lbl_userName.text = "Calling ..."
            self.lbl_connecting.text = appDelegate.videoCallerName
            self.lbl_userName.font = UIFont.boldSystemFont(ofSize: 17)
            self.lbl_connecting.font = UIFont.boldSystemFont(ofSize: 19)
            self.startRing()
        }
        else {
            // Doctor
            self.btn_acceptCall.isHidden = true
            self.btn_rejectCall.setTitle("End Call", for: .normal)
            self.lbl_userName.text = "Dialing ..."
            self.lbl_connecting.text = appDelegate.videoCallerName
            self.lbl_userName.font = UIFont.boldSystemFont(ofSize: 17)
            self.lbl_connecting.font = UIFont.boldSystemFont(ofSize: 19)

            self.timer = Timer.scheduledTimer(timeInterval: 70, target: self,   selector: (#selector(VideoViewController.checkVideoCallingStatus)), userInfo: nil, repeats: false)
            self.startDailerTone()
            performStartCallAction(uuid: UUID.init(), roomName: appDelegate.roomName)
        }
    }
    
    func setupRemoteVideoView() {
        
        
        //Remove full screen view
         self.fullScreenPreviewView.removeFromSuperview()
        //Show small preview view
        localVideoTrack!.addRenderer(self.previewView)
    }
    
    //Check video call is started or not if call is not started in 90 sec then end the call
    @objc func checkVideoCallingStatus() {
    
        if let room = room {
            if room.remoteParticipants.count == 0  {
                self.disconnect(sender: self.btn_rejectCall as AnyObject)
            }
        }
    }
    
    // MARK: IBActions
    @IBAction func disconnect(sender: AnyObject) {
        self.stopRing()
        if self.timer.isValid {
            self.timer.invalidate()
        }
        self.btn_rejectCall.isUserInteractionEnabled = false
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.isVideoCall = false
        if let room = room, let uuid = room.uuid {
           
            if room.remoteParticipants.count == 0 {

                let arrUdid = appDelegate.roomName.components(separatedBy: "_")

                let receiverUdis = arrUdid[1]
                let dictParams = ["receiver_uuid": receiverUdis,"payload":["poke_type":"request_disconnect"]] as [String : Any]

                // Call api for send notification
                Doctor.sendRoomNameForNotification(dict: dictParams, completion: { (message) in
                    self.performEndCallAction(uuid: uuid)
                   // self.showAlertWithTitle(title: "", message: message)
                }, failure: { (message) in
                    self.performEndCallAction(uuid: uuid)
                   // self.showAlertWithTitle(title: "", message: message)
                })
            }
            else {
               performEndCallAction(uuid: uuid)
            }
        }
        else {
            
            //Call API for send employer a notification that call is end by candidate

            if AppUser.userType == UserType.kPatient {
                let arrUdid = appDelegate.roomName.components(separatedBy: "_")

                let receiverUdis = arrUdid[0]
                let dictParams = ["receiver_uuid": receiverUdis,"payload":["poke_type":"request_disconnect","message":AppUser.patient?.firstName]] as [String : Any]
                
               // let dictParams = ["receiver_uuid": receiverUdis]
    
                // Send notification
                Doctor.sendRoomNameForNotification(dict: dictParams, completion: { (message) in
                   // self.showAlertWithTitle(title: "", message: message)
                    appDelegate.isVideoCall = false
                    if appDelegate.checkVideoFromTerminationState {
                       // appDelegate.window?.removeFromSuperview()
                        exit(0)
                    }
                    else {
                        self.dismiss(animated: true, completion: nil)
                    }
                }, failure: { (message) in
                  //  self.showAlertWithTitle(title: "", message: message)
                    appDelegate.isVideoCall = false
                    if appDelegate.checkVideoFromTerminationState {
                        // appDelegate.window?.removeFromSuperview()
                        exit(0)
                    }
                    else {
                        self.dismiss(animated: true, completion: nil)
                    }
                })
            }
            else {
                appDelegate.isVideoCall = false
                if appDelegate.checkVideoFromTerminationState {
                    // appDelegate.window?.removeFromSuperview()
                    exit(0)
                }
                else {
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func acceptCall(sender: AnyObject) {
     
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.isVideoCall = true
        performStartCallAction(uuid: UUID.init(), roomName: appDelegate.roomName)
    }
    
    @IBAction func toggleMic(sender: AnyObject) {
        if (self.localAudioTrack != nil) {
            self.localAudioTrack?.isEnabled = !(self.localAudioTrack?.isEnabled)!
            
            // Update the button title
            if (self.localAudioTrack?.isEnabled == true) {
                self.micButton.setImage(#imageLiteral(resourceName: "Button_Mute disabled"), for: .normal)
               // self.micButton.setTitle("Mute", for: .normal)
            } else {
                self.micButton.setImage(#imageLiteral(resourceName: "Button_Mute active"), for: .normal)
               // self.micButton.setTitle("Unmute", for: .normal)
            }
        }
    }
    
    
    //MARK:- Ringing methods
    func startRing() {
        ringPlayer?.stop()
        let soundPath = Bundle.main.path(forResource: "callRing", ofType: "mp3")
        if soundPath != nil {
            let rungURL = URL(fileURLWithPath: soundPath!)
            ringPlayer = try! AVAudioPlayer(contentsOf: rungURL)
            ringPlayer?.volume = 1.0
            ringPlayer?.numberOfLoops = -1
            if ringPlayer!.prepareToPlay() {
                ringPlayer?.play()
            }
        }
    }
    
    func startDailerTone() {
        ringPlayer?.stop()
        let soundPath = Bundle.main.path(forResource: "ring_ring", ofType: "mp3")
        if soundPath != nil {
            let rungURL = URL(fileURLWithPath: soundPath!)
            ringPlayer = try! AVAudioPlayer(contentsOf: rungURL)
            ringPlayer?.volume = 0.5
            ringPlayer?.numberOfLoops = -1
            if ringPlayer!.prepareToPlay() {
                ringPlayer?.play()
            }
        }
    }
    
    func stopRing() {
        ringPlayer?.stop()
        ringPlayer = nil
    }
 
    // MARK: Private
    func startPreview() {
        if PlatformUtils.isSimulator {
            return
        }
        
        // Preview our local camera track in the local video preview view.
        camera = TVICameraCapturer(source: .frontCamera, delegate: self)
        localVideoTrack = TVILocalVideoTrack.init(capturer: camera!)
        if (localVideoTrack == nil) {
           print("Failed to create video track")
        } else {
            // Add renderer to video track for local preview
            localVideoTrack!.addRenderer(self.fullScreenPreviewView)
          //  localVideoTrack!.addRenderer(self.previewView)

          //  localVideoTrack!.addRenderer(self.previewView)
            print("Video track created")
            // We will flip camera on tap.
//            let tap = UITapGestureRecognizer(target: self, action: #selector(VideoViewController.flipCamera))
//            self.previewView.addGestureRecognizer(tap)
        }
    }
    
    @IBAction func flipCamera(sender:UIButton) {
        if (self.camera?.source == .frontCamera) {
            self.camera?.selectSource(.backCameraWide)
        } else {
            self.camera?.selectSource(.frontCamera)
        }
    }
    
    func prepareLocalMedia() {
        // We will share local audio and video when we connect to the Room.
        
        // Create an audio track.
        if (localAudioTrack == nil) {
            localAudioTrack = TVILocalAudioTrack.init()
            
            if (localAudioTrack == nil) {
               // logMessage(messageText: "Failed to create audio track")
            }
        }
        
        // Create a video track which captures from the camera.
        if (localVideoTrack == nil) {
            self.startPreview()
        }
    }
    
    // Update our UI based upon if we are in a Room or not
    func showRoomUI(inRoom: Bool) {
        self.micButton.isHidden = !inRoom
        self.btn_rejectCall.isHidden = !inRoom
        
        if AppUser.userType == UserType.kPatient {
            self.btn_acceptCall.isHidden = inRoom
        }
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        UIApplication.shared.isIdleTimerDisabled = inRoom
    }
    func showCallerNameUI(inRoom:Bool) {
        
        self.lbl_connecting.isHidden = inRoom
        self.lbl_userName.isHidden = inRoom
    }
    func hideVideoCallView() {

        if self.navigationController != nil {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            self.navigationController?.popViewController(animated: true)
        }
        else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.isVideoCall = false
            if appDelegate.checkVideoFromTerminationState {
                // appDelegate.window?.removeFromSuperview()
                exit(0)
            }
            else {
                self.dismiss(animated: true, completion: nil)
            }
        }
//        let alertVC = UIAlertController.init(title: "", message: "Error in connection. Please try again.", preferredStyle: .alert)
//        let alertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (_ action) in
//
//            if self.navigationController != nil {
//                self.navigationController?.setNavigationBarHidden(false, animated: true)
//                self.navigationController?.popViewController(animated: true)
//            }
//            else {
//               self.dismiss(animated: true, completion: nil)
//            }
//        })
//        alertVC.addAction(alertAction)
//        self.present(alertVC, animated: true, completion: nil)
    }
    
    func cleanupRemoteParticipant() {
        if ((self.remoteParticipant) != nil) {
            if ((self.remoteParticipant?.videoTracks.count)! > 0) {
                let remoteVideoTrack = self.remoteParticipant?.remoteVideoTracks[0].remoteTrack
                remoteVideoTrack?.removeRenderer(self.remoteView!)
                self.remoteView?.removeFromSuperview()
                self.remoteView = nil
//                remoteVideoTrack?.removeRenderer(self.fullScreenPreviewView!)
//                self.fullScreenPreviewView?.removeFromSuperview()
//                self.fullScreenPreviewView = nil
            }
        }
//        self.remoteParticipant = nil
         self.fullScreenPreviewView = nil
    }
    
    func holdCall(onHold: Bool) {
        localAudioTrack?.isEnabled = !onHold
        localVideoTrack?.isEnabled = !onHold
    }
}

// MARK: TVIRoomDelegate
extension VideoViewController : TVIRoomDelegate {
    func didConnect(to room: TVIRoom) {
        
        // At the moment, this example only supports rendering one Participant at a time.
        if (room.remoteParticipants.count > 0) {
            self.remoteParticipant = room.remoteParticipants[0]
            self.remoteParticipant?.delegate = self
        }
        
        let cxObserver = callKitCallController.callObserver
        let calls = cxObserver.calls
        
        // Let the call provider know that the outgoing call has connected
        if let uuid = room.uuid, let call = calls.first(where:{$0.uuid == uuid}) {
            if call.isOutgoing {
                callKitProvider.reportOutgoingCall(with: uuid, connectedAt: nil)
            }
        }
        
        self.callKitCompletionHandler!(true)
    }
    
    func room(_ room: TVIRoom, didDisconnectWithError error: Error?) {
        
        self.cleanupRemoteParticipant()
        self.room = nil
        self.callKitCompletionHandler = nil

        if self.navigationController != nil {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            self.navigationController?.popViewController(animated: true)
        }
        else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.isVideoCall = false
            if appDelegate.checkVideoFromTerminationState {
                // appDelegate.window?.removeFromSuperview()
                exit(0)
            }
            else {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func room(_ room: TVIRoom, didFailToConnectWithError error: Error) {
        print("Failed to connect to room with error: \(error.localizedDescription)")
       
        performEndCallAction(uuid: room.uuid!)
        if self.navigationController != nil {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            self.navigationController?.popViewController(animated: true)
        }
        else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.isVideoCall = false
            if appDelegate.checkVideoFromTerminationState {
                // appDelegate.window?.removeFromSuperview()
                exit(0)
            }
            else {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func room(_ room: TVIRoom, participantDidConnect participant: TVIRemoteParticipant) {
        if (self.remoteParticipant == nil) {
            self.remoteParticipant = participant
            self.remoteParticipant?.delegate = self
        }
        print("Participant \(participant.identity) connected with \(participant.remoteAudioTracks.count) audio and \(participant.remoteVideoTracks.count) video tracks")
    }
    
    func room(_ room: TVIRoom, participantDidDisconnect participant: TVIRemoteParticipant) {

        if let roomUuid = room.uuid {
            if self.timer.isValid {
                self.timer.invalidate()
            }
            performEndCallAction(uuid: roomUuid)
        }
       print("Room \(room.name), Participant \(participant.identity) disconnected")
    }
}

// MARK: TVIRemoteParticipantDelegate
extension VideoViewController : TVIRemoteParticipantDelegate {
    
    func remoteParticipant(_ participant: TVIRemoteParticipant,
                           publishedVideoTrack publication: TVIRemoteVideoTrackPublication) {
        
        // Remote Participant has offered to share the video Track.
        print("Participant \(participant.identity) published video track")
    }
    
    func remoteParticipant(_ participant: TVIRemoteParticipant,
                           unpublishedVideoTrack publication: TVIRemoteVideoTrackPublication) {
        
        // Remote Participant has stopped sharing the video Track.
        print("Participant \(participant.identity) unpublished video track")
    }
    
    func remoteParticipant(_ participant: TVIRemoteParticipant,
                           publishedAudioTrack publication: TVIRemoteAudioTrackPublication) {
        
        // Remote Participant has offered to share the audio Track.
        print("Participant \(participant.identity) published audio track")
    }
    
    func remoteParticipant(_ participant: TVIRemoteParticipant,
                           unpublishedAudioTrack publication: TVIRemoteAudioTrackPublication) {
        
        // Remote Participant has stopped sharing the audio Track.
        print("Participant \(participant.identity) unpublished audio track")
    }
    
    func subscribed(to videoTrack: TVIRemoteVideoTrack,
                    publication: TVIRemoteVideoTrackPublication,
                    for participant: TVIRemoteParticipant) {
        
        // We are subscribed to the remote Participant's audio Track. We will start receiving the
        // remote Participant's video frames now.
        print("Subscribed to video track for Participant \(participant.identity)")
        if (self.remoteParticipant == participant) {
            setupRemoteVideoView()
            showCallerNameUI(inRoom: true)
            videoTrack.addRenderer(self.remoteView!)
//            videoTrack.removeRenderer(self.fullScreenPreviewView!)
//            videoTrack.addRenderer(self.fullScreenPreviewView!)
//            localVideoTrack!.addRenderer(self.previewView)
        }
    }
    
    func unsubscribed(from videoTrack: TVIRemoteVideoTrack,
                      publication: TVIRemoteVideoTrackPublication,
                      for participant: TVIRemoteParticipant) {
        
        // We are unsubscribed from the remote Participant's video Track. We will no longer receive the
        // remote Participant's video.
        print("Unsubscribed from video track for Participant \(participant.identity)")
        
        if (self.remoteParticipant == participant) {
            videoTrack.removeRenderer(self.remoteView!)
            self.remoteView?.removeFromSuperview()
            self.remoteView = nil
//            videoTrack.removeRenderer(self.fullScreenPreviewView!)
//            self.fullScreenPreviewView?.removeFromSuperview()
//            self.fullScreenPreviewView = nil
        }
    }
    
    func subscribed(to audioTrack: TVIRemoteAudioTrack,
                    publication: TVIRemoteAudioTrackPublication,
                    for participant: TVIRemoteParticipant) {
        
        // We are subscribed to the remote Participant's audio Track. We will start receiving the
        // remote Participant's audio now.
        print("Subscribed to audio track for Participant \(participant.identity)")
    }
    
    func unsubscribed(from audioTrack: TVIRemoteAudioTrack,
                      publication: TVIRemoteAudioTrackPublication,
                      for participant: TVIRemoteParticipant) {
        
        // We are unsubscribed from the remote Participant's audio Track. We will no longer receive the
        // remote Participant's audio.
        print("Unsubscribed from audio track for Participant \(participant.identity)")
    }
    
    func remoteParticipant(_ participant: TVIRemoteParticipant,
                           enabledVideoTrack publication: TVIRemoteVideoTrackPublication) {
        print("Participant \(participant.identity) enabled video track")
    }
    
    func remoteParticipant(_ participant: TVIRemoteParticipant,
                           disabledVideoTrack publication: TVIRemoteVideoTrackPublication) {
        print("Participant \(participant.identity) disabled video track")
    }
    
    func remoteParticipant(_ participant: TVIRemoteParticipant,
                           enabledAudioTrack publication: TVIRemoteAudioTrackPublication) {
        print("Participant \(participant.identity) enabled audio track")
    }
    
    func remoteParticipant(_ participant: TVIRemoteParticipant,
                           disabledAudioTrack publication: TVIRemoteAudioTrackPublication) {
        print("Participant \(participant.identity) disabled audio track")
    }
    
    func failedToSubscribe(toAudioTrack publication: TVIRemoteAudioTrackPublication,
                           error: Error,
                           for participant: TVIRemoteParticipant) {
        print("FailedToSubscribe \(publication.trackName) audio track, error = \(String(describing: error))")
    }
    
    func failedToSubscribe(toVideoTrack publication: TVIRemoteVideoTrackPublication,
                           error: Error,
                           for participant: TVIRemoteParticipant) {
        print("FailedToSubscribe \(publication.trackName) video track, error = \(String(describing: error))")
    }
}

// MARK: TVIVideoViewDelegate
extension VideoViewController : TVIVideoViewDelegate {
    func videoView(_ view: TVIVideoView, videoDimensionsDidChange dimensions: CMVideoDimensions) {
        self.view.setNeedsLayout()
    }
}

// MARK: TVICameraCapturerDelegate
extension VideoViewController : TVICameraCapturerDelegate {
    func cameraCapturer(_ capturer: TVICameraCapturer, didStartWith source: TVICameraCaptureSource) {
        self.previewView.shouldMirror = (source == .frontCamera)
    }
}


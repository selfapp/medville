//
//  HistoryCustomCell.swift
//  JustStuck
//
//  Created by Gyan Routray on 15/12/16.
//  Copyright © 2016 Headerlabs. All rights reserved.
//

import UIKit

class HistoryCustomCell: UITableViewCell {
    @IBOutlet weak var totalChargesLabel: UILabel!
    @IBOutlet weak var cellTitleLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var cellSubTitleLabel: UILabel!
    @IBOutlet weak var subtitleHeightConstraint: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
         if AppUser.type == kPROVIDER {
            self.subtitleHeightConstraint.constant = 0
            self.layoutIfNeeded()
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

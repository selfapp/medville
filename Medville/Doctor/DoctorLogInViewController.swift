//
//  DoctorLogInViewController.swift
//  Medville
//
//  Created by Jitendra Solanki on 9/25/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

class DoctorLogInViewController: ViewController {

    //MARK:- outlets
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var viewUserNameInput: UIView!
    
    @IBOutlet weak var viewPasswordInput: UIView!
    
    @IBOutlet weak var tf_userName: UITextField!
    
    @IBOutlet weak var tf_password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        intializeUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Private helper methods
    private func intializeUI(){
        //force setup constraints
        self.viewDidLayoutSubviews()
        let color = UIColor.init(r: 244, g: 247, b: 254, alpha: 1.0)

        viewUserNameInput.addBorderWith(borderWidth: 1.0, withBorderColor: color, withCornerRadious: viewUserNameInput.frame.size.height/2)
        
        viewPasswordInput.addBorderWith(borderWidth: 1.0, withBorderColor: color, withCornerRadious: viewPasswordInput.frame.size.height/2)

        //set placeholder color
        tf_userName.setPlaceholderColorWith(color: color)
        tf_password.setPlaceholderColorWith(color: color)
        tf_userName.addInputAccessoryViewWith(target: self, selector: #selector(doneBtnTapped_onKeyboard))
        tf_password.addInputAccessoryViewWith(target: self, selector: #selector(doneBtnTapped_onKeyboard))

//        tf_userName.text = "avinash.prasad@anyalpha.com"
//        tf_password.text = "Welcome@123"
    }
    //MARK:- Private methods
    @objc private func doneBtnTapped_onKeyboard(){
        self.view.endEditing(true)
    }

    private func isValidEmail(emailString:String) -> Bool {
        let email = emailString.replacingOccurrences(of: " ", with: "")
//        print("validate emilId: \(email)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: email)
        return result
    }
    
    
    private func doLogInWith(email:String,passwd:String){
        if Reachability.isConnectedToNetwork(){
            spinner.show(allowInteraction: false)
            Doctor.loginWith(email:email, password:passwd) { [unowned self] (doctor, message) in
                if let doctor = doctor{
                    self.spinner.hide()
                    UIApplication.shared.appDelegate.registerOnPusher(identity: doctor.uuid)
                    ChatManager._sharedManager.loginWithidentity(identity: doctor.uuid, completion: { (status) in
                        if status{
                            //set home page for doctor
                           print("Login in twilio")
                        }else{
                            self.showAlertWith(message: "Something went wrong", title: "")
                        }
                    })
                    DispatchQueue.main.async {
                        UIApplication.shared.appDelegate.setHomePageForUser(type: .kDocotor)
                        
                    }
                    
                }else{
                    self.spinner.hide()
                    self.showAlertWith(message: message!, title: "")
                }
            }
        }else{
            self.spinner.hide()
            self.showAlertWith(message:ErrorMessages.kNetworkError, title: "")
        }
    }
    
    //MARK:- Action methods
    
    @IBAction func btnActn_logIn(_ sender: UIButton) {
       
        //do validate
        guard
            let email = tf_userName.text , email.count >  0 ,
            let password = tf_password.text, password.count > 0
            else{
                self.showAlertWith(message: "All fields are required.", title: "")
                return
        }
        
        if !isValidEmail(emailString: email){
            self.showAlertWith(message: "Please enter a valid email.", title: "")
            return
        }
        
        //login doctor
        doLogInWith(email: email, passwd: password)
    }
    
    @IBAction func btnActn_forgotPassword(_ sender: UIButton) {
    }
    
    
    @IBAction func btnActn_patientLogIn(_ sender: UIButton) {
        view.endEditing(true)
        (UIApplication.shared.delegate as! AppDelegate).logInForUser(type: .kPatient)

    }
}

extension DoctorLogInViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         textField.resignFirstResponder()
        return true
    }
}

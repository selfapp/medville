//
//  ViewController.swift
//  Kannects
//
//  Created by Jitendra Solanki on 7/18/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit


class ViewController: UIViewController,KeyboardShowable, ImageSelectable {

    var selectedImage:UIImage!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(showKeyboard(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideKeyboard(notification:)), name: .UIKeyboardWillHide, object: nil)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
       // NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
       // NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }

    @objc func showKeyboard(notification:NSNotification){
        keyBoardWillShow(notification: notification)
    }
    @objc func hideKeyboard(notification:NSNotification){
        keyBoardWillHide(notification: notification)
    }
    
    
    func openImagePicker() {
        imagePickerControllerWith(delegate: self)
    }
    
}

extension ViewController : UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
//        imgV_profileImage.layer.cornerRadius = imgV_profileImage.frame.size.width/2
//        imgV_profileImage.layer.masksToBounds = true
//        
//        picker.dismiss(animated: true, completion: nil)
//        if let image = info["UIImagePickerControllerOriginalImage"] as? UIImage{
//            print("image size== \(image.size)")
//            
//            if  let scaledImage = image.scaleImage(targetSize: CGSize(width: 200, height: 200)){
//                //self.imgV_profileImage.image = scaledImage
//                //self.userImage = scaledImage
//                self.selectedImage = scaledImage
//            }
//            SDImageCache.shared().clearMemory()
//            SDImageCache.shared().clearDisk(onCompletion: {
//            })
//        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        //picker.dismiss(animated: true, completion: nil)
    }
    
    
}




//
//  MessagesViewController.swift
//  Medville
//
//  Created by Jitendra Solanki on 9/26/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit
import TwilioChatClient

class MessagesVC: UIViewController{

   
    //MARK:- outlets
    
    @IBOutlet weak var segmentView: JKSegmentView!
    
    @IBOutlet weak var hScrollView: UIScrollView!
    
    @IBOutlet weak var tableV_notification: UITableView!
    
    @IBOutlet weak var chatListTbl_view: UITableView!
    
    //MARK:-iVar
    let NOTIFICATION_CELL_IDENTIFIER = "NotificationPatientCellIdentifier"
    let cellIdentifier = "chatUserCellIdentifier"
    var notifications:[LocalNotification] = []

    var messages:[ChannelListModel] = []
    var sortedArray:[ChannelListModel] = []
    var chatUserList:[ChatUser] = []
    var isTwilioConnected:Bool = false
    
    var msg:String = "Loading..."
    //MARK:- View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initializeUI()
        configureTableView()
        getNotificationForDoctor()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getNotificationForDoctor()
        if ChatManager._sharedManager.isConnected {
            twilioSetUp()
        }else{
            perform(#selector(twilioCall), with: nil, afterDelay: 0.5)

        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func twilioCall(){
        if ChatManager._sharedManager.isConnected && !isTwilioConnected{
            twilioSetUp()
        }else{
            perform(#selector(twilioCall), with: nil, afterDelay: 0.5)
        }
    }
    
    //MARK:- Twilio Setup
    func twilioSetUp(){
        isTwilioConnected = true
        let client:TwilioChatClient = ChatManager._sharedManager.client!
        
            client.delegate = self
            if (client.synchronizationStatus == TCHClientSynchronizationStatus.completed){
                populateChannelList()
            }
    }
    
    func populateChannelList(){
        self.messages.removeAll()
        let channelList: TCHChannels = (ChatManager._sharedManager.client?.channelsList())!
        let newChannles = NSMutableOrderedSet(array: channelList.subscribedChannels())
       // sortChannels(channls: newChannles)
       // DispatchQueue.main.async {
            self.loadInitialMessage(channels: newChannles)
       // }
    }
    
    func loadInitialMessage(channels:NSMutableOrderedSet){
        var noOfChannel = 0
        msg = (noOfChannel == 0) ? "" : "Loading..."
        var chatUsersUuid:[String] = []
        for channel in channels{
            let channel1 = channel as! TCHChannel
            //            channel1.destroy(completion: { (result) in
            //                print("channel deleted ")
            //            })
            let uuid = channel1.uniqueName?.components(separatedBy: "/")
            chatUsersUuid.append(uuid![1])
            channel1.delegate = self
            channel1.messages?.getLastWithCount(1, completion: { (result, message) in
                noOfChannel += 1
                let messageCount:Int = (message?.count)!
                if result.isSuccessful() && (messageCount > 0){
                    let objt = ChannelListModel()
                    objt.channel = channel1
                    objt.message = message?[0]
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = DateFormat.kCurrentFormat.rawValue
                    if let date = dateFormatter.date(from: (objt.message?.timestamp)!) {
                        objt.timeStamp = date
                    }
                    self.messages.append(objt)
                }
                if noOfChannel == channels.count{
                    self.sortedArray.removeAll()
                    self.sortCHannel()
                    self.chatListTbl_view.reloadData()
                }
            })
        }
        self.getChatUser(uuids: chatUsersUuid)
    }
    
    
    // Sort Channles by friendly name
    func sortCHannel(){
        self.sortedArray = self.messages.sorted(by: {
            $0.timeStamp?.compare($1.timeStamp!) == .orderedDescending
        })
    }

    
    
    //MARK:- Private helper methods
    private func initializeUI(){
        self.navigationItem.title = "Messages"
        
        //customize  segment view
        segmentView.setupViewWithSegments(segments: ["Chat","Notification"])
        segmentView.delegate = self
    }
    
    private func configureTableView(){
        let nib = UINib(nibName: "PatientNotificationCell", bundle: Bundle.main)
        tableV_notification.register(nib, forCellReuseIdentifier: NOTIFICATION_CELL_IDENTIFIER)
        tableV_notification.tableFooterView =  UIView()
        
        let nib1 = UINib(nibName: "ChatUserCell", bundle: Bundle.main)
        chatListTbl_view.register(nib1, forCellReuseIdentifier: cellIdentifier)
        chatListTbl_view.tableFooterView = UIView(frame: CGRect.zero)
     }

    

    //Get Notification
    private func getNotificationForDoctor(){
        if Reachability.isConnectedToNetwork(){
            if let doctor = AppUser.doctor{
                //spinner.show(allowInteraction: false)
                Doctor.getNotificationForDoctor(id: doctor.id,  {[weak self] (notification) in
                    self?.notifications.removeAll()
                    self?.notifications = notification
                    DispatchQueue.main.async {
                        self?.tableV_notification.reloadData()
                    }
                    }, failure: {[weak self] (message) in
                        //  self?.showAlertWith(message: message, title: "")
                })
            }else{
                //self.showAlertWith(message: ErrorMessages.kDoctorNotFound, title:  "")
            }
        }else{
            self.showAlertWith(message:ErrorMessages.kNetworkError, title: "")
        }
    }
    
    // Get Chat User Details
    private func getChatUser(uuids:[String]){
        if Reachability.isConnectedToNetwork(){
            Doctor.getChatsForDoctor(uuids: uuids, { (chatUser) in
                self.chatUserList.removeAll()
                self.chatUserList = chatUser
                self.chatListTbl_view.reloadData()
                print("Response --> \(chatUser)")
            }, failure: { (message) in
                print("message")
            })
        }else{
            self.showAlertWith(message:ErrorMessages.kNetworkError, title: "")
        }
    }

    func getChatUserDetails(uuid:String) -> ChatUser? {
        
        let user = self.chatUserList.filter( { return $0.uuid == uuid } )
        if user.count > 0{
            return user.last!
        }
        return nil
    }

}


extension MessagesVC:UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if chatListTbl_view == tableView{
            if self.sortedArray.count > 0 {
                chatListTbl_view.separatorStyle = .singleLine
                chatListTbl_view.backgroundView?.isHidden = true
                return 1
            }else{
                let message = UILabel()
                message.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height)
                message.text = msg
                message.numberOfLines = 0
                message.sizeToFit()
                message.textAlignment = .center
                chatListTbl_view.backgroundView = message
                chatListTbl_view.separatorStyle = .none
                chatListTbl_view.backgroundView?.isHidden = false
                return 0
            }
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if chatListTbl_view == tableView{
          return self.sortedArray.count
        }
        return notifications.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == chatListTbl_view{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)as! ChatUserCell
            let object = self.sortedArray[indexPath.row]
            let users = object.channel?.friendlyName?.components(separatedBy: "/")
            let uuid = object.channel?.uniqueName?.components(separatedBy: "/")
            if let chat = self.getChatUserDetails(uuid: (uuid?.last)!){
                cell.avtorImageView.sd_setImage(with: chat.avatar, placeholderImage: UIImage(named: "defualt-profile"))
            }
            
            cell.userName.text = users?[1]
            cell.messagelbl.text = object.message?.body
            cell.messagelbl.textColor = (object.unreadMessage > 0) ? UIColor(red: 128/255, green: 156/255, blue: 237/255, alpha: 1) : UIColor.gray
            cell.timeLabel.text = object.message?.timestamp?.getTimeDifferenceStringTillNowWith(currentFormat: DateFormat.kCurrentFormat.rawValue)
            return cell
        }
        
        
        // Notification cell
        let cell = tableView.dequeueReusableCell(withIdentifier: NOTIFICATION_CELL_IDENTIFIER, for: indexPath)as! PatientNotificationCell
        cell.selectionStyle = .none
        let notification = notifications[indexPath.row]
        
        cell.notificationMsg.text = notification.message
        cell.notificationIcon.setImage(#imageLiteral(resourceName: "notification-icon-2"), for: UIControlState.normal)
        cell.notificationTime.text = notification.created_at.getTimeDifferenceStringTillNowWith(currentFormat: DateFormat.kCurrentFormat.rawValue)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if chatListTbl_view == tableView {
            //self.tabBarController?.viewControllers?[2].tabBarItem.badgeValue = nil
            let object = self.sortedArray[indexPath.row]
            let channel:TCHChannel = object.channel!
        if let chatVC  = viewControllerFor(identifier: VCIdentifier.kChatViewController)as? ChatViewController{
            chatVC.channel = channel
            let users = channel.friendlyName?.components(separatedBy: "/")
            let uuids = channel.uniqueName?.components(separatedBy: "/")
            chatVC.chatUserName = users?[1]
            chatVC.chatUserIdentity = uuids?[1]
            chatVC.currentIdentity = uuids![0]
            chatVC.currentUserName = users?[0]
            self.navigationController?.pushViewController(chatVC, animated: true)
        }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
}



extension MessagesVC:JKSegmentViewDelegate{
    func didSelectSegmentAt(index segmentIndex: Int, view segmentView: JKSegmentView) {
        var x:CGFloat = 0
        if segmentIndex == 0{
            self.chatListTbl_view.reloadData()
        }else{
            x = UIScreen.main.bounds.size.width
            self.tableV_notification.reloadData()
        }
        
        UIView.animate(withDuration: 0.3) {
            self.hScrollView.contentOffset.x = x
        }
        
    }
}

//MARK:- Twilio Delegate
extension MessagesVC:TwilioChatClientDelegate{
 
    func chatClient(_ client: TwilioChatClient, channelAdded channel: TCHChannel) {
        
    }
    func chatClient(_ client: TwilioChatClient, synchronizationStatusUpdated status: TCHClientSynchronizationStatus) {
        if (status == TCHClientSynchronizationStatus.completed){
            populateChannelList()
        }

    }
    func chatClient(_ client: TwilioChatClient, notificationAddedToChannelWithSid channelSid: String) {
        
    }
}

extension MessagesVC:TCHChannelDelegate{
    func chatClient(_ client: TwilioChatClient, channel: TCHChannel, messageAdded message: TCHMessage) {
        self.addMessageTo(channel: channel, message: message)
    }

    func addMessageTo(channel:TCHChannel,message:TCHMessage){
        let noOfValue = self.messages.count
        if noOfValue > 0 {
        for index in 0...noOfValue{
            let object = self.messages[index]
            if object.channel?.uniqueName == channel.uniqueName{
                object.message = message
                object.unreadMessage += 1
                self.messages.remove(at: index)
                self.messages.insert(object, at: index)
                self.sortCHannel()
                break
            }
        }
        }
        self.chatListTbl_view.reloadData()
    }
    
}

//
//  MyAppointmentViewController.swift
//  Medville
//
//  Created by Jitendra Solanki on 9/26/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

class MyAppointmentViewController: UIViewController {
    //MARK:- outlets

    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var segmentView: JKSegmentView!
    @IBOutlet weak var tableV_upcoming: UITableView!
    @IBOutlet weak var tableV_completed: UITableView!
    @IBOutlet weak var scrollView:UIScrollView!
    
    let cellIdentifer = "AppointmentCell"

    var arr_upcomingAppointments:[Appointment] = []
    var arr_completedAppointments:[Appointment] = []
    
    //MARK:- Pull to refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(MyAppointmentViewController.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.black
        refreshControl.attributedTitle = NSAttributedString(string: "Pull-to-Refresh")

        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        getAppointments()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initializeUI()
        configureTableView()
        spinner.show(allowInteraction: false)
        getAppointments()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        spinner.show(allowInteraction: false)
        getAppointments()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Private helper methods
    private func initializeUI(){
        self.navigationItem.title = "Appointments"
        
        //customize  segment view
        segmentView.setupViewWithSegments(segments: ["Upcoming","Completed"])
        segmentView.delegate = self
    }
    private func configureTableView(){
        let nib = UINib(nibName: "AppointmentCell", bundle: Bundle.main)
        tableV_upcoming.register(nib, forCellReuseIdentifier: cellIdentifer)
        tableV_completed.register(nib, forCellReuseIdentifier: cellIdentifer)
        tableV_completed.tableFooterView = UIView(frame: CGRect.zero)
        tableV_upcoming.tableFooterView = UIView(frame: CGRect.zero)
        self.tableV_completed.addSubview(self.refreshControl)
        self.tableV_upcoming.addSubview(self.refreshControl)

    }
    
    private func getAppointments(){
        if Reachability.isConnectedToNetwork(){
            
            if let doctor = AppUser.doctor{
                
                Doctor.getAppointmentsForDoctor(id: doctor.id, {[weak self] (upcoming, completed) in
                    self?.spinner.hide()
                    if (self?.refreshControl.isRefreshing)!
                    {
                        self?.refreshControl.endRefreshing()
                    }

                    self?.arr_upcomingAppointments.removeAll()
                    self?.arr_completedAppointments.removeAll()
                    
                    self?.arr_upcomingAppointments = upcoming
                    self?.arr_completedAppointments = completed
                    
                    DispatchQueue.main.async {
                        (self?.segmentView.selectedIndex == 0) ? self?.tableV_upcoming.reloadData() : self?.tableV_completed.reloadData()
                    }
                    
                }, failure: {[weak self] (message) in
                    self?.spinner.hide()
                    if (self?.refreshControl.isRefreshing)!
                    {
                        self?.refreshControl.endRefreshing()
                    }

                    self?.showAlertWith(message: message, title: "")
                })
                
            }else{
                self.showAlertWith(message: ErrorMessages.kDoctorNotFound, title:  "")
            }
            
        }else{
            self.showAlertWith(message:ErrorMessages.kNetworkError, title: "")

        }
    }
    
}

extension MyAppointmentViewController:JKSegmentViewDelegate{
    func didSelectSegmentAt(index segmentIndex: Int, view segmentView: JKSegmentView) {
        var x:CGFloat = 0
        if segmentIndex == 0{
            self.tableV_upcoming.reloadData()
        }else{
            x = UIScreen.main.bounds.size.width
            self.tableV_completed.reloadData()
        }
 
        UIView.animate(withDuration: 0.3) {
            self.scrollView.contentOffset.x = x
        }
        
    }
}

extension MyAppointmentViewController:UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.segmentView.selectedIndex == 0 ? arr_upcomingAppointments.count :   arr_completedAppointments.count
       // return 10
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
     
        cell.backgroundColor = indexPath.row % 2 == 0 ? UIColor.white : UIColor(r:241,g:241,b:241,alpha:1.0)
        cell.accessoryType = .disclosureIndicator
        cell.selectionStyle = .none
        cell.textLabel?.textColor =  UIColor(r:32,g:32,b:32,alpha:1.0)
        cell.textLabel?.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.bold)
        cell.detailTextLabel?.textColor = UIColor(r:150,g:150,b:150,alpha:1.0)
    
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView == self.tableV_upcoming ? tableView.dequeueReusableCell(withIdentifier: cellIdentifer, for: indexPath) as? AppointmentCell : tableView.dequeueReusableCell(withIdentifier: cellIdentifer, for: indexPath) as? AppointmentCell
        
        //set data for cell
        let appointment:Appointment  = tableView == self.tableV_upcoming ? arr_upcomingAppointments[indexPath.row] : arr_completedAppointments[indexPath.row]
        cell?.avtorImage?.sd_setImage(with: appointment.patient?.avatarPath, placeholderImage: UIImage(named: "defualt-profile"))
        cell?.nameLabel.text = appointment.patient?.firstName?.capitalized
        cell?.timeDetailLabel.text = appointment.start_time.convertDateStringFrom(currentFormat: DateFormat.kCurrentFormat.rawValue, toRequiredFormat: DateFormat.kRequireFormat.rawValue)
        cell?.appointmentStatus.text = appointment.state.capitalized
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let patientDetails = viewControllerFor(identifier: VCIdentifier.kPatientDetails)as! PatientDetailsViewController
       
        patientDetails.patient =
        tableView == self.tableV_upcoming ? arr_upcomingAppointments[indexPath.row].patient : arr_completedAppointments[indexPath.row].patient
        self.navigationController?.pushViewController(patientDetails, animated: true)
    }
    
}


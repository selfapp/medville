//
//  WebViewController.swift
//  Medville
//
//  Created by Jitendra Solanki on 9/27/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

class WebViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    //MARK:- iVar
    var fileName:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initializeUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Private helper methods
    private func initializeUI(){
        self.viewDidLayoutSubviews()
        self.navigationItem.title = ""
        
        //add left bar button
        let btn =  UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(backBtnTapped))
        self.navigationItem.leftBarButtonItem = btn
        
        //load html into Webview
         if let path = Bundle.main.path(forResource: fileName, ofType: "html"){
            let url = URL(fileURLWithPath: path)
             self.webView.loadRequest(URLRequest(url: url))
         }
        
    }
    
    //MARK:- Action methods
    @objc private func backBtnTapped(){
        self.navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

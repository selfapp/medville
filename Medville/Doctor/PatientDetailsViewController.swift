//
//  PatientDetailsViewController.swift
//  Medville
//
//  Created by Jitendra Solanki on 10/4/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

class PatientDetailsViewController: UIViewController {

    //MARK:- outlets
    
    @IBOutlet weak var imgV_profilePic: UIImageView!
    @IBOutlet weak var lbl_name: UILabel!
   // @IBOutlet weak var btn_call: JKRoundButton!
    @IBOutlet weak var view_details: UIView!
 
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var details_innerView: UIView!
    @IBOutlet weak var lbl_age: UILabel!
    @IBOutlet weak var lbl_bloodGroup: UILabel!
    @IBOutlet weak var lbl_weight: UILabel!
    @IBOutlet weak var lbl_height: UILabel!
    @IBOutlet weak var lbl_doc: UILabel!
    @IBOutlet weak var stackView_doc: UIStackView!
    
    @IBOutlet weak var attachment_CollectionView: UICollectionView!
    @IBOutlet weak var tableV_appointments: UITableView!
   
    @IBOutlet weak var appointmentHistoryView: UIView!
    /*to set the height to show 5 row */
    @IBOutlet weak var appointmentHistryConstent: NSLayoutConstraint!
    @IBOutlet weak var constaintHeight_tableOuterView: NSLayoutConstraint!
    
    //MARK:- iVar
    var patient:Patient!
    
    let cellIdentifier =  "AppointmentHistoryCell"
    var arrAppointmentHistory:[Appointment] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.attachment_CollectionView.register(UINib(nibName: "AttachmentCell", bundle: nil), forCellWithReuseIdentifier: "AttachmentCell")

        // Do any additional setup after loading the view.
        initializeUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addGradeintOnCallButton()

    }
    override func viewDidAppear(_ animated: Bool) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:- Private helper methods
    private func initializeUI(){
        self.viewDidLayoutSubviews()
        self.navigationItem.title = "Patient Details"
        
        //add left bar button
        let btn =  UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(backBtnTapped))
        self.navigationItem.leftBarButtonItem = btn
        imgV_profilePic.layer.cornerRadius = imgV_profilePic.frame.size.width/2
        imgV_profilePic.clipsToBounds = true
        //make corner round
       // view_details.makeCornerRound(cornerRadius: 10)
        
        tableV_appointments.layer.cornerRadius = 5.0
        //view_details.makeCornerRound(cornerRadius: 15.0)
        //view_appointmentsHistory.makeCornerRound(cornerRadius: 5.0)
        //add shadow
       // view_details.addShadowWith(opacity: 1.0, radius: 10, offset: CGSize(width:2,height:2), color: UIColor.black)
        //view_details.layer.cornerRadius = 5.0

       // view_details.addShadowWith(opacity: 1.0, radius: 10, offset: CGSize(width:2,height:2), color: UIColor(r:237,g:237,b:237,alpha:1.0))
        tableV_appointments.addShadowWith(opacity: 1.0, radius: 10, offset: CGSize(width:2,height:2), color: UIColor(r:237,g:237,b:237,alpha:1.0))
        tableV_appointments.tableFooterView = UIView()
        
        //constaintHeight_tableOuterView.constant = CGFloat(min(44*5, arrAppointmentHistory.count))
        
        
        //set data
        imgV_profilePic.sd_setImage(with: patient.avatarPath, placeholderImage: UIImage(named: "defualt-profile"))
        
        lbl_name.text = patient.firstName?.capitalized
        lbl_bloodGroup.text =  patient.bloodGroup
        if let age = patient.age{
            lbl_age.text = "\(age)"
        }
        if let weight = patient.weight{
            lbl_weight.text = "\(weight)"
        }
        
        if let height = patient.height{
            lbl_height.text = "\(height)"
        }
        if ((patient.inactive_appointments?.count) != nil){
            appointmentHistoryView.isHidden = false
            let val1 = 44*5
            let val2 = 44 * (patient.inactive_appointments?.count)!
            
            appointmentHistryConstent.constant = CGFloat(min(val1, val2))
            
            constaintHeight_tableOuterView.constant = CGFloat(min(val1, val2))
            
            
        }

     }
    override func viewDidLayoutSubviews() {
        imgV_profilePic.layer.cornerRadius = imgV_profilePic.frame.size.width/2
        imgV_profilePic.clipsToBounds = true

    }
    private func addGradeintOnCallButton(){
        let layer = CAGradientLayer()
        let colors = [UIColor(r:113,g:141,b:228,alpha:1.0),UIColor(r:113,g:141,b:228,alpha:1.0)]
        layer.colors = colors.map{$0.cgColor}
       // layer.frame = btn_call.bounds
        layer.startPoint =  CGPoint(x:0.0, y:0.5)
        layer.endPoint =  CGPoint(x:1.0, y:0.5)
        
       // btn_call.layer.addSublayer(layer)
//        btn_call.layer.insertSublayer(layer, at: 1)
//        if let imgV = btn_call.imageView{
//            btn_call.bringSubview(toFront: imgV)
//        }
    }
    
    //MARK:- Action methods
    @objc private func backBtnTapped(){
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func callButtonTap(_ sender: Any) {
        self.view.endEditing(true)
        
         let audioVC = AudioViewController.shared
            audioVC.callerIdentity = patient.uuid
        audioVC.callerName = patient.firstName
        audioVC.currentUserName = (AppUser.doctor?.first_name)!
        audioVC.placeCall()
        self.present(audioVC, animated: true) {
        }
    }
    
    @IBAction func chatButtonTap(_ sender: Any) {
        activityIndicator.show(allowInteraction: false)
        let doctorUUid = (AppUser.doctor?.uuid)!
        let channelName = "\(doctorUUid)/\(patient.uuid)"
        let doctofName:String = (AppUser.doctor?.first_name)!
        let patientFname:String = patient.firstName!
        let friendlyName = "\(doctofName)/\(patientFname)"
        
        ChatManager._sharedManager.isChannelExist(channelName: channelName) { (oldChannel) in
            self.activityIndicator.hide()
            if oldChannel != nil{
                if let chatVC  = self.viewControllerFor(identifier: VCIdentifier.kChatViewController)as? ChatViewController{
                    chatVC.channel = oldChannel!
                    chatVC.chatUserName = patientFname
                    chatVC.chatUserIdentity = self.patient.uuid
                    chatVC.currentIdentity = doctorUUid
                    chatVC.currentUserName = doctofName
                    self.navigationController?.pushViewController(chatVC, animated: true)
                    
                }
            }else{
                ChatManager._sharedManager.createChannel(chanelName: channelName, friendlyName: friendlyName, completion: { (newChannel) in
                    ChatManager._sharedManager.addUserinChannel(userName: self.patient.uuid, channel: newChannel)
                    if let chatVC  = self.viewControllerFor(identifier: VCIdentifier.kChatViewController)as? ChatViewController{
                        chatVC.channel = newChannel
                        chatVC.chatUserName = patientFname
                        chatVC.chatUserIdentity = self.patient.uuid
                        chatVC.currentIdentity = doctorUUid
                        chatVC.currentUserName = doctofName
                        self.navigationController?.pushViewController(chatVC, animated: true)
                    }
                })
            }
        }
    }
    
    
    @IBAction func videoButtonTap(_ sender: Any) {
        self.view.endEditing(true)
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.isVideoCall = true
        let videoVC = storyboard.instantiateViewController(withIdentifier: VCIdentifier.kVideoVC) as? VideoViewController
        let userId:String = (AppUser.doctor?.uuid)!
        appDelegate.roomName = "\(userId)_\(patient.uuid)"
        appDelegate.videoCallerName = patient.firstName!
        self.present(videoVC!, animated: true, completion: nil)
        
    }
    
}


extension PatientDetailsViewController :UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return arrAppointmentHistory.count
        if ((patient.inactive_appointments?.count) != nil) {
            return (patient.inactive_appointments?.count)!
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        cell.selectionStyle = .none
        //cell.textLabel?.text = arrAppointmentHistory[indexPath.row].start_time
       
        cell.textLabel?.text = patient.inactive_appointments![indexPath.row].start_time.convertDateStringFrom(currentFormat: DateFormat.kCurrentFormat.rawValue, toRequiredFormat: DateFormat.kRequireFormat.rawValue)
        return cell
    }
    
}

extension PatientDetailsViewController : UICollectionViewDelegate,UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (patient.patientsDoc?.count)!
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellOfCollection = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentCell", for: indexPath) as! AttachmentCell
        
        let doc = patient.patientsDoc?[indexPath.row]
        
        cellOfCollection.attachedImage.sd_setImage(with: doc?.thumbnail_url, placeholderImage: UIImage(named: "Icon_Image"))
        cellOfCollection.deleteButton.isHidden = true
        return cellOfCollection

    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vcFullImageView = viewControllerFor(identifier: VCIdentifier.kAttachmentDoc) as! AttachedDocViewController

        vcFullImageView.attechedDoc = patient.patientsDoc!
        vcFullImageView.index = indexPath.row
        vcFullImageView.modalPresentationStyle = .overFullScreen
        vcFullImageView.modalTransitionStyle = .crossDissolve
        self.present(vcFullImageView, animated: true, completion: nil)
    }
   
    
}

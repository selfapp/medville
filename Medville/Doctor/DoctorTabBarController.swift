
//  DoctorTabBarController.swift
//  Medville
//
//  Created by Jitendra Solanki on 9/26/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

class DoctorTabBarController: UITabBarController{


    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.appDelegate.registerOnPusher(identity: (AppUser.doctor?.uuid)!)
        if !ChatManager._sharedManager.isConnected {
            twillioSetup()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func twillioSetup(){
        
        if let doctor = AppUser.doctor{

            ChatManager._sharedManager.loginWithidentity(identity: doctor.uuid, completion: { (status) in
                print("is Twilio Connected - \(status)")
                let viewcon = AudioViewController.shared
                if !status{
                    self.twillioSetup()
                }
            })
        }
    }
}



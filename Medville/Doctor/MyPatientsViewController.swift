//
//  MyPatientsViewController.swift
//  Medville
//
//  Created by Jitendra Solanki on 9/26/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

class MyPatientsViewController: UIViewController {

    //MARK:- Outlets
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var tableV_patients: UITableView!
    
    //MARK: iVar
    let patientCell = "MyPatientsCellIdentifier"
    
    var arr_patients:[Patient] = []
    
    //MARK:- Pull to refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(MyPatientsViewController.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.black
        refreshControl.attributedTitle = NSAttributedString(string: "Pull-to-Refresh")

        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.getPatientsList()
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initializeUI()
        getPatientsList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Private helper methods
    private func initializeUI(){
        self.navigationItem.title = "My Patients"
        tableV_patients.tableFooterView = UIView(frame: CGRect.zero)
        self.tableV_patients.addSubview(self.refreshControl)
        let nib = UINib(nibName: "MyPatientsCell", bundle: Bundle.main)
        tableV_patients.register(nib, forCellReuseIdentifier: patientCell)
    }
    
    private func getPatientsList(){
        if Reachability.isConnectedToNetwork(){
            
            if let doctor = AppUser.doctor{
                spinner.show(allowInteraction: true)
                
                //get Patient list
                Doctor.getPatientsListForDocotor(id: doctor.id, {[weak self] (activePatients, inActivePatients) in
                    self?.spinner.hide()
                    if (self?.refreshControl.isRefreshing)!
                    {
                        self?.refreshControl.endRefreshing()
                    }
                    self?.arr_patients.removeAll()
                    self?.arr_patients = activePatients
                    //self?.arr_patients.append(contentsOf: inActivePatients)
                    DispatchQueue.main.async {
                         self?.tableV_patients.reloadData()
                    }
                }, failure: {[weak self] (message) in
                    self?.spinner.hide()
                    self?.showAlertWith(message: message, title: "")
                })
                
            }else{
                self.showAlertWith(message: ErrorMessages.kDoctorNotFound, title:  "")
            }
            
        }else{
            self.showAlertWith(message:ErrorMessages.kNetworkError, title: "")
            
        }
    }

}

extension MyPatientsViewController :UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_patients.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: patientCell, for: indexPath)as! MyPatientsCell
        cell.selectionStyle = .none
        cell.lbl_name.text = arr_patients[indexPath.row].firstName?.capitalized
        cell.imgV_profilePic.sd_setImage(with: arr_patients[indexPath.row].avatarPath, placeholderImage: UIImage(named: "defualt-profile"))
        
        return cell
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let patientDetails = viewControllerFor(identifier: VCIdentifier.kPatientDetails)as! PatientDetailsViewController
        patientDetails.patient = arr_patients[indexPath.row]
        self.navigationController?.pushViewController(patientDetails, animated: true)
    }
}

//
//  AttachedDocViewController.swift
//  Medville
//
//  Created by Durgesh on 05/12/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

class AttachedDocViewController: UIViewController {
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var thumnail: UIImageView!
    @IBOutlet weak var webview: UIWebView!
    @IBOutlet weak var attachImageView: UIImageView!
    @IBOutlet weak var deleteButton: UIButton!
   // @IBOutlet weak var attachedDocScrollView: UIScrollView!
    var attechedDoc:[Attachment] = []
    var index:Int = 0
    var localAttachment:[UIImage]?
    
    var delegate:PatientSettingViewController? = nil
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let attachDoc = attechedDoc[index]
        activityIndicator.show(allowInteraction: true)
        thumnail.sd_setImage(with: attachDoc.thumbnail_url, placeholderImage:
            UIImage(named: "Icon_Image"))
        if attachDoc.mime_type.range(of:"image") != nil {
            self.attachImageView.sd_setImage(with: attachDoc.url, completed: { (image, error, cacheType, url) in
                self.thumnail.isHidden = true
                self.activityIndicator.hide()
            })
            webview.isHidden = true
        }else{
        let url = attachDoc.url
        let requestObj = URLRequest(url: url!)
            webview.delegate = self
        webview.loadRequest(requestObj)
            attachImageView.isHidden = true
        }
//        attachedDocScrollView.minimumZoomScale = 1.0
//        attachedDocScrollView.maximumZoomScale = 5.0
//        setupview()
        let userType = AppUser.userType
        if userType == .kPatient{
            self.deleteButton.isHidden = false
        }else{
        self.deleteButton.isHidden = true
        }

    }

//    func setupview() {
//        let width = self.view.frame.size.width
//        let height = UIScreen.main.bounds.size.height - 65
//        if ((localAttachment?.count) != nil){
//            var attache = attechedDoc?.count
//            attache = attache! + (localAttachment?.count)!
//            attachedDocScrollView.contentSize = CGSize(width: width * CGFloat(attache!), height: self.attachedDocScrollView.frame.size.height)
//        }else{
//        attachedDocScrollView.contentSize = CGSize(width: width * CGFloat((attechedDoc?.count)!), height: self.attachedDocScrollView.frame.size.height)
//        }
//        attachedDocScrollView.isPagingEnabled = true
//        var i = 0
//        for doc in attechedDoc! {
//            let image = UIImageView(frame:CGRect(x: CGFloat(i) * width, y:0, width: width, height:height))
//            image.sd_setImage(with: doc.url, placeholderImage: UIImage(named: "defualt-profile"))
//            image.contentMode = .scaleAspectFit
//            attachedDocScrollView.addSubview(image)
//            i = i + 1
//        }
//        if ((localAttachment?.count) != nil){
//            for doc in localAttachment! {
//                let image = UIImageView(frame:CGRect(x: CGFloat(i) * width, y:0, width: width, height:height))
//                image.image = doc
//                image.contentMode = .scaleAspectFit
//                attachedDocScrollView.addSubview(image)
//                i = i + 1
//            }
//        }
//
//        attachedDocScrollView.setContentOffset(CGPoint(x: CGFloat(index!) * width, y: 0), animated: true)
//    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButtonTap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

    }
    
    @IBAction func deleteButtonTap(_ sender: Any) {
       // let page = attachedDocScrollView.contentOffset.x / attachedDocScrollView.frame.size.width
        let button = UIButton()
        button.tag = Int(index)
        delegate?.deleteButtonTap(button)
        self.dismiss(animated: true, completion: nil)

    }
}
//self.activityIndicator.hide()
extension AttachedDocViewController : UIWebViewDelegate{
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.activityIndicator.hide()
        self.thumnail.isHidden = true
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.activityIndicator.hide()
    }
}


//
//  ChoosePasswordViewController.swift
//  Medville
//
//  Created by Jitendra Solanki on 10/10/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

protocol ChoosePasswordDelegate :class {
    func didUpdatedPassword(success:Bool, message:String?)
}
class ChoosePasswordViewController: ViewController {

    //MARK:- outlets
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var tf_oldPasswd: UITextField!
    
    @IBOutlet weak var tf_newPasswd: UITextField!
    
    @IBOutlet weak var tf_confirmPasswd: UITextField!
    
    weak var delegate:ChoosePasswordDelegate?
   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        intializeUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Private helper methods
    private func intializeUI(){
        let borderColor = UIColor(r:197,g:197,b:197,alpha:1.0)
        tf_newPasswd.addBorderWith(color: borderColor, width: 0.8)
        tf_oldPasswd.addBorderWith(color: borderColor, width: 0.8)
        tf_confirmPasswd.addBorderWith(color: borderColor, width: 0.8)
        
        tf_confirmPasswd.addLeftViewOf(width: 10)
        tf_oldPasswd.addLeftViewOf(width: 10)
        tf_newPasswd.addLeftViewOf(width: 10)
    }
    
    
    private func changePasswordWith(dict:[String:Any],id:Int){
        
    }
    //MARK:- Action methods
    
    @IBAction func btnActn_cancel(_ sender: UIButton) {
        self.delegate?.didUpdatedPassword(success: true, message: "")
        self.dismiss(animated: false, completion: nil)
    }
    @IBAction func btnActn_save(_ sender: UIButton) {
        guard  let oldPasswd = tf_oldPasswd.text, oldPasswd.characters.count > 0,
        let newPasswd = tf_newPasswd.text, newPasswd.characters.count > 0,
        let confirmPasswd = tf_confirmPasswd.text, confirmPasswd.characters.count > 0 else{
            self.showAlertWith(message: "All fields are required.", title: "")
            return
        }
        
        let dict = ["doctor":["current_password":oldPasswd,
                              "password":newPasswd,
                              "password_confirmation":confirmPasswd]
                    ]
        
        //call Change password API
        if Reachability.isConnectedToNetwork(){
            spinner.show(allowInteraction: false)
            if let doctor = AppUser.doctor{
                Doctor.changePasswordFor(id: doctor.id, dict: dict, completion: { [unowned self] (message, success) in
                    
                    self.spinner.hide()
                    if success{
                        self.showAlertWith(title: "", message: message, handler: { (okActn) in
                            DispatchQueue.main.async {
                                self.delegate?.didUpdatedPassword(success: true, message: "")
                                 self.dismiss(animated: true, completion: nil)
                            }
                        })
                    }else{
                        self.showAlertWith(message: message, title: "")
                    }
                    
                }, failure: {[unowned self] (message) in
                    self.spinner.hide()
                     self.showAlertWith(message: message, title: "")
                })
            }else{
//                print("doctor not found")
            }
        }else{
            self.showAlertWith(message: ErrorMessages.kNetworkError, title: "")
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ChoosePasswordViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

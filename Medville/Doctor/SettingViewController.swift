//
//  SettingViewController.swift
//  Medville
//
//  Created by Jitendra Solanki on 9/26/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initializeUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Private helper methods
    private func initializeUI(){
        self.navigationItem.title = "Settings"
        
        let barBtn = UIBarButtonItem.init(image: #imageLiteral(resourceName: "logout"), style: .plain, target: self, action: #selector(logOutBtn_tapped))
        self.navigationItem.rightBarButtonItem = barBtn
    }
    
    //MARK:- Action methods
    
    @objc func logOutBtn_tapped(){
        UIApplication.shared.appDelegate.logoutForUser(type: .kDocotor)

       
        
    }
    @IBAction func btnActn_navigateToDetailFor(_ sender: UIButton) {
        
        if sender.tag == 101{
            //navigate to doctorProfileview controller
            self.navigationController?.pushViewController(viewControllerFor(identifier: VCIdentifier.kDoctorProfile), animated: true)
         }else{
            //navigate to webviewcontroller
            let webVc = viewControllerFor(identifier: VCIdentifier.kAboutWebView)as! WebViewController
            webVc.fileName = sender.tag == 102 ? "About" : "T&C"
            self.navigationController?.pushViewController(webVc, animated: true)
         }
        
        //
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  DoctorProfileViewController.swift
//  Medville
//
//  Created by Jitendra Solanki on 9/27/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit
import SDWebImage

class DoctorProfileViewController: ViewController {

    //MARK:- outlets
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var tf_countryCode: UITextField!
    @IBOutlet weak var tf_choosePassword: UITextField!
    @IBOutlet weak var tf_email: UITextField!
    @IBOutlet weak var tf_lastName: UITextField!
    @IBOutlet weak var tf_firstName: UITextField!
    @IBOutlet weak var imgV_profileImage: UIImageView!
    @IBOutlet weak var tf_mobileNumber:UITextField!
    
    @IBOutlet weak var tf_speciality: UITextField!
    
    @IBOutlet weak var tf_address: UITextField!
    
    //MARK:- ChoosePassword Popup
    @IBOutlet weak var choosePasswd_popupV: UIView!
    
    @IBOutlet weak var oldPasswdord: UITextField!
    
    @IBOutlet weak var newPassword: UITextField!
   
    @IBOutlet weak var confirmPasswd: UITextField!
    
    @IBOutlet weak var passwd_spinner: UIActivityIndicatorView!
    //MARK:- iVar
    var userImage:UIImage?
 
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initializeUI()
        setDataInFieldsFor(doctor: AppUser.doctor)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //MARK:- Private helper methods
    private func initializeUI(){
        self.viewDidLayoutSubviews()
        self.navigationItem.title = "Profile Settings"
        
        //add left bar button
        let btn =  UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(backBtnTapped))
        self.navigationItem.leftBarButtonItem = btn
 
         imgV_profileImage.layer.cornerRadius = imgV_profileImage.frame.size.width/2
         imgV_profileImage.layer.masksToBounds = true
        tf_mobileNumber.addInputAccessoryViewWith(target: self, selector: #selector(doneBtn_tapped))
        tf_countryCode.addInputAccessoryViewWith(target: self, selector: #selector(doneBtn_tapped))
        
        imgV_profileImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOn_imageView(sender:))))
        imgV_profileImage.isUserInteractionEnabled = true
        
        //setup ui for popup
        intializeUIFor_ChangePasswordPOPUP()
        
     }
    
    private func intializeUIFor_ChangePasswordPOPUP(){
        let borderColor = UIColor(r:197,g:197,b:197,alpha:1.0)
        newPassword.addBorderWith(color: borderColor, width: 0.8)
        oldPasswdord.addBorderWith(color: borderColor, width: 0.8)
        confirmPasswd.addBorderWith(color: borderColor, width: 0.8)
        
        confirmPasswd.addLeftViewOf(width: 10)
        oldPasswdord.addLeftViewOf(width: 10)
        newPassword.addLeftViewOf(width: 10)
        
        //add tap gesture
        choosePasswd_popupV.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapon_choosePasswordPopup(sender:))))

    }
    
    private func setDataInFieldsFor(doctor:Doctor?){
       // let doctor = AppUser.doctor
        tf_firstName.text = doctor?.first_name
        tf_lastName.text = doctor?.last_name
        tf_email.text = doctor?.email
        tf_countryCode.text = doctor?.countyCode
        tf_mobileNumber.text = doctor?.phoneNumber
        tf_speciality.text = doctor?.specialization.capitalized
        tf_address.text = doctor?.address
        
        imgV_profileImage.image = #imageLiteral(resourceName: "defualt-profile")
        
        imgV_profileImage.sd_setImage(with: doctor?.avatarPath) { (image, error, cacheType, url) in
            
        
            if image != nil{
                self.userImage = image
            }
        }
       // imgV_profileImage.sd_setImage(with: doctor?.avatarPath, placeholderImage: #imageLiteral(resourceName: "defualt-profile"), options:[])
//        if let doctor = AppUser.doctor {
//          //tf_firstName.text =
//        }
    }
    
    private func updateProfileWith(id:Int, dict:[String:Any]){
        spinner.show(allowInteraction: false)
        Doctor.updateProfileFor(id:id,dict: dict) { [unowned self] (doctor, message) in
            
            DispatchQueue.main.async {
                 self.spinner.hide()
            }
            if let updatedDoctor = doctor{
                
                //update doctor in user defaults
                AppUser.doctor = updatedDoctor
                
                DispatchQueue.main.async {
                     self.setDataInFieldsFor(doctor: updatedDoctor)
                    self.showAlertWith(message: message!, title: "")
                }
            }else{
                self.showAlertWith(message: message!, title: "")
            }
        }
    }
    
    
    private func changePasswordWith(dict:[String:Any]){
        
        choosePasswd_popupV.endEditing(true)
        
        if Reachability.isConnectedToNetwork(){
            passwd_spinner.show(allowInteraction: false)
            
            
            
            if let doctor = AppUser.doctor{
                Doctor.changePasswordFor(id: doctor.id, dict: dict, completion: { [unowned self] (message, success) in
                    
                    //empty data in fields
                    self.newPassword.text = ""
                    self.oldPasswdord.text = ""
                    self.confirmPasswd.text = ""
                    
                    self.passwd_spinner.hide()
                    if success{
                        self.showAlertWith(title: "", message: message, handler: { (okActn) in
                            DispatchQueue.main.async {
                                //self.delegate?.didUpdatedPassword(success: true, message: "")
                                //self.dismiss(animated: true, completion: nil)
                                
                                self.choosePasswd_popupV.isHidden = true
                            }
                        })
                    }else{
                        self.showAlertWith(message: message, title: "")
                    }
                    
                    }, failure: {[unowned self] (message) in
                        self.passwd_spinner.hide()
                        self.showAlertWith(message: message, title: "")
                })
            }else{
//                print("doctor not found")
            }
        }else{
            self.showAlertWith(message: ErrorMessages.kNetworkError, title: "")
        }
    }
    
    //MARK:- ChoosePassword Popup Action
    
    @IBAction func btnActn_cancel_popup(_ sender: Any) {
        choosePasswd_popupV.isHidden =  true
        newPassword.text = ""
        oldPasswdord.text = ""
        confirmPasswd.text = ""
        newPassword.resignFirstResponder()
        oldPasswdord.resignFirstResponder()
        confirmPasswd.resignFirstResponder()
    }
    
    @IBAction func btnActn_savePassword(_ sender: Any) {
        guard  let oldPasswd = oldPasswdord.text, oldPasswd.count > 0,
            let newPasswd = newPassword.text, newPasswd.count > 0,
            let confirmPasswd = confirmPasswd.text, confirmPasswd.count > 0 else{
                self.showAlertWith(message: "All fields are required.", title: "")
                return
        }
        
        let dict = ["doctor":["current_password":oldPasswd,
                              "password":newPasswd,
                              "password_confirmation":confirmPasswd]
        ]
        
        //call Change password API
        
        changePasswordWith(dict: dict)
        
    }
    
    
    //MARK:- Action methods
    @objc private func backBtnTapped(){
        self.navigationController?.popViewController(animated: true)
    }
    @objc private func doneBtn_tapped(){
        tf_mobileNumber.isFirstResponder ? tf_mobileNumber.resignFirstResponder() : tf_countryCode.resignFirstResponder()
       // tf_mobileNumber.resignFirstResponder()
    }
    
    @objc private func didTapOn_imageView(sender:UITapGestureRecognizer){
        openImagePicker()
        //self.openImagePicker()
    }
    
    @objc private func didTapon_choosePasswordPopup(sender:UITapGestureRecognizer){
        choosePasswd_popupV.endEditing(true)
        choosePasswd_popupV.isHidden = true

    }
   
    @IBAction func btnActn_saveChanges(_ sender: UIButton) {
    
        //do validation
        
        guard let fName = tf_firstName.text, fName.characters.count > 0,
        let lName = tf_lastName.text,lName.characters.count > 0,
        let email = tf_email.text, email.characters.count > 0,
       // let password = tf_choosePassword.text , password.characters.count > 0,
        let cCode = tf_countryCode.text, cCode.characters.count > 0,
        let mNum = tf_mobileNumber.text, mNum.characters.count > 0 ,
        let specialization = tf_speciality.text , specialization.characters.count > 0,
        let address = tf_address.text, address.characters.count > 0
        else{
            self.showAlertWith(message: "All fields are required.", title: "")
            return
        }
        
        var doctorHash = [
            "email":email,
            "specialization":specialization,
            "first_name":fName,
            "last_name":lName,
            "country_code":cCode,
            "phone_number":mNum,
            "description":"Doctor"
            ]
        
        
        if let profileImage = userImage {
            let imgData = UIImageJPEGRepresentation(profileImage, 0.0)
            let base64 = "data:image/jpeg;base64," + (imgData?.base64EncodedString())!
            doctorHash["avatar"] = base64
        }
        
        let dict:[String:Any] = ["doctor":doctorHash]
        
        
        
        if Reachability.isConnectedToNetwork(){
            updateProfileWith(id:AppUser.doctor!.id, dict: dict)
            
        }else{
            self.showAlertWith(message: ErrorMessages.kNetworkError, title: "")
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DoctorProfileViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == tf_choosePassword{
            
            choosePasswd_popupV.isHidden = false

//            let passwdVc = viewControllerFor(identifier: VCIdentifier.kChoosePassword)as! ChoosePasswordViewController
//            passwdVc.modalPresentationStyle = .overCurrentContext
//            passwdVc.delegate = self
//            self.present(passwdVc, animated: false, completion: {
//                //return false
//                // self.tabBarController?.tabBar.isUserInteractionEnabled = false
//            })
            return false
        }
        return true
    }
}


extension DoctorProfileViewController: ChoosePasswordDelegate{
    func didUpdatedPassword(success:Bool, message:String?) {
        //self.tabBarController?.tabBar.isUserInteractionEnabled = false
    }
}

extension DoctorProfileViewController {
   
    override func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        imgV_profileImage.layer.cornerRadius = imgV_profileImage.frame.size.width/2
        imgV_profileImage.layer.masksToBounds = true
        
        picker.dismiss(animated: true, completion: nil)
        if let image = info["UIImagePickerControllerOriginalImage"] as? UIImage{
//            print("image size== \(image.size)")
            
            if  let scaledImage = image.scaleImage(targetSize: CGSize(width: 200, height: 200)){
                self.imgV_profileImage.image = scaledImage
                self.userImage = scaledImage
                //self.selectedImage = scaledImage
            }
              SDImageCache.shared().clearMemory()
             SDImageCache.shared().clearDisk(onCompletion: {
             })
        }
    }
    
    override func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}


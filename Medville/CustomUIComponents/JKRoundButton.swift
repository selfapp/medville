//
//  JKRoundButton.swift
//  Medville
//
//  Created by Jitendra Solanki on 9/25/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

class JKRoundButton: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
 */
    override func draw(_ rect: CGRect) {
        // Drawing code
        self.layer.cornerRadius = rect.size.height/2
        self.layer.masksToBounds = true
    }
 

}




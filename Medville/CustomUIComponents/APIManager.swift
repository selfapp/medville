//
//  APIManager.swift
//  JustStuck
//
//  Created by Gyan Routray on 23/01/17.
//  Copyright © 2017 Headerlabs. All rights reserved.
//

import Foundation
import UIKit

//enum RequestType:String{
//    case post = "POST"
//    case get = "GET"
//    case put = "PUT"
//    case delete = "DELETE"
//}

let failureMessage = "Something went wrong. Please try later."
// MARK:- Common APIs
func renewAccessToken(completion: @escaping (_ status: Bool) -> Void) {
    
    guard let dictUser = UserDefaults.standard.object(forKey: kUserDetailsKey) as? [String: Any] else{
//        print("User data not found.")
        let appdelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        stopIndicator()
        appdelegate.logoutLocally()
        return
    }
    let refreshToken = dictUser[kRefreshTokenKey] as? String
//    let dictParams = ["device_uuid": DefaultParams.uuid,
//                      "device_token": DefaultParams.deviceToken,
//                      "app_version": DefaultParams.appVersion,
//                      "refresh_token": refreshToken]
    let userDict = ["user":["refresh_token":refreshToken ?? ""]]
    let request = urlRequestWith(path: "/sessions/renew_auth_token", params: userDict as Dictionary<String, AnyObject>?, authorize: false)
    post(request: request) { (success, object, statusCode) -> () in
        
        DispatchQueue.main.async(execute: {
            if success {
                //
                if let dictResult = object as? [String:Any],var resource = dictResult["resource"]as? [String:Any]{
                    let json = JSON(dictResult)
                    let userType =  json["type"].stringValue
                    let authToken =  json["auth_token"].stringValue
                    resource["auth_token"] = authToken
                    
                    //set usertype
                    AppUser.type = (userType.lowercased() == "doctor") ? kDOCTOR : kUSER
                    
                    //update user details
                    AppUser().updateUserDetails(detailsDict: resource)
                }
                ///
//                let authToken = object["auth_token"]
//                var resourceDict = object["resource"] as! [String: Any]
//                resourceDict["auth_token"] = authToken
//                //UserDefaults.standard .set(resourceDict, forKey: kUserDetailsKey)
//                //UserDefaults.standard.synchronize()
//                print("Success Access token dict: \(object)")
            } else {
//                print("Unable to renew access token. There may be some other error. So signing out the user")
                let appdelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
                stopIndicator()
                appdelegate.logoutLocally()
            }
             completion(success)
        })
    }
}
func stopIndicator() {
    DispatchQueue.main.async() {
        if UIApplication.shared.isIgnoringInteractionEvents{
            UIApplication.shared.endIgnoringInteractionEvents()
        }
    }
}

func login(email: String, password: String,  successBlock: @escaping (_ success: Bool, _ responseDict: NSMutableDictionary, _ statusCode: Int) -> Void, failure: @escaping ( _ message: String, _ statusCode: Int) -> Void) {
    let credentialsDict = ["email": email, "password": password]
    let defaultDict = ["uuid": DefaultParams.uuid, "token": DefaultParams.deviceToken,"app_version": DefaultParams.appVersion]
    
    let userDictc = ["user": credentialsDict, "device": defaultDict]
    let loginObject = makeParamsDict(dict: userDictc)
    let request = urlRequestWith(path: "/sign-in", params: loginObject as Dictionary<String, AnyObject>?, authorize: false)
    post(request: request) { (success, object, statusCode) -> () in
        
        DispatchQueue.main.async(execute: {
            if success {
                successBlock(success, object, statusCode)
            } else {
                var message = failureMessage
                if let passedMessage = object["error"] as? String {
                    message = passedMessage
                }
                failure(message, statusCode)
            }
        })
    }
}
//func callBack (successBlock:  @escaping (_ success: Bool, _ responseDict: Any?, _ statusCode: Int) -> Void, failureBlock: @escaping ( _ message: String, _ statusCode: Int) -> Void){
//    
//}

func signup(dataDict: [String: Any], successBlock: @escaping ( _ responseDict: NSMutableDictionary, _ statusCode: Int) -> Void, failure: @escaping ( _ message: String, _ statusCode: Int) -> Void) {
    
    let loginObject = makeParamsDict(dict: dataDict)
    let request = urlRequestWith(path: "/sign-up", params: loginObject as Dictionary<String, AnyObject>?, authorize: false)
    
    post(request: request) { (success, object, statusCode) -> () in
        DispatchQueue.main.async(execute: {
            if success {
                successBlock(object, statusCode)
            } else {
                var message = failureMessage
                if let passedMessage = object["error"] as? String {
                    message = passedMessage
                }
                failure(message, statusCode)
            }
        })
    }
    
}

func updateProfile(dataDict: [String: Any], successBlock: @escaping (_ success: Bool, _ responseDict: NSMutableDictionary, _ statusCode: Int) -> Void, failure: @escaping ( _ message: String, _ statusCode: Int) -> Void) {
    let serviceObject = makeParamsDict(dict: dataDict)
    let request = urlRequestWith(path: "/profile-update.json", params: serviceObject as Dictionary<String, AnyObject>?, authorize: true)
    put(request: request) { (success, object, statusCode) -> () in
        
        DispatchQueue.main.async(execute: {
            if success {
                successBlock(success, object, statusCode)
            } else {
                var message = failureMessage
                if let passedMessage = object["error"] as? Array<Any>,let messageStr = passedMessage[0] as? String  {
                    message = messageStr
                }
                failure(message, statusCode)

            }
        })
    }
}

func updatePassword(dataDict: [String: Any], successBlock: @escaping (_ responseDict: NSMutableDictionary, _ statusCode: Int) -> Void, failure: @escaping ( _ message: String, _ statusCode: Int) -> Void) {
    let serviceObject = makeParamsDict(dict: dataDict)
    let request = urlRequestWith(path: "/password-update.json", params: serviceObject as Dictionary<String, AnyObject>?, authorize: true)
    put(request: request) { (success, object, statusCode) -> () in
        
        DispatchQueue.main.async(execute: {
            if success {
                successBlock(object, statusCode)
            } else {
                var message = failureMessage
//                print(object)
                if let passedMessage = object["error"] as? [Any], passedMessage.count > 0 {
                    message = passedMessage[0] as! String
                }
                failure(message, statusCode)
                
            }
        })
    }
}

func getReviewsForServiceWith(serviceId: Int, successBlock: @escaping (_ responseDict: Any?) -> Void, failure: @escaping ( _ message: String, _ statusCode: Int) -> Void) {
    let request = urlRequestWith(path: "/service-reviews.json?service_id=\(serviceId)", params: nil, authorize: true)
    get(request: request) { (success, object, statusCode) -> () in
        DispatchQueue.main.async(execute: {
            if success {
                successBlock(object)
            } else {
                var message = failureMessage
                if let passedMessage = object["error"] as? [Any], passedMessage.count > 0 {
                    message = passedMessage[0] as! String
                }
                failure(message, statusCode)
                
            }
        })
    }
}
func registerCreditCard(dataDict: [String: Any], successBlock: @escaping ( _ responseDict: NSMutableDictionary, _ statusCode: Int) -> Void, failure: @escaping ( _ message: String, _ statusCode: Int) -> Void) {
    
    let loginObject = makeParamsDict(dict: dataDict)
    let request = urlRequestWith(path: "/create-source.json", params: loginObject as Dictionary<String, AnyObject>?, authorize: true)
    
    post(request: request) { (success, object, statusCode) -> () in
        DispatchQueue.main.async(execute: {
            if success {
                successBlock(object, statusCode)
            } else {
                var message = failureMessage
                if let passedMessage = object["error"] as? String {
                    message = passedMessage
                }
                failure(message, statusCode)
            }
        })
    }
    
}
// MARK:- User APIs
func searchWith(key: String, successBlock: @escaping (_ responseDict: Any?, _ statusCode: Int) -> Void, failure: @escaping ( _ message: String, _ statusCode: Int) -> Void){
    let searchPath = "/search-services?query=\(key)"
    let request = urlRequestWith(path: searchPath, params: nil, authorize: true)
    get(request: request) { (success, object, statusCode) -> () in
        
        DispatchQueue.main.async(execute: {
            if success {
                successBlock(object, statusCode)
            } else {
                var message = failureMessage
                if let passedMessage = object["error"] as? [Any], passedMessage.count > 0 {
                    message = passedMessage[0] as! String
                }
                failure(message, statusCode)

            }
        })
    }
}

//func createService(with dataDict: [String: Any], successBlock: @escaping (_ responseDict: Any?, _ statusCode: Int) -> Void, failure: @escaping ( _ message: String, _ statusCode: Int) -> Void) {
//    let loginObject = makeParamsDict(dict: dataDict)
//    let request = urlRequestWith(path: "/add-service.json", params: loginObject as Dictionary<String, AnyObject>?, authorize: true)
//    post(request: request) { (success, object, statusCode) -> () in
//        
//        DispatchQueue.main.async(execute: {
//            if success {
//                successBlock(object, statusCode)
//            } else {
//                var message = failureMessage
//                if let passedMessage = object["error"] as? [Any], passedMessage.count > 0 {
//                    message = passedMessage[0] as! String
//                }
//                failure(message, statusCode)
//
//            }
//        })
//    }
//}

func getProvidersDetailsForUser(userId: Int, successBlock: @escaping (_ responseDict: Any?, _ statusCode: Int) -> Void, failure: @escaping ( _ message: String, _ statusCode: Int) -> Void) {
    let request = urlRequestWith(path: "/user.json?id=\(userId)&type=provider", params: nil, authorize: true)
    get(request: request) { (success, object, statusCode) -> () in
        
        DispatchQueue.main.async(execute: {
            if success {
                successBlock(object, statusCode)
            } else {
                var message = failureMessage
                if let passedMessage = object["error"] as? [Any], passedMessage.count > 0 {
                    message = passedMessage[0] as! String
                }
                failure(message, statusCode)
                
            }
        })
    }
}

func getProvidersReviewsForUser(userId: Int, successBlock: @escaping (_ responseDict: Any?, _ statusCode: Int) -> Void, failure: @escaping ( _ message: String, _ statusCode: Int) -> Void) {
    let request = urlRequestWith(path: "/services.json", params: nil, authorize: true)
    get(request: request) { (success, object, statusCode) -> () in
        
        DispatchQueue.main.async(execute: {
            if success {
                successBlock(object, statusCode)
            } else {
                var message = failureMessage
                if let passedMessage = object["error"] as? [Any], passedMessage.count > 0 {
                    message = passedMessage[0] as! String
                }
                failure(message, statusCode)
            }
        })
    }
}

func createCall(dataDict: [String: Any], successBlock: @escaping ( _ responseDict: Any?, _ statusCode: Int) -> Void, failure: @escaping ( _ message: String, _ statusCode: Int) -> Void) {
    let loginObject = makeParamsDict(dict: dataDict)
    let request = urlRequestWith(path: "/calls.json", params: loginObject as Dictionary<String, AnyObject>?, authorize: true)
    post(request: request) { (success, object, statusCode) -> () in
        
        DispatchQueue.main.async(execute: {
            if success {
                successBlock(object, statusCode)
            } else {
                var message = failureMessage
                if let passedMessage = object["error"] as? [Any], passedMessage.count > 0 {
                    message = passedMessage[0] as! String
                }
                failure(message, statusCode)
            }
        })
    }
}

func submitReview(dataDict: [String: Any], successBlock: @escaping ( _ responseDict: Any?, _ statusCode: Int) -> Void, failure: @escaping ( _ message: String, _ statusCode: Int) -> Void) {
    let loginObject = makeParamsDict(dict: dataDict)
    let request = urlRequestWith(path: "/create-review.json", params: loginObject as Dictionary<String, AnyObject>?, authorize: true)
    post(request: request) { (success, object, statusCode) -> () in
        
        DispatchQueue.main.async(execute: {
            if success {
                successBlock(object, statusCode)
            } else {
                var message = failureMessage
                if let passedMessage = object["error"] as? [Any], passedMessage.count > 0 {
                    message = passedMessage[0] as! String
                }
                failure(message, statusCode)
            }
        })
    }
}

func getHistories( successBlock: @escaping (_ responseDict: Any?, _ statusCode: Int) -> Void, failure: @escaping ( _ message: String, _ statusCode: Int) -> Void) {
    let request = urlRequestWith(path: "/calls.json", params: nil, authorize: true)
    get(request: request) { (success, object, statusCode) -> () in
        
        DispatchQueue.main.async(execute: {
            if success {
                successBlock(object, statusCode)
            } else {
                var message = failureMessage
                if let passedMessage = object["error"] as? [Any], passedMessage.count > 0 {
                    message = passedMessage[0] as! String
                }
                failure(message, statusCode)
            }
        })
    }
}

func getCallSessionandToken(_ doctor_id: Int,  successBlock: @escaping (_ responseDict: Any?, _ statusCode: Int) -> Void, failure: @escaping ( _ message: String, _ statusCode: Int) -> Void) {
    let request = urlRequestWith(path: "/calls/opentok_session?doctor_id=\(doctor_id)", params: nil, authorize: true)
    get(request: request) { (success, object, statusCode) -> () in
        
        DispatchQueue.main.async(execute: {
            if success {
                successBlock(object, statusCode)
            } else {
                var message = failureMessage
                if let passedMessage = object["error"] as? [Any], passedMessage.count > 0 {
                    message = passedMessage[0] as! String
                }
                failure(message, statusCode)
            }
        })
    }
}



// MARK:- Provider APIs

//func addService(dataDict: [String: Any], successBlock: @escaping (_ success: Bool, _ responseDict: Any?, _ statusCode: Int) -> Void, failure: @escaping ( _ message: String, _ statusCode: Int) -> Void) {
//    let loginObject = makeParamsDict(dict: dataDict)
//    let request = urlRequestWith(path: "/add-service.json", params: loginObject as Dictionary<String, AnyObject>?, authorize: true)
//    post(request: request) { (success, object, statusCode) -> () in
//
//        DispatchQueue.main.async(execute: {
//            if success {
//                successBlock(success, object, statusCode)
//            } else {
//                var message = failureMessage
//                if let passedMessage = object["error"] as? String {
//                    message = passedMessage
//                }
//                failure(message, statusCode)
//
//            }
//        })
//    }
//}

//func editService(serviceId: Int, dataDict: [String: Any], successBlock: @escaping (_ success: Bool, _ responseDict: Any?, _ statusCode: Int) -> Void, failure: @escaping ( _ message: String, _ statusCode: Int) -> Void) {
//    let loginObject = makeParamsDict(dict: dataDict)
//    let request = urlRequestWith(path: "/update-service.json?id=\(serviceId)", params: loginObject as Dictionary<String, AnyObject>?, authorize: true)
//    put(request: request) { (success, object, statusCode) -> () in
//
//        DispatchQueue.main.async(execute: {
//            if success {
//                successBlock(success, object, statusCode)
//            } else {
//                var message = failureMessage
//                if let passedMessage = object["error"] as? String {
//                    message = passedMessage
//                }
//                failure(message, statusCode)
//
//            }
//        })
//    }
//}

//func deleteProviderService(serviceId: Int, successBlock: @escaping (_ success: Bool, _ responseDict: Any?, _ statusCode: Int) -> Void, failure: @escaping ( _ message: String, _ statusCode: Int) -> Void) {
//    let request = urlRequestWith(path: "/destroy-service.json?id=\(serviceId)", params: nil as Dictionary<String, AnyObject>?, authorize: true)
//    delete(request: request) { (success, object, statusCode) -> () in
//
//        DispatchQueue.main.async(execute: {
//            if success {
//                successBlock(success, object, statusCode)
//            } else {
//                var message = failureMessage
//                if let passedMessage = object["error"] as? String {
//                    message = passedMessage
//                }
//                failure(message, statusCode)
//
//            }
//        })
//    }
//}

func searchWithkey(key: String, successBlock: @escaping (_ success: Bool, _ responseDict: Any?, _ statusCode: Int) -> Void, failure: @escaping ( _ message: String, _ statusCode: Int) -> Void) {
    let request = urlRequestWith(path: "/services.json", params: nil, authorize: true)
    get(request: request) { (success, object, statusCode) -> () in
        
        DispatchQueue.main.async(execute: {
            if success {
                successBlock(success, object, statusCode)
            } else {
                var message = failureMessage
                if let passedMessage = object["error"] as? String {
                    message = passedMessage
                }
                failure(message, statusCode)
                
            }
        })
    }
}
//func getProviderServices( successBlock: @escaping (_ success: Bool, _ responseDict: Any?, _ statusCode: Int) -> Void, failure: @escaping ( _ message: String, _ statusCode: Int) -> Void) {
//    let request = urlRequestWith(path: "/services.json", params: nil, authorize: true)
//    get(request: request) { (success, object, statusCode) -> () in
//
//        DispatchQueue.main.async(execute: {
//            if success {
//                successBlock(success, object, statusCode)
//            } else {
//                var message = failureMessage
//                if let passedMessage = object["error"] as? String {
//                    message = passedMessage
//                }
//                failure(message, statusCode)
//
//            }
//        })
//    }
//}

func updateDoctorDetails(dataDict: [String: Any], successBlock: @escaping (_ success: Bool, _ responseDict: Any?, _ statusCode: Int) -> Void, failure: @escaping ( _ message: String, _ statusCode: Int) -> Void) {
    let loginObject = makeParamsDict(dict: dataDict)
    let request = urlRequestWith(path: "/provider-detail.json", params: loginObject as Dictionary<String, AnyObject>?, authorize: true)
    post(request: request) { (success, object, statusCode) -> () in
        
        DispatchQueue.main.async(execute: {
            if success {
                successBlock(success, object, statusCode)
            } else {
                var message = failureMessage
                if let passedMessage = object["error"] as? String {
                    message = passedMessage
                }
                failure(message, statusCode)
                
            }
        })
    }
}
//func getProviderReview( successBlock: @escaping (_ responseDict: NSMutableDictionary, _ statusCode: Int) -> Void, failure: @escaping ( _ message: String, _ statusCode: Int) -> Void) {
//    let request = urlRequestWith(path: "/provider-reviews.json", params: nil as Dictionary<String, AnyObject>?, authorize: true)
//    get(request: request) { (success, object, statusCode) -> () in
//
//        DispatchQueue.main.async(execute: {
//            if success {
//                successBlock(object, statusCode)
//            } else {
//                var message = failureMessage
//                if let passedMessage = object["error"] as? String {
//                    message = passedMessage
//                }
//                failure(message, statusCode)
//
//            }
//        })
//    }
//}


func manageNullValueFrom(dataObject: NSMutableDictionary?) -> AnyObject {
    
       return dataObject?.copy() as AnyObject
}


//MARK:- Mobile verification
///////////
func renewAccessToekn(completion:@escaping (_ success:Bool)->Void){
    var dict:[String:Any]!
    
    guard let userType = AppUser.userType else{
//        print("User type not found, so logout user")
        if let topVc = UIApplication.topViewController(){
            DispatchQueue.main.async {
                if UIApplication.shared.isIgnoringInteractionEvents{
                    UIApplication.shared.endIgnoringInteractionEvents()
                }
            }
            topVc.showAlertWith(title: "", message: "You have to login again to continue.", handler: { (okActn) in
               // DispatchQueue.main.async {
                    UIApplication.shared.appDelegate.logoutForUser(type: .kPatient)
                //}
            })
        }
        return
    }
    
    var url:String!
    if userType == .kPatient{
        dict = ["patient":["refresh_token":AppUser.refreshToken]]
        url = "/doctors/renew_auth_token"
    }else{
        dict = ["doctor":["refresh_token":AppUser.refreshToken]]
        url = "/patients/renew_auth_token"

    }
    
    //call API
    makeHttpPostRequestWith(url: url, dataDict: dict, shouldAuthorize: false, successBlock: { (success, response, statusCode) in
        
        
        DispatchQueue.main.async(execute: {
            
            guard
                let json = response as? [String:Any],
                let authToken = json[""]as? String,
                let refreshToken = json[""]as? String else{
                    
//                    print("Unable to renew access token so logging out============")
                    if UIApplication.shared.isIgnoringInteractionEvents{
                        UIApplication.shared.endIgnoringInteractionEvents()
                    }
                    if let topVc = UIApplication.topViewController(){
                        topVc.showAlertWith(title: "", message: "You have to login again to continue.", handler: { (okActn) in
                            // DispatchQueue.main.async {
                            UIApplication.shared.appDelegate.logoutForUser(type: userType)
                            //}
                        })
                    }
                    return
            }
            
//            print("Access token renewed============")
            AppUser.authToken = authToken
            AppUser.refreshToken = refreshToken
            completion(true)
            
        })
        
        
    }) { (message, statusCode) in
//        print("Unable to renew access token. There may be some other error. So signing out the user")
        
        DispatchQueue.main.async(execute: {
            if UIApplication.shared.isIgnoringInteractionEvents{
                UIApplication.shared.endIgnoringInteractionEvents()
            }

            if let topVc = UIApplication.topViewController(){
                topVc.showAlertWith(title: "", message: "You have to login again to continue.", handler: { (okActn) in
                    // DispatchQueue.main.async {
                    UIApplication.shared.appDelegate.logoutForUser(type: userType)
                    //}
                })
            }
            
        })
        
        
    }
}

func makeHttpPostRequestWith(url:String,dataDict:[String:Any], shouldAuthorize authorize:Bool,successBlock: @escaping (_ success: Bool, _ responseDict: Any?, _ statusCode: Int) -> Void, failure: @escaping ( _ message: String, _ statusCode: Int) -> Void){
    let loginObject = makeParamsDict(dict: dataDict)
    let request = urlRequestWith(path: url, params: loginObject as Dictionary<String, AnyObject>?, authorize: authorize)
    post(request: request) { (success, object, statusCode) -> () in
        
        DispatchQueue.main.async(execute: {
            if success {
                successBlock(success, object, statusCode)
            } else {
                
                if statusCode == 401 && authorize {
                    //renew access token
                    renewAccessToekn(completion: { (success) in
                        if success{
                            makeHttpPostRequestWith(url: url, dataDict: dataDict, shouldAuthorize: authorize, successBlock: successBlock, failure: failure)
                        }
                    })
                    
                }else{
                    //call failure block

                    let message = failureMessage
                    guard let errorMessage = object["message"]as? String  else{
                        failure(message,statusCode)
                        return
                    }
                    failure(errorMessage, statusCode)

                }
            }
        })
    }
}

func makePostForPusher(param:[String:Any]){
    /*
     userName - durgesh@headerlabs.com
     password - Welcome@123
     */
    let headers = [
        "content-type": "application/json",
        "authorization": "Bearer 378EFDF2F5D81ECF4FB96B3C37D1F6C",
        "cache-control": "no-cache"
    ]
    
    
    
    let request = NSMutableURLRequest(url: NSURL(string: "https://5fca81ce-ef08-464a-8014-a285b706f5dc.pushnotifications.pusher.com/publish_api/v1/instances/5fca81ce-ef08-464a-8014-a285b706f5dc/publishes")! as URL,
                                      cachePolicy: .useProtocolCachePolicy,
                                      timeoutInterval: 10.0)
    request.httpMethod = "POST"
    request.allHTTPHeaderFields = headers
    
    do{
        let postData = try JSONSerialization.data(withJSONObject: param, options: [])
            request.httpBody = postData as Data
    }catch{}
//    request.httpBody = postData as Data
    
    let session = URLSession.shared
    let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
        if (error != nil) {
            print(error)
        } else {
            let httpResponse = response as? HTTPURLResponse
            print(httpResponse)
        }
    })
    
    dataTask.resume()

}


func makeGetHttpRequestWith(url:String,dataDict:[String:Any]?, successBlock: @escaping (_ success: Bool, _ responseDict: Any?, _ statusCode: Int) -> Void, failure: @escaping ( _ message: String, _ statusCode: Int) -> Void){
    let request = urlRequestWith(path: url, params: dataDict, authorize: true)
    get(request: request) { (success, object, statusCode) -> () in
        
        DispatchQueue.main.async(execute: {
            if success {
                successBlock(success,object, statusCode)
            } else {
                
                if statusCode == 401 {
                    //renew access token
                    renewAccessToekn(completion: { (success) in
                        if success{
                            
                            makeGetHttpRequestWith(url: url, dataDict: dataDict, successBlock: successBlock, failure: failure)
                            
                        }
                    })
                
                }else{
                    //call failure block
                    let message = failureMessage
                    guard let errorMessage = object["message"]as? String  else{
                        failure(message,statusCode)
                        return
                    }
                    failure(errorMessage, statusCode)
                    
                }
            }
        })
    }
}


func makePutHttpRequestWith(url:String,dataDict:[String:Any]?, successBlock: @escaping (_ success: Bool, _ responseDict: Any?, _ statusCode: Int) -> Void, failure: @escaping ( _ message: String, _ statusCode: Int) -> Void){
    let request = urlRequestWith(path: url, params: dataDict, authorize: true)
    
    put(request: request) { (success, object, statusCode) in
        DispatchQueue.main.async(execute: {
            if success {
                successBlock(success,object, statusCode)
            } else {
                
                if statusCode == 401{
                    
                    //renew access Token
                    renewAccessToekn(completion: { (success) in
                        if success{
                            makePutHttpRequestWith(url: url, dataDict: dataDict, successBlock: successBlock, failure: failure)
                        }
                    })
                }else{

                    //call failure block
                    let message = failureMessage
                    guard let errorMessage = object["message"]as? String  else{
                        failure(message,statusCode)
                        return
                    }
                    failure(errorMessage, statusCode)
                    
                }
            }
        })
    }
    
}

// Delete
func makeDeleteHttpRequestWith(url:String,dataDict:[String:Any]?, successBlock: @escaping (_ success: Bool, _ responseDict: Any?, _ statusCode: Int) -> Void, failure: @escaping ( _ message: String, _ statusCode: Int) -> Void){
    let request = urlRequestWith(path: url, params: dataDict, authorize: true)
    
    delete(request: request) { (success, object, statusCode) in
        DispatchQueue.main.async(execute: {
            if success {
                successBlock(success,object, statusCode)
            } else {
                
                if statusCode == 401{
                    
                    //renew access Token
                    renewAccessToekn(completion: { (success) in
                        if success{
                            makePutHttpRequestWith(url: url, dataDict: dataDict, successBlock: successBlock, failure: failure)
                        }
                    })
                }else{
                    
                    //call failure block
                    let message = failureMessage
                    guard let errorMessage = object["message"]as? String  else{
                        failure(message,statusCode)
                        return
                    }
                    failure(errorMessage, statusCode)
                    
                }
            }
        })
    }
    
}



 

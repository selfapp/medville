//
//  Extended.swift
//  Pocco
//
//  Created by Jitendra Solanki on 7/5/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication {
    
    var appDelegate:AppDelegate {
        return self.delegate as! AppDelegate
    }
    
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
//        if let sideMenuController = controller as? MFSideMenuContainerViewController{
//            return topViewController(controller: sideMenuController.centerViewController as! UIViewController?)
//        }
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

extension UIColor {
    convenience init(r: Float, g: Float, b: Float,alpha:Float) {
        
        self.init(displayP3Red: CGFloat(r/255), green: CGFloat(g/255), blue: CGFloat(b/255), alpha: CGFloat(alpha))
        //self.init(colorLiteralRed: r/255, green: g/255, blue: b/255, alpha: alpha)
        
    }
    
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
    
}

extension UIActivityIndicatorView{
    
    func show(allowInteraction:Bool){
        if !allowInteraction{
            UIApplication.shared.beginIgnoringInteractionEvents()
        }
        self.startAnimating()
    }
    
    func hide(){
        DispatchQueue.main.async {
            
            if UIApplication.shared.isIgnoringInteractionEvents{
                UIApplication.shared.endIgnoringInteractionEvents()
            }
            self.stopAnimating()
        }
    }
}

extension UIView{
    
    func firstResponder()-> UIView?{
        if self.isFirstResponder{
            return self
        }
        
        for view in self.subviews{
            
            if view.subviews.count > 0{
                
                if  let responderV = view.firstResponder(){
                    return responderV
                }
 
            }
            if view.isFirstResponder{
                return view
            }
        }
        return nil
    }
    
    func makeCornerRound(cornerRadius:CGFloat){
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
    }
    
    func addBorderWith(color:UIColor, width:CGFloat){
        self.layer.borderWidth = width
        self.layer.borderColor = color.cgColor
    }
    
    func addBorderWith(borderWidth width:CGFloat, withBorderColor color:UIColor, withCornerRadious radious:CGFloat) {
        self.layer.borderColor = color.cgColor;
        self.layer.borderWidth = width
        if radious != 0 {
            self.layer.cornerRadius = radious
            self.clipsToBounds = true
        }
    }
    
    func addShadowWith(opacity:Float,radius:CGFloat, offset:CGSize,color: UIColor){
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = radius
        self.layer.shadowOpacity = opacity
        self.layer.shadowPath = UIBezierPath(rect:bounds).cgPath
        //layer.shouldRasterize = true
    }
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}

extension UITextField{
    func setPlaceholderColorWith(color:UIColor){
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "", attributes: [NSAttributedStringKey.foregroundColor:color])
    }
   
    //to add a tool bar with done button
    func addInputAccessoryViewWith(target:UIViewController, selector:Selector){
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem.init(title: "Done", style: UIBarButtonItemStyle.plain, target: target, action: selector)
        toolbar.setItems([doneButton], animated: true)
        self.inputAccessoryView = toolbar
    }
    
    func addLeftViewOf(width:CGFloat){
        let leftView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: self.frame.size.height))
        self.leftView = leftView
        self.leftViewMode = .always
    }
}

extension UIImage{
    func scaleImage(targetSize maxSize:CGSize)->UIImage?{
        
        let imgWidth = self.size.width
        let imgHeight = self.size.height
        
        if imgWidth <= maxSize.width && imgHeight <= maxSize.height{
            return self
        }
        
        var bounds = CGRect(x: 0, y: 0, width: maxSize.width, height: maxSize.height)
        
        if imgWidth > maxSize.width || imgHeight > maxSize.height{
            
            let ratio = imgWidth/imgHeight
            
            if ratio > 1{
                bounds.size.width = maxSize.width
                bounds.size.height = bounds.size.width*ratio
            }else{
                bounds.size.height = maxSize.height
                bounds.size.width = bounds.size.height*ratio
            }
        }
        
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, 1.0)
        self.draw(in: bounds)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        
        if newImage != nil{
            
            if let data = UIImagePNGRepresentation(newImage!){
                UIGraphicsEndImageContext()
                return UIImage(data: data)
            }
            
        }
        UIGraphicsEndImageContext()
        return newImage
        
    }
}
extension UITextView{
    //to add a tool bar with done button
    func addInputAccessoryViewWith(target:UIViewController, selector:Selector){
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem.init(title: "Done", style: UIBarButtonItemStyle.plain, target: target, action: selector)
        toolbar.setItems([doneButton], animated: true)
        self.inputAccessoryView = toolbar
    }
    
}

extension UIViewController{
    
    func viewControllerFor(identifier:String)->UIViewController{
        let stroyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc =  stroyBoard.instantiateViewController(withIdentifier: identifier)
        return vc
    }
    
    func showOkAlertWithTitle(title: String, message: String, okCompletion:((UIAlertAction)->Void)?) {
        let alertVC = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .cancel, handler:okCompletion)
        alertVC.addAction(alertAction)
        self.present(alertVC, animated: true, completion: nil)
    }

    func showAlertWith(message:String, title:String, okCompletion:  ((UIAlertAction)->Void)?, cancelCompletion: ((UIAlertAction)->Void)?){
        DispatchQueue.main.async {
            let alertController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
            let okAction = UIAlertAction.init(title: "Ok", style:.default, handler: okCompletion)
            let cancelAction =  UIAlertAction.init(title:"Cancel",style:.cancel,handler:cancelCompletion)
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func showAlertWith(title:String,message:String, handler:((UIAlertAction)->Void)?){
        DispatchQueue.main.async {
            let alertController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
            let okActn = UIAlertAction.init(title: "Ok", style: .default, handler: { (okAction) in
                handler?(okAction)
            })
            alertController.addAction(okActn)
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    func showAlertWith(message:String,title:String){
        DispatchQueue.main.async {
            let alertController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
            let okActn = UIAlertAction.init(title: "Ok", style: .cancel, handler:nil)
            alertController.addAction(okActn)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerWith(delegate:(UIImagePickerControllerDelegate & UINavigationControllerDelegate)?){
        let actionSheet =  UIAlertController(title: "Medville", message: "", preferredStyle: .actionSheet)
        
        let camera  = UIAlertAction(title: "Camera", style: .default) { (cameraAction) in
            
            let mediaType = AVMediaType.video
            
            let authorizationStatus =  AVCaptureDevice.authorizationStatus(for: mediaType)
            
            if authorizationStatus == .authorized{
                self.prsentImagePickerWith(option: "Camera",delegate: delegate)
                
            }else if authorizationStatus == .notDetermined{
                AVCaptureDevice.requestAccess(for: mediaType, completionHandler: { (newStatus) in
                    if newStatus {
                        let authorizationStatus1 =  AVCaptureDevice.authorizationStatus(for: mediaType)
                        if authorizationStatus1 == .authorized{
                            self.prsentImagePickerWith(option: "Camera",delegate: delegate)
                        }
                        
                    }else{
                        
                    }
                })
            }else{
                
                let message = "It seems access for camera is denied for Medville. To make it work, Go to Setting -> Privacy -> Camera and enable the access for Medville app"
                self.showAlertWith(title: "", message: message, handler: nil)
            }
        }
        
        let gallery  = UIAlertAction(title: "Photo Album", style: .default) { (galleryActn) in
            
            let status = PHPhotoLibrary.authorizationStatus()
            
            switch status{
                
            case .authorized:
                self.prsentImagePickerWith(option: "PhotoAlbum",delegate: delegate)
            case .notDetermined:
                PHPhotoLibrary.requestAuthorization({ (newStatus) in
                    if newStatus == PHAuthorizationStatus.authorized{
                        self.prsentImagePickerWith(option: "PhotoAlbum",delegate: delegate)
                        
                    }else{
                        
                    }
                })
            default:
                self.showAlertWith(title: "", message: "The access is not granted to use photo library. Go to Setting app to change the access for Medville", handler: nil)
                
            }
        }
        
        let cancel  = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        actionSheet.addAction(camera)
        actionSheet.addAction(gallery)
        actionSheet.addAction(cancel)
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    private func prsentImagePickerWith(option:String,delegate:(UIImagePickerControllerDelegate & UINavigationControllerDelegate)?){
        let uiimagePicker = UIImagePickerController.init()
        uiimagePicker.delegate = delegate
        
        if option == "Camera" && UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
            uiimagePicker.sourceType = .camera
        }else {
            uiimagePicker.sourceType = .photoLibrary
        }
        
        DispatchQueue.main.async {
            self.present(uiimagePicker, animated: true, completion: nil)
        }
        
    }

}

extension UINavigationBar{
    
    func gradeintBarTintColor(with gradient:[UIColor]){
        var frameAndStatusBarSize = self.bounds
        frameAndStatusBarSize.size.height +=  20
        setBackgroundImage(UINavigationBar.gradient(size: frameAndStatusBarSize.size, color: gradient), for: .default)
    }
    
    static func gradient(size:CGSize,color:[UIColor]) -> UIImage?{
        //turn color into cgcolor
        let colors = color.map{$0.cgColor}
        
        //begin graphics context
        UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        
        // From now on, the context gets ended if any return happens
        defer {UIGraphicsEndImageContext()}
        
        //create core graphics context
        let locations:[CGFloat] = [0.0,1.0]
        guard let gredient = CGGradient.init(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: colors as NSArray as CFArray, locations: locations) else {
            return nil
        }
        //draw the gradient
        context.drawLinearGradient(gredient, start: CGPoint(x:0.0,y:0.0), end: CGPoint(x:0.0,y:size.height), options: [])
        
        // Generate the image (the defer takes care of closing the context)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}


extension Date{
    
//    var time:Time{
//        return Time(self)
//    }
    
    func nextDay()->Date?{
        return NSCalendar.current.date(byAdding: .day, value: 1, to: self)
    }
    
    func previousDay() -> Date?{
        return NSCalendar.current.date(byAdding: .day, value: -1, to: self)
    }
    
    //MARK "Aug-30-2017"
    
    func toOnlyDate()->String{
      let formatter = DateFormatter()
        formatter.dateFormat = "MMM-dd-yyyy"
        return formatter.string(from: self)
     }
    
    func toOnlyTime()-> String{
        let formatter  = DateFormatter()
        formatter.dateFormat = "hh:mma"
        return formatter.string(from: self)
    }
    
    //MARK:- "Aug-30-2017 9:00AM
    func toDateAndTime()->String{
        let dtf = DateFormatter()
        dtf.dateFormat = "MMM-dd-yyyy hh:mma"
        return dtf.string(from: self)
    }
    
    //PoccoFormat= "Aug-30-2017 9:00AM"

    static func getDateInPoccoFormat(from:String)->Date?{
        let formatter = DateFormatter()
        
        formatter.dateFormat = "MMM-dd-yyyy hh:mma"
        return formatter.date(from: from)
    }
    
    //to get a date from a given string and we have only date like this:"Aug-30-2017"
    static func getDateFrom(from:String)-> Date?{
        let formatter = DateFormatter()
        
        //we are appending string to make given string in UTC format
        let strDate = from.appending(" 09:06:32 +0000")
        formatter.dateFormat = "MMM-dd-yyyy HH:mm:ss z"
        return formatter.date(from: strDate)
    }
    
    func convertInUTC() -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let strDate = formatter.string(from: self)
        
        return strDate
    }
    static func getCurrentTimeInUTC(from:String)->Date?{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        
        return formatter.date(from:from)
    }
}


 

extension NSLocale{
    
    func ISDCode() -> String {
        
        
        
        let dialingCodes = [
            
            "Abkhazia"                                     : "+7 840",
            "Afghanistan"                                  : "+93",
            "Albania"                                      : "+355",
            "Algeria"                                      : "+213",
            "American Samoa"                               : "+1 684",
            "Andorra"                                      : "+376",
            "Angola"                                       : "+244",
            "Anguilla"                                     : "+1 264",
            "Antigua and Barbuda"                          : "+1 268",
            "Argentina"                                    : "+54",
            "Armenia"                                      : "+374",
            "Aruba"                                        : "+297",
            "Ascension"                                    : "+247",
            "Australia"                                    : "+61",
            "Australian External Territories"              : "+672",
            "Austria"                                      : "+43",
            "Azerbaijan"                                   : "+994",
            "Bahamas"                                      : "+1 242",
            "Bahrain"                                      : "+973",
            "Bangladesh"                                   : "+880",
            "Barbados"                                     : "+1 246",
            "Barbuda"                                      : "+1 268",
            "Belarus"                                      : "+375",
            "Belgium"                                      : "+32",
            "Belize"                                       : "+501",
            "Benin"                                        : "+229",
            "Bermuda"                                      : "+1 441",
            "Bhutan"                                       : "+975",
            "Bolivia"                                      : "+591",
            "Bosnia and Herzegovina"                       : "+387",
            "Botswana"                                     : "+267",
            
            "Brazil"                                       : "+55",
            
            "British Indian Ocean Territory"               : "+246",
            
            "British Virgin Islands"                       : "+1 284",
            
            "Brunei"                                       : "+673",
            
            "Bulgaria"                                     : "+359",
            
            "Burkina Faso"                                 : "+226",
            
            "Burundi"                                      : "+257",
            
            "Cambodia"                                     : "+855",
            
            "Cameroon"                                     : "+237",
            
            "Canada"                                       : "+1",
            
            "Cape Verde"                                   : "+238",
            
            "Cayman Islands"                               : "+ 345",
            
            "Central African Republic"                     : "+236",
            
            "Chad"                                         : "+235",
            
            "Chile"                                        : "+56",
            
            "China"                                        : "+86",
            
            "Christmas Island"                             : "+61",
            
            "Cocos-Keeling Islands"                        : "+61",
            
            "Colombia"                                     : "+57",
            
            "Comoros"                                      : "+269",
            
            "Congo"                                        : "+242",
            
            "Congo, Dem. Rep. of (Zaire)"                  : "+243",
            
            "Cook Islands"                                 : "+682",
            
            "Costa Rica"                                   : "+506",
            
            "Ivory Coast"                                  : "+225",
            
            "Croatia"                                      : "+385",
            
            "Cuba"                                         : "+53",
            
            "Curacao"                                      : "+599",
            
            "Cyprus"                                       : "+537",
            
            "Czech Republic"                               : "+420",
            
            "Denmark"                                      : "+45",
            
            "Diego Garcia"                                 : "+246",
            
            "Djibouti"                                     : "+253",
            
            "Dominica"                                     : "+1 767",
            
            "Dominican Republic"                           : "+1 809",
            
            "East Timor"                                   : "+670",
            
            "Easter Island"                                : "+56",
            
            "Ecuador"                                      : "+593",
            
            "Egypt"                                        : "+20",
            
            "El Salvador"                                  : "+503",
            
            "Equatorial Guinea"                            : "+240",
            
            "Eritrea"                                      : "+291",
            
            "Estonia"                                      : "+372",
            
            "Ethiopia"                                     : "+251",
            
            "Falkland Islands"                             : "+500",
            
            "Faroe Islands"                                : "+298",
            
            "Fiji"                                         : "+679",
            
            "Finland"                                      : "+358",
            
            "France"                                       : "+33",
            
            "French Antilles"                              : "+596",
            
            "French Guiana"                                : "+594",
            
            "French Polynesia"                             : "+689",
            
            "Gabon"                                        : "+241",
            
            "Gambia"                                       : "+220",
            
            "Georgia"                                      : "+995",
            
            "Germany"                                      : "+49",
            
            "Ghana"                                        : "+233",
            
            "Gibraltar"                                    : "+350",
            
            "Greece"                                       : "+30",
            
            "Greenland"                                    : "+299",
            
            "Grenada"                                      : "+1 473",
            
            "Guadeloupe"                                   : "+590",
            
            "Guam"                                         : "+1 671",
            
            "Guatemala"                                    : "+502",
            
            "Guinea"                                       : "+224",
            
            "Guinea-Bissau"                                : "+245",
            
            "Guyana"                                       : "+595",
            
            "Haiti"                                        : "+509",
            
            "Honduras"                                     : "+504",
            
            "Hong Kong SAR China"                          : "+852",
            
            "Hungary"                                      : "+36",
            
            "Iceland"                                      : "+354",
            
            "India"                                        : "+91",
            
            "Indonesia"                                    : "+62",
            
            "Iran"                                         : "+98",
            
            "Iraq"                                         : "+964",
            
            "Ireland"                                      : "+353",
            
            "Israel"                                       : "+972",
            
            "Italy"                                        : "+39",
            
            "Jamaica"                                      : "+1 876",
            
            "Japan"                                        : "+81",
            
            "Jordan"                                       : "+962",
            
            "Kazakhstan"                                   : "+7 7",
            
            "Kenya"                                        : "+254",
            
            "Kiribati"                                     : "+686",
            
            "North Korea"                                  : "+850",
            
            "South Korea"                                  : "+82",
            
            "Kuwait"                                       : "+965",
            
            "Kyrgyzstan"                                   : "+996",
            
            "Laos"                                         : "+856",
            
            "Latvia"                                       : "+371",
            
            "Lebanon"                                      : "+961",
            
            "Lesotho"                                      : "+266",
            
            "Liberia"                                      : "+231",
            
            "Libya"                                        : "+218",
            
            "Liechtenstein"                                : "+423",
            
            "Lithuania"                                    : "+370",
            
            "Luxembourg"                                   : "+352",
            
            "Macau SAR China"                              : "+853",
            
            "Macedonia"                                    : "+389",
            
            "Madagascar"                                   : "+261",
            
            "Malawi"                                       : "+265",
            
            "Malaysia"                                     : "+60",
            
            "Maldives"                                     : "+960",
            
            "Mali"                                         : "+223",
            
            "Malta"                                        : "+356",
            
            "Marshall Islands"                             : "+692",
            
            "Martinique"                                   : "+596",
            
            "Mauritania"                                   : "+222",
            
            "Mauritius"                                    : "+230",
            
            "Mayotte"                                      : "+262",
            
            "Mexico"                                       : "+52",
            
            "Micronesia"                                   : "+691",
            
            "Midway Island"                                : "+1 808",
            
            "Moldova"                                      : "+373",
            
            "Monaco"                                       : "+377",
            
            "Mongolia"                                     : "+976",
            
            "Montenegro"                                   : "+382",
            
            "Montserrat"                                   : "+1664",
            
            "Morocco"                                      : "+212",
            
            "Myanmar"                                      : "+95",
            
            "Namibia"                                      : "+264",
            
            "Nauru"                                        : "+674",
            
            "Nepal"                                        : "+977",
            
            "Netherlands"                                  : "+31",
            
            "Netherlands Antilles"                         : "+599",
            
            "Nevis"                                        : "+1 869",
            
            "New Caledonia"                                : "+687",
            
            "New Zealand"                                  : "+64",
            
            "Nicaragua"                                    : "+505",
            
            "Niger"                                        : "+227",
            
            "Nigeria"                                      : "+234",
            "Niue"                                         : "+683",
            "Norfolk Island"                               : "+672",
            "Northern Mariana Islands"                     : "+1 670",
            "Norway"                                       : "+47",
            "Oman"                                         : "+968",
            "Pakistan"                                     : "+92",
            "Palau"                                        : "+680",
            "Palestinian Territory"                        : "+970",
            "Panama"                                       : "+507",
            "Papua New Guinea"                             : "+675",
            "Paraguay"                                     : "+595",
            "Peru"                                         : "+51",
            "Philippines"                                  : "+63",
            "Poland"                                       : "+48",
            "Portugal"                                     : "+351",
            "Puerto Rico"                                  : "+1 787",
            "Qatar"                                        : "+974",
            "Reunion"                                      : "+262",
            "Romania"                                      : "+40",
            "Russia"                                       : "+7",
            "Rwanda"                                       : "+250",
            "Samoa"                                        : "+685",
            "San Marino"                                   : "+378",
            "Saudi Arabia"                                 : "+966",
            "Senegal"                                      : "+221",
            "Serbia"                                       : "+381",
            "Seychelles"                                   : "+248",
            "Sierra Leone"                                 : "+232",
            "Singapore"                                    : "+65",
            "Slovakia"                                     : "+421",
            "Slovenia"                                     : "+386",
            "Solomon Islands"                              : "+677",
            "South Africa"                                 : "+27",
            "South Georgia and the South Sandwich Islands" : "+500",
            "Spain"                                        : "+34",
            "Sri Lanka"                                    : "+94",
            "Sudan"                                        : "+249",
            "Suriname"                                     : "+597",
            "Swaziland"                                    : "+268",
            "Sweden"                                       : "+46",
            "Switzerland"                                  : "+41",
            "Syria"                                        : "+963",
            "Taiwan"                                       : "+886",
            "Tajikistan"                                   : "+992",
            "Tanzania"                                     : "+255",
            "Thailand"                                     : "+66",
            "Timor Leste"                                  : "+670",
            "Togo"                                         : "+228",
            "Tokelau"                                      : "+690",
            "Tonga"                                        : "+676",
            "Trinidad and Tobago"                          : "+1 868",
            "Tunisia"                                      : "+216",
            "Turkey"                                       : "+90",
            "Turkmenistan"                                 : "+993",
            "Turks and Caicos Islands"                     : "+1 649",
            "Tuvalu"                                       : "+688",
            "Uganda"                                       : "+256",
            "Ukraine"                                      : "+380",
            "United Arab Emirates"                         : "+971",
            "United Kingdom"                               : "+44",
            "United States"                                : "+1",
            "Uruguay"                                      : "+598",
            "U.S. Virgin Islands"                          : "+1 340",
            "Uzbekistan"                                   : "+998",
            "Vanuatu"                                      : "+678",
            "Venezuela"                                    : "+58",
            "Vietnam"                                      : "+84",
            "Wake Island"                                  : "+1 808",
            "Wallis and Futuna"                            : "+681",
            "Yemen"                                        : "+967",
            "Zambia"                                       : "+260",
            "Zanzibar"                                     : "+255",
            "Zimbabwe"                                     : "+263"
            
        ]
        
        guard let countryCode = self.object(forKey: .countryCode), let displayName = self.displayName(forKey: .countryCode, value: countryCode), let trunkDialingCode = dialingCodes[displayName] else {
            
            return ""
            
        }
        
        return trunkDialingCode.replacingOccurrences(of: "+", with: "")
        
    }
    
}


//MARK:-
extension String{
    func convertDateStringFrom(currentFormat: String, toRequiredFormat requiredFormat: String) -> String{
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = currentFormat
        let curDate = dateFormater.date(from: self)
        dateFormater.dateFormat = requiredFormat
        if let currentDate = curDate{
            return dateFormater.string(from: currentDate)
        }
        return self
    }
    func getTimeDifferenceStringTillNowWith(currentFormat: String) -> String {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = currentFormat
        let curDate = dateFormater.date(from: self)
        if let currentDate = curDate{
            return currentDate.maxTimeDifferenceTillNow()
        }
        return ""
    }
    var trim: String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
}

//MARK:-
extension Date{
    func maxTimeDifferenceTillNow() -> String {
        let calender:Calendar = Calendar.current
        let components: DateComponents = calender.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self, to: Date())
//        print(components)
        var returnString:String = ""
        if components.year! >= 1 {
            returnString = String(describing: components.year!)+" year"
        }else if components.month! >= 1{
            returnString = String(describing: components.month!)+" month"
        }else if components.day! >= 1{
            returnString = String(describing: components.day!) + " days"
        }else if components.hour! >= 1{
            returnString = String(describing: components.hour!) + " hour"
        }else if components.minute! >= 1{
            returnString = String(describing: components.minute!) + " min"
        }else if components.second! < 60 {
            returnString = "Just Now"
        }
        
        return returnString
    }
    func currentTimeZoneDate() -> String {
        let dtf = DateFormatter()
        dtf.timeZone = TimeZone.current
        dtf.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dtf.string(from: self)
    }
    func getTimeDifferenceSince(date: Double) -> String {
        let date1:Date = Date() // Same you did before with timeNow variable
        let date2: Date = Date(timeIntervalSince1970: date)
        
        let calender:Calendar = Calendar.current
        let components: DateComponents = calender.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date1, to: date2)
//        print(components)
        var returnString:String = ""
//        print(components.second ?? "0.0")
        if components.second! < 60 {
            returnString = "Just Now"
        }else if components.minute! >= 1{
            returnString = String(describing: components.minute) + " min ago"
        }else if components.hour! >= 1{
            returnString = String(describing: components.hour) + " hour ago"
        }else if components.day! >= 1{
            returnString = String(describing: components.day) + " days ago"
        }else if components.month! >= 1{
            returnString = String(describing: components.month)+" month ago"
        }else if components.year! >= 1 {
            returnString = String(describing: components.year)+" year ago"
        }
        return returnString
    }
    
    func timeDifferenceTillNow() -> Int {
        let calender:Calendar = Calendar.current
        let calenderComponet = calender.dateComponents([.second], from: self, to: Date())
        guard let seconds = calenderComponet.second else {
//            print("Unable to get seconds. // In Date Extension")
            return 0
        }
        return seconds
    }
    
    private(set) var stringValueInUTC: String{
        get{
            var dateString = String(describing: self)
            dateString = dateString.replacingOccurrences(of: "+0000", with: "UTC")
//            print(dateString)
            return dateString
        }
        set{
            
        }
    }
    func stringValue()  -> String{
        var dateString = String(describing: self)
        dateString = dateString.replacingOccurrences(of: "+0000", with: "UTC")
        return dateString
    }
    func stringValue(format: String) -> String{
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = format
        let dateString = dateFormater.string(from: self)
        return dateString
    }
}


extension Int{
    init(timeDifferenceInSecondsSince previousTime: Date){
        let currentTime:Date = Date()
        let calender:Calendar = Calendar.current
        let calenderComponet = calender.dateComponents([.second], from: previousTime, to: currentTime)
        guard let seconds = calenderComponet.second else {
//            print("Unable to get seconds.")
            self = 0
            return
        }
        self = seconds
        
    }
    
}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        return ceil(boundingBox.height)
    }
}

// Attrubyted text trimming
public extension NSAttributedString {
    public func attributedStringByTrimmingNewlines() -> NSAttributedString {
        var attributedString = self
        while attributedString.string.first == "\n" {
            attributedString = attributedString.attributedSubstring(from: NSMakeRange(1, attributedString.string.count - 1))
        }
        while attributedString.string.last == "\n" {
            attributedString = attributedString.attributedSubstring(from: NSMakeRange(0, attributedString.string.count - 1))
        }
        return attributedString
    }
}

//
//  GRFloatingTextView.swift
//  JustStuck
//
//  Created by Gyan Routray on 30/01/17.
//  Copyright © 2017 Headerlabs. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class GRFloatingTextView: UITextView {
    let leftEdgeInset: CGFloat = 10.0
    let rightEdgeInset: CGFloat = 10.0
    var palceholderHeight: CGFloat = 20.0
    let placeholderLabel = UILabel()
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        self.setup()
    }
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
//        print(textContainerInset)
    }
    
    func setup() {
        NotificationCenter.default.addObserver(self, selector: #selector(textFieldDidEndEditing(notification:)), name: .UITextViewTextDidEndEditing, object: self)
        
        NotificationCenter.default.addObserver(self, selector: #selector(textFieldDidChange(notification:)), name: .UITextViewTextDidChange, object: self)
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: .UITextViewTextDidEndEditing, object: self)
        NotificationCenter.default.removeObserver(self, name: .UITextViewTextDidChange, object: self)
    }

    @objc func textFieldDidEndEditing(notification: NSNotification) {
        if (text?.count)!<1 {
            self.placeholderLabel.isHidden = false
        }
        else{
           self.placeholderLabel.isHidden = true
        }
    }
    
    @objc func textFieldDidChange(notification: NSNotification) {
        if (text?.count)!<1 {
            self.placeholderLabel.isHidden = false
            isScrollEnabled = true
        }
        else{
            self.placeholderLabel.isHidden = true
        }
    }

    public override func draw(_ rect: CGRect) {
        contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        let placeholderFrame = CGRect(x: 4, y: 8, width: frame.size.width, height: (font?.pointSize)! + CGFloat(4))
        placeholderLabel.frame = placeholderFrame
        insertSubview(placeholderLabel, belowSubview: textInputView)
       // placeholderLabel.textColor = UIColor(red: 0.78, green: 0.78, blue: 0.80, alpha: 1.0)
        placeholderLabel.textColor = UIColor.darkGray
        placeholderLabel.font = font
        
        guard let cCount = text?.count, cCount > 0 else  {
            placeholderLabel.text = placeholder
            placeholderLabel.isHidden = false
            return
        }
        placeholderLabel.isHidden = true

    }
    
    @IBInspectable var placeholder: String = ""{
        didSet{
            placeholderLabel.text = placeholder
        }
    }
}

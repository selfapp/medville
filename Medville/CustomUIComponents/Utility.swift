//
//  Utility.swift
//  Medville
//
//  Created by Jitendra Solanki on 9/25/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import Foundation

struct VCIdentifier {
    //MARK:- Patient
    static let kPatientLogIn = "PatientLogInViewController"
    static let kPasscodeVerify = "PasscodeVerifyViewController"
    static let kAddCard = "AddCardViewController"
    static let kPatientTabBar = "PatientTabBarController"
    static let kDoctorListVC =  "DoctorListViewController"
    static let kDoctorProfileDetails = "DoctorProfileDetailViewController"
    static let kBookAppointment = "BookAppointmentVC"
    static let kAttachmentDoc = "AttachedDocViewController"

    //MARK:-Doctor
    static let kDoctorLogIn = "DoctorLogInViewController"
    static let kDoctorTabBar = "DoctorTabBarController"
    static let kDoctorProfile = "DoctorProfileViewController"
    static let kAboutWebView  = "WebViewController"
    
    static let kPatientDetails = "PatientDetailsViewController"
    static let kChoosePassword = "ChoosePasswordViewController"
    static let kChatViewController = "ChatViewController"
    static let kAudioViewController = "AudioViewController"

    static let kVideoVC = "VideoViewController"
}

enum UserType :String {
    case kPatient =  "kPatient"
    case kDocotor = "kDoctor"
}

enum DateFormat:String {
    case kCurrentFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    case kRequireFormat = "dd MMM yyyy h:mm a"
}

enum DoctorProfile {
    static let kUpcomingAppointment = "UpcomingAppointment"
    static let kCompletedAppointment = "CompletedAppointment"
    static let kDoctorDetail = "DeoctorDetails"
}

//
//  GRHTTPClient.swift
//  JustStuck
//
//  Created by Gyan Routray on 23/01/17.
//  Copyright © 2017 Headerlabs. All rights reserved.
//

import Foundation

func get(request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ object: NSMutableDictionary, _ statusCode: Int) -> ()) {
    dataTask(request: request, method: "GET", completion: completion)
}
func post(request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ object: NSMutableDictionary, _ statusCode: Int) -> ()) {
    dataTask(request: request, method: "POST", completion: completion)
}
func put(request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ object: NSMutableDictionary, _ statusCode: Int) -> ()) {
    dataTask(request: request, method: "PUT", completion: completion)
}
func patch(request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ object: NSMutableDictionary, _ statusCode: Int) -> ()) {
    dataTask(request: request, method: "PATCH", completion: completion)
}
func delete(request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ object: NSMutableDictionary, _ statusCode: Int) -> ()) {
    dataTask(request: request, method: "DELETE", completion: completion)
}

private func dataTask(request: NSMutableURLRequest, method: String, completion: @escaping (_ success: Bool, _ object: NSMutableDictionary, _ statusCode: Int) -> ()) {
    request.httpMethod = method
    let session = URLSession(configuration: URLSessionConfiguration.default)
    let task =  session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
    if error != nil{
//        print("error==== \(String(describing: error?.localizedDescription))")
        let dict: [String: Any] = ["error": error?.localizedDescription ?? "Something went wrong. Please try again later."]
        let errorDict: NSMutableDictionary = NSMutableDictionary(dictionary: dict)
        
        guard let nsError = error as NSError? else{
            completion(false, errorDict, 0)
            return
        }
        completion(false, errorDict, nsError.code)
    }else{
        var statusCode = 0
         if let httpResponse = response as? HTTPURLResponse{
            statusCode = httpResponse.statusCode
            }
      //  let httpResponse = response as! HTTPURLResponse
//        print("Status Code:  \(statusCode)")
         if let responsData = data {
            //let json = try? JSONSerialization.jsonObject(with: data, options: [])
            let json : JSON = JSON(data: responsData)
            if let dataDict = json.dictionaryObject{
                let dictMutable: NSMutableDictionary = NSMutableDictionary(dictionary: dataDict)
                    if  200...299 ~= statusCode {
//                        print("After Status Code:  \(statusCode)")
                      completion(true, dictMutable, statusCode)
                    }
                    else{
//                        print("After Status Code:  \(statusCode)")
                       completion(false, dictMutable, statusCode)
                    }
            }
            else{
//                print("Response Data: \(json)")
//                print("JSON Data is not in dictionary format.")
                completion(false, ["error": "JSON Data is not in dictionary format."], statusCode)
            }
        }
         else{
//            print("Data: \(String(describing: data))")
        }
    }
}
    task.resume()
}

func urlRequestWith(path: String, params: [String:Any]? = nil, authorize: Bool) -> NSMutableURLRequest {
    var finalpath = ""
    if let encoded = path.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed){
            finalpath = encoded
    }

    let request = NSMutableURLRequest(url: NSURL(string: kBaseURL+finalpath)! as URL)
    request.addValue("application/json",forHTTPHeaderField: "Content-Type")
    request.addValue("application/json",forHTTPHeaderField: "Accept")
    
    if authorize {
        
        guard let authToken = AppUser.authToken else {
//            print("Needs authorization, but no saved auth_token found in userdefault")
            return request
        }
//        guard let userDict =  UserDefaults.standard.object(forKey: kUserDetailsKey) as? NSDictionary else{
//            print("Needs authorization, but no saved user_details found")
//            return request
//        }
//        guard let authToken = userDict.object(forKey: kAuthTokenKey) as? String else{
//            print("Needs authorization, but no saved auth_token found in userdefault")
//            return request
//        }
        request.addValue("Token token=\"\(authToken)\"", forHTTPHeaderField: "Authorization")
    }
    
    guard let dict = params else {
//        print(" -------Sendind Request with------- \nBody: \(params)\nURL: \(request.url) \nAuth_token: \(String(describing: request.value(forHTTPHeaderField: "Authorization")))")
//        print("Headers: \(request.allHTTPHeaderFields!)  ")
        return request
    }
//    print("  -------Sendind Request with------- \nBody: \(dict)\nURL: \(request.url) \nAuth_token: \(String(describing: request.value(forHTTPHeaderField: "Authorization")))")
//    print("Headers: \(request.allHTTPHeaderFields!)  ")
    do {
        let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
        request.httpBody =  jsonData
    }
    catch{
        
    }
    return request
}
func makeParamsDict(dict: [String : Any]) -> [String: Any]{
    let newDict = dict
   // newDict["user"] = dict
    return newDict
}

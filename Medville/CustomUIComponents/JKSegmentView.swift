//
//  JKSegmentView.swift
//  JKDropDownList
//
//  Created by Jitendra Solanki on 7/26/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

protocol JKSegmentViewDelegate:class {
    func didSelectSegmentAt(index segmentIndex:Int,view segmentView:JKSegmentView)
}
class JKSegmentView: UIView {
    
    let BASETAG_VALUE = 100
    let BOTTOM_BORDER_VIEW_TAG = 300
    let INDICATOR_VIEW_TAG = 305
    var currentSelectedSegmentTag = 0
    var previousSelectedSegmentTag = 0
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     */
    
    @IBInspectable var titleColor:UIColor?
    @IBInspectable var highlightedTitleColor:UIColor?
    @IBInspectable var segmentBackground:UIColor?
    @IBInspectable var segmentSelected:UIColor?
    @IBInspectable var borderColor:UIColor?
    @IBInspectable var indicatorColor:UIColor?
    
    @IBInspectable var selectedIndex:Int = 0
    
    var arrSegments:[String] = []
    weak var delegate:JKSegmentViewDelegate?
    
    override func draw(_ rect: CGRect) {
        // Drawing code
        setupViewWithSegments(segments: self.arrSegments)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
       // setupViewWithSegments(segments: self.arrSegments)
    }
    //MARK:- Default setup
    
    func getButtonWithTag(value tag:Int)->UIButton{
        if let btn = self.viewWithTag(tag)as? UIButton{
            return btn
        }else{
            let btn = UIButton()
            btn.tag = tag
            //btn.setTitle(segments[i], for: .normal)
            btn.setTitleColor(titleColor, for: .normal)
            btn.backgroundColor = segmentBackground
            btn.addTarget(self, action: #selector(segmentSelected(sender:)), for: .touchUpInside)
            self.addSubview(btn)
            return btn
        }
    }
    
    func setupViewWithSegments(segments:[String]){
        //width for each segment
        self.arrSegments = segments
        let width = self.frame.size.width / CGFloat(segments.count)
        
        for i in 0...(segments.count-1){
            
            //get btn for tag
            let btn = getButtonWithTag(value: BASETAG_VALUE+i)
            btn.frame = CGRect(x: width * CGFloat(i), y: 0, width: width, height: self.frame.size.height)
            btn.setTitle("", for: .normal)
            btn.titleEdgeInsets  = UIEdgeInsetsMake(0, 0, -20, 0)
            //btn.contentHorizontalAlignment = .center
            btn.setTitle(segments[i], for: .normal)
            
        }
        
        setBottomBorderView()
        //if currentSelectedSegmentTag != previousSelectedSegmentTag {
            setSegmentSelectedFor(tag: BASETAG_VALUE+selectedIndex)
            
        //}
    }
    
    private func setBottomBorderView(){
        
        var borderView:UIView!
        
        if let borderV = self.viewWithTag(BOTTOM_BORDER_VIEW_TAG){
            borderView = borderV
        }else{
            borderView = UIView()
            borderView.tag = BOTTOM_BORDER_VIEW_TAG
        }
        
        
        var frame = self.bounds
        frame.origin.y += frame.size.height-1
        frame.size.height = 1
        borderView.frame = frame
        //        borderView.frame = CGRect(x: 0, y: self.frame.origin.y + self.frame.size.height-1, width:self.frame.origin.x + self.frame.size.width, height: 1)
        borderView.backgroundColor = borderColor
        
        self.addSubview(borderView)
        
    }
    
    
    func indicatorViewWith(frame:CGRect)->UIView{
        if let indicator = self.viewWithTag(INDICATOR_VIEW_TAG){
            return indicator
        }else{
            let indicatorView = UIView()
            indicatorView.tag = INDICATOR_VIEW_TAG
            indicatorView.backgroundColor = indicatorColor
            self.addSubview(indicatorView)
            return indicatorView
        }
    }
    //MARK:- Select a segment
    
    func updateIndicatorViewFor(segment:UIButton){
        //get indicator
        let indicatorV = indicatorViewWith(frame: segment.frame)
        
        
        let centerX = segment.frame.origin.x + segment.frame.size.width/2
        self.isUserInteractionEnabled = false
       
        UIView.animate(withDuration: 0.3, animations: {
            
            indicatorV.frame = CGRect(x:segment.frame.origin.x, y:segment.frame.origin.y + segment.frame.size.height-1, width: segment.frame.size.width, height:1)

            indicatorV.center = CGPoint(x: centerX, y:segment.frame.origin.y+segment.frame.size.height)
       
        }) { (isCompleted) in
            self.isUserInteractionEnabled = true
        }
        
    }
    
    func setSegmentSelectedFor(tag tagvalue:Int){
        
        
        if let segView = self.viewWithTag(tagvalue)as? UIButton{
            
            if currentSelectedSegmentTag != tagvalue{
                previousSelectedSegmentTag = currentSelectedSegmentTag
                currentSelectedSegmentTag = tagvalue
                
            }
            
            segView.backgroundColor = segmentSelected
            segView.setTitleColor(highlightedTitleColor, for: .normal)
            updateIndicatorViewFor(segment: segView)
            
            //get the prevoius selected segment and set default appearance
            if let lastSelectedSegView = self.viewWithTag(previousSelectedSegmentTag)as? UIButton{
                lastSelectedSegView.backgroundColor = segmentBackground
                lastSelectedSegView.setTitleColor(titleColor, for: .normal)
            }
            
        }
        
    }
    
    @objc func segmentSelected(sender: UIButton){
        if currentSelectedSegmentTag != sender.tag{
            self.selectedIndex = BASETAG_VALUE-sender.tag
            delegate?.didSelectSegmentAt(index: self.selectedIndex, view: self)
            setSegmentSelectedFor(tag: sender.tag)

        }
    }
}

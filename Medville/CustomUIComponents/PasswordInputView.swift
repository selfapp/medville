//
//  PasswordInputView.swift
//  Medville
//
//  Created by Jitendra Solanki on 9/25/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

protocol PasswordInputViewDelegate:class {
    func PassWordInputDidFinishWith(code:String)
}

@IBDesignable

class PasswordInputView: UIView,UIKeyInput{

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    weak var delegate:PasswordInputViewDelegate?

    private var nextTag = 1
    
    // MARK: - UIResponder
    
    open override var canBecomeFirstResponder : Bool {
        return true
    }
    
    // MARK: - UIView
    
    override func draw(_ rect: CGRect) {
         setupView()
    }
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
       
    }
    
    override func awakeFromNib() {
         super.awakeFromNib()
        setupView()
    }
    
    override func layoutSubviews() {
         setupView()
    }
    func setupView(){
        
        //self.translatesAutoresizingMaskIntoConstraints = false
        
        let size = self.frame.size
        let width = (size.width-40)/4
        var slotFrame = CGRect(x: 8, y: 0, width:width, height: 40)
        //var dashFrame = CGRect(x: 10, y: 10, width:width, height: 40)

        // Add four digitLabels

       // var frame = CGRect(x: 15, y: 10, width: 35, height: 40)
        for index in 1...4 {
            
            var digitLabel:UILabel!
            if  let oldLabel = viewWithTag(index) as? UILabel{
                digitLabel = oldLabel
                digitLabel.frame = slotFrame
            }else{
            
            digitLabel = UILabel(frame: slotFrame)
            digitLabel.font = .systemFont(ofSize: 42)
            digitLabel.tag = index
            //digitLabel.text = "–"
            digitLabel.textColor = .white
            digitLabel.textAlignment = .center
            //digitLabel.backgroundColor = .yellow
                addSubview(digitLabel)
            }
            
            //add a view to show a dash at bottom of the digit
            var dashFrame = digitLabel.frame
            dashFrame.origin.y = dashFrame.origin.y+dashFrame.size.height
            dashFrame.size.height = 2

            var dashView:UIView!
            if let oldDashView = viewWithTag(300+index){
                dashView = oldDashView
                dashView.frame = dashFrame
            }else{
             dashView = UIView(frame: dashFrame)
            dashView.backgroundColor =  .white
            dashView.tag =  300 + index
            addSubview(dashView)
            }
            //frame.origin.x += 35 + 15
            slotFrame.origin.x += width + 8

        }
        //self.backgroundColor = .red
    }
    required public init?(coder aDecoder: NSCoder) {
        
        //fatalError("init(coder:) has not been implemented")
            super.init(coder: aDecoder)
    } // NSCoding
    
    // MARK: - UIKeyInput
    
    public var hasText : Bool {
        return nextTag > 1 ? true : false
    }
    
    public var hasEmptySlot :Bool{
        return nextTag != 5
    }
    
    public var code:String{
        get{ if nextTag == 5 {
            var code = ""
            for index in 1..<nextTag {
                code += (viewWithTag(index)! as! UILabel).text!
            }
            return code
        }
        return ""
        }
        
    }
    
    open func insertText(_ text: String) {
        if nextTag < 5 {
            (viewWithTag(nextTag)! as! UILabel).text = text
            nextTag += 1
            
            if nextTag == 5 {
                var code = ""
                for index in 1..<nextTag {
                    code += (viewWithTag(index)! as! UILabel).text!
                }
                
                delegate?.PassWordInputDidFinishWith(code: code)
             }
        }
    }
    
    open func deleteBackward() {
        if nextTag > 1 {
            nextTag -= 1
            //(viewWithTag(nextTag)! as! UILabel).text = "–"
            (viewWithTag(nextTag)! as! UILabel).text = ""

        }
    }
    
    open func clear() {
        while nextTag > 1 {
            deleteBackward()
        }
    }
    
    // MARK: - UITextInputTraits
    
    open var keyboardType: UIKeyboardType { get { return .numberPad } set { } }
}

//
//  RoundedShadowView.swift
//  Medville
//
//  Created by Jitendra Solanki on 10/4/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit
///
import Foundation
class ShadowView: UIView {
    var shadowView = UIView()
    var manualHeight: CGFloat = 0.0
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    public func resizeViewWithHeight(_ heigh: CGFloat){
        self.manualHeight = heigh
        self.shadowView.removeFromSuperview()
        setNeedsDisplay()
    }
    public override func draw(_ rect: CGRect) {
        isUserInteractionEnabled = true
        self.shadowView.frame = frame
        layer.cornerRadius = 3
        layer.masksToBounds = true
        self.shadowView.layer.shadowOffset = CGSize(width:2, height:2)
        self.shadowView.layer.shadowColor = UIColor(r:238,g:238,b:238,alpha:1.0).cgColor
        self.shadowView.layer.shadowOpacity = 0.5
        self.shadowView.layer.shadowRadius = 5
        
        self.shadowView.layer.shadowPath = UIBezierPath(roundedRect: bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 5, height: 5)).cgPath
        self.shadowView.layer.shouldRasterize = true
        self.shadowView.layer.rasterizationScale = UIScreen.main.scale
        self.shadowView.layer.masksToBounds = false
        superview?.insertSubview(self.shadowView, belowSubview: self)
    }
    func setup(){
        self.shadowView.backgroundColor = UIColor.clear
    }
}

/////
class RoundedShadowView: UIView {
 
    
    let innerView  = UIView()
    
    
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        
        addShadowWith(opacity: 1.0, radius: 5.0, offset: CGSize(width:2, height:2), color: UIColor.black)

        self.innerView.frame = frame
        innerView.layer.cornerRadius = 5.0
        innerView.addShadowWith(opacity: 0.5, radius: 5.0, offset: CGSize(width:0, height:0), color: UIColor.black)

        //innerView.layer.masksToBounds = true
        //innerView.clipsToBounds = true
        //innerView.backgroundColor = .clear

        superview?.insertSubview(innerView, belowSubview: self)
        addSubview(innerView)

        //innerView.makeCornerRound(cornerRadius: 5.0)
 
        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = true
        
       //self.insertSubview(innerView, at: 0)

        //self.makeCornerRound(cornerRadius: 5.0)

    }
 
    override func layoutSubviews() {
//        self.innerView.frame = self.bounds
//        innerView.layer.cornerRadius = 5.0
//        innerView.layer.masksToBounds = true
//        //innerView.makeCornerRound(cornerRadius: 5.0)
//
//        addShadowWith(opacity: 1.0, radius: 5.0, offset: CGSize(width:2, height:2), color: UIColor.black)
//        addSubview(innerView)

    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        innerView.frame = self.frame
    }
    required init?(coder aDecoder: NSCoder) {
         super.init(coder: aDecoder)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    

}

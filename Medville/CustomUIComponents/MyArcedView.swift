//
//  MyArcedView.swift
//  Medville
//
//  Created by Jitendra Solanki on 9/27/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

class MyArcedView: UIView {

         override func draw(_ rect: CGRect) {
            // Drawing code
            // Drawing code
            self.backgroundColor = .blue
         
            let arcHeight: CGFloat = 10
            let arcColor = UIColor.blue
            arcColor.setFill() // <--
            let con = UIGraphicsGetCurrentContext() // <--
            con?.fill(rect) // <--
            
             let arcRectBottom = CGRect(x: 0, y: rect.height - arcHeight, width: rect.width, height: arcHeight)
            // ... same as your code ...
            let arcRadius = (arcHeight / 2) + (pow(arcRectBottom.width, 2) / (8 * arcHeight))
            let arcCenter = CGPoint(x: arcRectBottom.origin.x + (arcRectBottom.width / 2), y: arcRectBottom.origin.y + arcRadius)
            //let arcCenter = CGPoint(x: arcRectBottom.midX, y: arcRectBottom.midY - arcRadius)
            
            let angle = acos(arcRectBottom.width / (2 * arcRadius))
           
            let startAngle = radians(180) + CGFloat(angle)
            let endAngle = radians(360) - CGFloat(angle)
            
            let path = UIBezierPath(arcCenter: arcCenter, radius: arcRadius / 2, startAngle: startAngle, endAngle: endAngle, clockwise: true)
            
            
            path.lineWidth = arcRadius
            arcColor.setStroke()
            path.stroke(with: .clear, alpha: 1)
            
            

//            
            //top arc
            let arcRectTop = CGRect(x: 0, y: rect.origin.y+arcHeight, width: rect.width, height: arcHeight)
            let arcCenterTop = CGPoint(x: arcRectTop.origin.x + (arcRectTop.width / 2), y:arcRectTop.origin.y + arcRadius)
            
//
//            
           let pathTop = UIBezierPath(arcCenter: arcCenterTop, radius: arcRadius / 2, startAngle: startAngle, endAngle: endAngle, clockwise: true)
            pathTop.lineWidth  = arcRadius
            
            //pathTop.lineWidth = arcRadius
         // pathTop.stroke(with: .clear, alpha: 1)
        
        }
    
        private func radians(_ degrees: CGFloat) -> CGFloat {
            return degrees * CGFloat(M_PI) / 180
        }
    
    private func angleBetween(point1:CGPoint,point2:CGPoint)->Double{
//        let a = x2 - x1
//        let b = y2 - y1
        let a = point2.x - point1.x
        let b = point2.y - point1.y

        let angleStep = M_PI_2 / Double(2 + 1)
        var angle = angleStep
        var points: [CGPoint] = []
        while angle < M_PI_2 {
            let x = a * CGFloat(cos(angle))
            let y = point2.y - b * CGFloat(sin(angle))
            points.append(CGPoint(x:x, y:y))
            angle += angleStep
            
        }
        return angle
    }
    
    }
 

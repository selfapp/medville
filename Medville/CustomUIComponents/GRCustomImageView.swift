//
//  GRCustomImageView.swift
//  JustStuck
//
//  Created by Gyan Routray on 09/01/17.
//  Copyright © 2017 Headerlabs. All rights reserved.
//

import UIKit

 @IBDesignable public class GRCustomImageView: UIControl {
    public var updated: Bool = false
    private var imageView = UIImageView()
    override init(frame: CGRect) {
        super.init(frame: frame)
         self.setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    func setup() {
     }
     public override func draw(_ rect: CGRect) {
        if !needContentMode {
            if (image != nil) {
                image?.draw(in: rect)
            }
        }
        
        if roundView{
            layer.cornerRadius = bounds.size.width/2
        }
        
        layer.masksToBounds = true
    }

    // --------- Image ----------
    @IBInspectable  public var image: UIImage?{
        didSet{
            guard let newImage = image else {
                imageView.image = oldValue
                return
            }
            guard let oldImage = oldValue else {// In case of no old image
                imageView.image = image
                self.updated = true
                return
            }
            guard let newimageData = UIImagePNGRepresentation(newImage) else {
                imageView.image = oldValue
                return
            }
            guard let oldImageData = UIImagePNGRepresentation(oldImage) else {
                imageView.image = image
                self.updated = true
                return
            }
            imageView.image = image
            self.updated = newimageData == oldImageData ? false : true
//            print(String(describing: self.updated))
        }
    }
    
    @IBInspectable public var borderColor: UIColor = UIColor.clear {
        didSet{
            layer.borderColor = borderColor.cgColor
         }
    }
    
    @IBInspectable public var borderWidth: CGFloat = 0.0 {
        didSet{
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable public var cornerRadious: CGFloat = 0.0 {
        
        didSet{
            layer.cornerRadius = cornerRadious
        }
    }
    
    @IBInspectable public var roundView: Bool = true {
        didSet{
            if roundView{
                layer.cornerRadius = bounds.size.width/2
            }
            else{
                layer.cornerRadius = cornerRadious
            }
        }
    }
    
    @IBInspectable public var needContentMode: Bool = false {
        didSet{
            if needContentMode{
                insertSubview(imageView, at: 0)
                imageView.contentMode = contentMode
                imageView.backgroundColor = backgroundColor
            }
            else{
                imageView.removeFromSuperview()
            }
        }
    }

    override public func setNeedsLayout() {
        super.setNeedsLayout()
        self.setNeedsDisplay()
    }
    override public func layoutSubviews() {
        super.layoutSubviews()
        if needContentMode{
            imageView.frame = bounds
        }

    }
}


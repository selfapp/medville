//
//  TwoWayRounded.swift
//  Medville
//
//  Created by Sunder Singh on 10/6/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import UIKit

class TwoWayRounded: UIView {

    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
     
     let path = UIBezierPath(roundedRect: self.bounds,
     byRoundingCorners: [.topLeft, .bottomRight],
     cornerRadii: CGSize(width: 15.0, height: 0.0))
     UIColor(r: 88.0, g: 211.0, b: 243.0, alpha: 1.0).setFill()
     path.fill()
    }
    
}

//
//  GRCustomView.swift
//  JustStuck
//
//  Created by Gyan Routray on 19/01/17.
//  Copyright © 2017 Headerlabs. All rights reserved.
//

import UIKit

@IBDesignable public class GRCustomView: UIView {
    var HSeparator = UILabel(frame: .zero)
    var VSeparator = UILabel(frame: .zero)
    var leftIcon = UIImageView(frame: .zero)
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    override public func layoutSubviews() {
        super.layoutSubviews()
    }
    public override func draw(_ rect: CGRect) {
        leftIconWidth = bounds.size.height * 0.65
        leftIconHeight = bounds.size.height * 0.65
        HSeparator.frame = CGRect(x: 0, y: bounds.size.height-1, width: bounds.size.width, height:1)
        HSeparator.backgroundColor = separatorColor
        
        VSeparator.frame = CGRect(x: (leftIconInset*2)+leftIconWidth, y: VseparatorSpacing, width: 1, height: bounds.size.height - (VseparatorSpacing * 2))
        VSeparator.backgroundColor = separatorColor
        
       leftIcon.frame = CGRect(x: leftIconInset, y: (bounds.size.height/2) - (leftIconHeight/2), width: leftIconWidth, height: leftIconHeight)
    }
    
    func setup() {
        addSubview(HSeparator)
        addSubview(VSeparator)
        addSubview(leftIcon)
     }
    @IBInspectable public var separatorColor: UIColor = UIColor.blue{
        didSet{
            HSeparator.backgroundColor = separatorColor
        }
    }
    @IBInspectable public var image: UIImage?{
        didSet{
            leftIcon.image = image
        }
    }
    @IBInspectable public var VseparatorSpacing: CGFloat = 5{
        didSet{
            VSeparator.frame = CGRect(x: (leftIconInset*2)+leftIconWidth, y: VseparatorSpacing, width: 1, height: bounds.size.height - (VseparatorSpacing * 2))
        }
    }
    public var leftIconWidth: CGFloat = 30{
        didSet{
           leftIcon.frame = CGRect(x: leftIconInset, y: center.y , width: leftIconWidth, height: leftIconHeight)
            
            VSeparator.frame = CGRect(x: (leftIconInset*2)+leftIconWidth, y: VseparatorSpacing, width: 1, height: bounds.size.height - (VseparatorSpacing * 2))
        }
    }
    public var leftIconHeight: CGFloat = 30{
        didSet{
            leftIcon.frame = CGRect(x: leftIconInset, y: center.y , width: leftIconWidth, height: leftIconHeight)
            
            VSeparator.frame = CGRect(x: (leftIconInset*2)+leftIconWidth, y: VseparatorSpacing, width: 1, height: bounds.size.height - (VseparatorSpacing * 2))
        }
    }
    @IBInspectable public var leftIconInset: CGFloat = 5{
        didSet{
            leftIcon.frame = CGRect(x: leftIconInset, y: center.y , width: leftIconWidth, height: leftIconHeight)
            
            VSeparator.frame = CGRect(x: (leftIconInset*2)+leftIconWidth, y: VseparatorSpacing, width: 1, height: bounds.size.height - (VseparatorSpacing * 2))
        }
    }
    @IBInspectable public var addleftIcon: Bool = true{
        didSet{
            if addleftIcon {
                addSubview(leftIcon)
                addSubview(VSeparator)
            }
            else{
                leftIcon.removeFromSuperview()
                VSeparator.removeFromSuperview()
            }
        }
    }

}

@IBDesignable public class GRUnderLinedView: UIView {
    var HSeparator = UILabel(frame: .zero)
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
    }
    public override func draw(_ rect: CGRect) {
        HSeparator.frame = CGRect(x: 0, y: bounds.size.height-1, width: bounds.size.width, height:1)
        HSeparator.backgroundColor = separatorColor
    }
    
    func setup() {
        addSubview(HSeparator)
    }
    @IBInspectable public var separatorColor: UIColor = UIColor.blue{
        didSet{
            HSeparator.backgroundColor = separatorColor
        }
    }
}


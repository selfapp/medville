//
//  DataSchema.swift
//  Medville
//
//  Created by Jitendra Solanki on 9/25/17.
//  Copyright © 2017 jitendra. All rights reserved.
//

import Foundation

enum ErrorMessages{
    static let kNetworkError = "Please make sure, you are connected to internet and try again."
    static let kDoctorNotFound  = "Session expired. Please log in again to continue."
    static let kPateintNotFound = "Session expired. Please log in again to continue."
}


struct AppUser{

    static var isLoggedIn:Bool{
        get{
            return UserDefaults.standard.bool(forKey: kLoginStatusKey)
        }set{
            UserDefaults.standard.set(newValue, forKey: kLoginStatusKey)
            UserDefaults.standard.synchronize()
        }
    }
    static var authToken:String? {
        get{
            return UserDefaults.standard.string(forKey: kAuthTokenKey)
        }set{
            UserDefaults.standard.set(newValue, forKey: kAuthTokenKey)
            UserDefaults.standard.synchronize()
        }
    }
    static var accessTokenForTwilio:String? {
        get{
            return UserDefaults.standard.string(forKey: kDeviceTokenTwilio)
        }set{
            UserDefaults.standard.set(newValue, forKey: kDeviceTokenTwilio)
            UserDefaults.standard.synchronize()
        }
    }
    
    
    static var refreshToken:String{
        get{
            return UserDefaults.standard.string(forKey: kRefreshTokenKey) ?? ""
        }set{
            UserDefaults.standard.set(newValue, forKey: kRefreshTokenKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    static var userType:UserType? {
        get{
            let type = UserDefaults.standard.string(forKey: kCurrentRoleKey) ?? ""
            return UserType(rawValue: type)
         }
        set{
            if newValue != nil {
                UserDefaults.standard.set(newValue!.rawValue, forKey: kCurrentRoleKey)
            }
            else {
                UserDefaults.standard.set("", forKey: kCurrentRoleKey)
            }
            UserDefaults.standard.synchronize()
        }
    }
    
    static var patient:Patient? {
        get{
            if let patientData = UserDefaults.standard.value(forKey: kPATIENT)as? Data{
                 return try? PropertyListDecoder().decode(Patient.self, from: patientData)
            }
            return nil
        }set{
            if let newPatient = newValue{
                UserDefaults.standard.set(try? PropertyListEncoder().encode(newPatient), forKey: kPATIENT)
                UserDefaults.standard.synchronize()
            }
         }
    }
    
    static var chatUser:[ChatUser]? {
        get{
            let userDefaults = UserDefaults.standard
            if let decoded  = userDefaults.object(forKey: kChatUser) as? Data{
            let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [ChatUser]
                return decodedTeams
            
            }
            return nil
        }set{
            if let newChatUser = newValue{
                let userDefaults = UserDefaults.standard
                let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: newChatUser)
                userDefaults.set(encodedData, forKey: kChatUser)
                userDefaults.synchronize()
            }
        }
        }
    
    
    static var doctor:Doctor? {
        get{
            if let doctorData = UserDefaults.standard.value(forKey: kDOCTOR)as? Data{
                let savedDoctor = try? PropertyListDecoder().decode(Doctor.self, from: doctorData)
                return savedDoctor
            }
             return nil
        }set{
            if let newDoctor = newValue{
            UserDefaults.standard.set(try? PropertyListEncoder().encode(newDoctor), forKey: kDOCTOR)
             UserDefaults.standard.synchronize()
                
            }
        }
    }
    
    
    static func setupUserFor(type:UserType, patient:Patient?, doctor:Doctor?){
        
        let standard = UserDefaults.standard
        if type == .kDocotor{
            //save doctor
            standard.setValue(doctor, forKey: "kDoctor")
        }else{
            //save patient
        }
        standard.synchronize()
    }
    
    /////////////////////////////////
    private func setUpAppuserDetails() {
        if let detailsDict : [String: Any] = UserDefaults.standard.object(forKey: kUserDetailsKey) as? [String : Any] {
//            print(detailsDict)
            AppUser.firstname = detailsDict["name"] as! String
            AppUser.title = detailsDict["title"]as! String
            AppUser.address = detailsDict["address"]as! String
            //AppUser.lastName = detailsDict["last_name"] as! String
            //AppUser.email = detailsDict["email"] as! String
            AppUser.phoneNumber = detailsDict["phone_number"] as! String
            AppUser.imageUrlString = detailsDict["image_url"] as! String
            //AppUser.stripeToken = detailsDict["stripe_customer_id"] as! String
        }
    }
    
    func setUpDetails(detailsDict: [String: Any]) {
    }
    
    func updateUserDetails(detailsDict: [String: Any]) {
//        print(detailsDict)
        var userDict = [String: Any]()
        if let fName = detailsDict["name"]  {
            userDict["name"] = fName
        }
        else{
            userDict["name"] = ""
        }
        
        if let title = detailsDict["title"]as? String{
            userDict["title"]  = title
        }else{
            userDict["title"]  = ""
        }
        //        if let lName = detailsDict["last_name"]  {
        //            userDict["last_name"] = lName
        //        }
        //        else{
        //            userDict["last_name"] = ""
        //        }
        //        if let email = detailsDict["email"]  {
        //            userDict["email"] = email
        //        }
        //        else{
        //            userDict["email"] = ""
        //        }
        if let auth_token = detailsDict["auth_token"]  {
            userDict["auth_token"] = auth_token
        }
        else{
            userDict["auth_token"] = ""
        }
        if let auth_token = detailsDict["refresh_token"]  {
            userDict["refresh_token"] = auth_token
        }
        else{
            userDict["refresh_token"] = ""
        }
        
        if let auth_token = detailsDict["phone_number"]  {
            userDict["phone_number"] = auth_token
        }
        else{
            userDict["phone_number"] = ""
        }
        
        if let address = detailsDict["address"]as? String{
            userDict["address"] = address
        }else{
            userDict["address"] = ""
        }
        //        if let stripe_token = detailsDict["stripe_customer_id"]
        //            as? String {
        //            userDict["stripe_customer_id"] = stripe_token
        //        }
        //        else{
        //            userDict["stripe_customer_id"] = ""
        //        }
        if let userId = detailsDict["id"]  {
            userDict["id"] = userId
        }
        else{
            userDict["id"] = 0
        }
        
        //let imageDict = detailsDict["image"] as? [String : Any]
        if let url = detailsDict["avatar_path_relative"] as? String{
            let urlWIthBase = kBaseURL + url
            userDict["image_url"] = urlWIthBase
        }
        else{
            if let url = detailsDict["image_url"] as? String{
                userDict["image_url"] = url
            }
            else{
                userDict["image_url"] = ""
            }
        }
//        print(userDict)
        UserDefaults.standard.set(userDict, forKey: kUserDetailsKey)
        UserDefaults.standard.synchronize()
        setUpAppuserDetails()
    }
    
    //save the type of app user in defaults
    static var type: String{
        get{
            return UserDefaults.standard.string(forKey: kCurrentRoleKey) ?? ""
        }
        set{
            if newValue == kUSER || newValue == kDOCTOR{
                UserDefaults.standard.set(newValue, forKey: kCurrentRoleKey)
            }
            else {
                UserDefaults.standard.set("", forKey: kCurrentRoleKey)
            }
            UserDefaults.standard.synchronize()
        }
    }
    
    static var firstname = ""
    static var lastName = ""
    static var email = ""
    static var phoneNumber = ""
    static var imageUrlString = ""
    static var stripeToken = ""
    static var title = ""
    static var address = ""
    static var name: String{
        get{
            var tempName = ""
            if self.firstname.characters.count > 0 && self.lastName.characters.count > 0{
                tempName = self.firstname + " " + self.lastName
            }
            else if self.firstname.characters.count > 0 {
                tempName = self.firstname
            }
            else if self.lastName.characters.count > 0 {
                tempName = self.lastName
            }
            else{
                tempName = "Not Available"
            }
            return tempName
        }
        set{
            self.name = newValue
        }
    }
    
    static var isHyphenateLogin:Bool{
        get{
            return UserDefaults.standard.bool(forKey: kHyphenate)
        }set{
            UserDefaults.standard.set(newValue, forKey: kHyphenate)
            UserDefaults.standard.synchronize()
        }

    }
}


//MARK:- default param
final class DefaultParams{
    
    var deviceToken = ""
    static var deviceToken: String{
        get{
            let appdelagate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
            return (appdelagate.UUID.count > 1) ? appdelagate.UUID : "asdasdasdasdasdadasda66bnnbnbnnbnbnjbhhhasdasd"
        }
    }
    static var deviceModel:String{
        get{
            return UIDevice().localizedModel
        }
    }
    static var defaultDict: [String: Any]{
        get{
            let dict = ["uuid": self.uuid, "token": self.deviceToken, "app_version": self.appVersion]
            return dict
        }
    }
    
    static var appVersion: String{
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            return version
        }
        else{
            return "\(0.0)"
        }
    }
    
    static var OSVersion: String{
        return UIDevice().systemVersion
    }
    static var OSType: String{
        return UIDevice().systemName
    }
    
    static private(set) var uuid: String{
        get{
            if let uuidStr = UIDevice().identifierForVendor {
                var uuidFinal = "\(uuidStr)"
                uuidFinal = uuidFinal.replacingOccurrences(of: "-", with: "")
                return uuidFinal
            }else{
                return ""
            }
        }set{}
        
    }
}

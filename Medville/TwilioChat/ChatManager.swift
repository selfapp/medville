//
//  ChatManager.swift
//  Medville
//
//  Created by Durgesh on 14/02/18.
//  Copyright © 2018 jitendra. All rights reserved.
//

import UIKit
import TwilioChatClient
import TwilioAccessManager


class ChatManager: NSObject {
    
    let tokenURL = "/calls/chat_token"
    
    static let _sharedManager = ChatManager()
    var isConnected:Bool = false
// MARK: Chat variables
    var client: TwilioChatClient? = nil
    var accessManager: TwilioAccessManager? = nil
    var delegate:ChatViewController? = nil
    var lastToken:Data?

    class func sharedManager() -> ChatManager {
        return _sharedManager
    }

    func loginWithidentity(identity:String,completion: @escaping (Bool) -> Void){
        
        
        self.retrieveToken(url: tokenURL) { (token, error) in
            if let token = token {
                print("Token = \(token)")
                AppUser.accessTokenForTwilio = token
                // Set up Twilio Chat client
                 self.accessManager = TwilioAccessManager(token:token, delegate:self)
                TwilioChatClient.chatClient(withToken: token, properties: nil, delegate: self) {
                    (result, chatClient) in
                    if result.isSuccessful(){
                    self.client = chatClient;
                        self.isConnected = true
                        if self.lastToken != nil{
                            self.updateChatClient(token: self.lastToken!)
                        }
                        completion(true)
                    print("Twilio Chat client connected")
                    }else{
                        completion(false)
                    }
                }
            } else {
                completion(false)
                print("Error retrieving token: \(error.debugDescription)")
            }
        }
    }
    
   // Check Channel is joined or Not
    func isChannelExist(channelName:String, completion: @escaping (TCHChannel?) -> Void){
        let channelList = self.client?.channelsList()
        channelList?.channel(withSidOrUniqueName: channelName, completion: { (result, existingChannel) in
            if result.isSuccessful(){
                completion(existingChannel!)
            }
            else{
                completion(nil)
            }
        })
        
    }
      
    
    // Create New channel
    func createChannel(chanelName:String, friendlyName:String, completion: @escaping (TCHChannel) -> Void){
    
        let channelList = self.client?.channelsList()
        let options:[NSObject : AnyObject] = [
            TCHChannelOptionFriendlyName as NSObject: friendlyName as AnyObject,
            TCHChannelOptionUniqueName as NSObject : chanelName as AnyObject,
            TCHChannelOptionType as NSObject: TCHChannelType.public.rawValue as AnyObject
        ]
        channelList?.createChannel(options: options as! [String : Any], completion: { (result, newChannel) in
            if result.isSuccessful(){
                newChannel?.join(completion: { (joinResult) in
                    completion(newChannel!)
                })
            }
            if result.resultCode == 50307{
                channelList?.channel(withSidOrUniqueName: chanelName, completion: { (result, existingChannel) in
                    if (existingChannel != nil){
                        existingChannel?.join(completion: { (result) in
                            print("channel join")
                        })
                    }
                })
            }
        })
    }
    
    func addUserinChannel(userName:String,channel:TCHChannel){
        channel.members?.add(byIdentity: userName, completion: { (result) in
            if result.isSuccessful(){
                print("member added")
            }
        })
    }
    
    
// logout Twilio
    func logout(){
        self.client?.deregister(withNotificationToken: self.lastToken!, completion: { (result) in
            
        })
        self.isConnected = false
        self.client?.shutdown()
        self.client = nil
    }
    
    
    func updateToken(token:Data){
        self.lastToken = token
       // updateChatClient(token: token)
    }
    func updateChatClient(token:Data){
        
            if (self.lastToken != nil){
                self.client?.register(withNotificationToken: token, completion: { (result) in
                    
                    if result.isSuccessful(){
                        print("Push notification registered")
                    }else{
                       print("push not registered")
                    }
                })
            }
    }

    
    // Get token
     func retrieveToken(url: String, completion: @escaping (String?, Error?) -> Void) {
        
        makeGetHttpRequestWith(url: url, dataDict: nil, successBlock: { (status, json, statusCode) in
            if statusCode == 200{
                
                if  let dict = json as? [String:Any] {
                    let token = dict["token"] as! String
                    completion(token, nil)
                }
            }else{
              completion(nil,nil)
            }
        
        }) { (error, statusCode) in
            print("error")
            completion(nil, nil)
        }
    }
}

//MARK:- Twilio Delegate
extension ChatManager: TwilioChatClientDelegate{
    
    func chatClient(_ client: TwilioChatClient, channelAdded channel: TCHChannel) {
        
    }
    func chatClient(_ client: TwilioChatClient, synchronizationStatusUpdated status: TCHClientSynchronizationStatus) {
        
    }
}



//MARK:- Token manager
extension ChatManager: TwilioAccessManagerDelegate{
    func accessManagerTokenWillExpire(_ accessManager: TwilioAccessManager) {
        
        retrieveToken(url: tokenURL) { (token, error) in
            AppUser.accessTokenForTwilio = token
          accessManager.updateToken(token!)
        }
    }
    
    func accessManager(_ accessManager: TwilioAccessManager!, error: Error!) {
        print("Access manager error: \(error.localizedDescription)")
    }
}


//
//  ChatViewController.swift
//  Medville
//
//  Created by Durgesh on 15/02/18.
//  Copyright © 2018 jitendra. All rights reserved.
//

import UIKit
import TwilioChatClient
import MessageKit
import MapKit

class ChatViewController: MessagesViewController {

    var chatUserName: String?
    var chatUserIdentity:String?
    var currentUserName:String?
    var currentIdentity:String = ""
    
    var channel: TCHChannel = TCHChannel()
    var tchMessages:Set<TCHMessage> = Set<TCHMessage>()
    var messages: [MockMessage] = []

    let initialMeesageCount:Int = 100
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ChatManager._sharedManager.delegate = self
        self.channel.delegate = self
        ChatManager._sharedManager.client?.delegate = self
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        messagesCollectionView.messageCellDelegate = self
        messageInputBar.delegate = self

        messageInputBar.sendButton.tintColor = UIColor(red: 69/255, green: 193/255, blue: 89/255, alpha: 1)
        scrollsToBottomOnKeybordBeginsEditing = true // default false
        maintainPositionOnKeyboardFrameChanged = true // default false
        navigationBarSetup()
        self.tabBarController?.tabBar.isHidden = true
        
    }
    
    func navigationBarSetup(){
        let leftBarbtn = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(backButton))
        self.navigationItem.leftBarButtonItem = leftBarbtn
        if AppUser.userType == UserType.kDocotor {
        let rightBarVideobtn = UIBarButtonItem(image: #imageLiteral(resourceName: "iconVideo"), style: .plain, target: self, action: #selector(videoCall))
            let rightBarAudiobtn = UIBarButtonItem(image: #imageLiteral(resourceName: "iconCall"), style: .plain, target: self, action: #selector(audioCall))

        self.navigationItem.rightBarButtonItems = [rightBarAudiobtn,rightBarVideobtn]
            
        }else{
            self.navigationItem.rightBarButtonItems = nil
        }
    }
    
    @objc func audioCall(){
        let audioVC = AudioViewController.shared
        audioVC.callerIdentity = chatUserIdentity!
        audioVC.callerName = chatUserName!
        audioVC.currentUserName = currentUserName!
        audioVC.placeCall()
        self.present(audioVC, animated: true) {
        }
    }
    
    @objc func videoCall(){
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.isVideoCall = true
        let videoVC = storyboard.instantiateViewController(withIdentifier: VCIdentifier.kVideoVC) as? VideoViewController
        let chatUserUuid:String = chatUserIdentity!
        appDelegate.roomName = "\(currentIdentity)_\(chatUserUuid)"
        appDelegate.videoCallerName = chatUserName!
        self.present(videoVC!, animated: true, completion: nil)
    }
    
    @objc func backButton(){
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.title = chatUserName
        self.messages.removeAll()
        loadInitialMessage()
        self.tabBarController?.viewControllers?[2].tabBarItem.badgeValue = nil
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func loadInitialMessage(){
        self.channel.messages?.getLastWithCount(UInt(initialMeesageCount), completion: { (result, messages) in
            if result.isSuccessful(){
                self.addMessages(messagesArray: messages!)
            }
            
        })
    }

    func addMessages(messagesArray:[TCHMessage]){
        
        self.tchMessages = self.tchMessages.union( Set(messagesArray))
        for message in messagesArray {
            self.messages.append(convertMessage(message: message))
        }
      //  sortMessage()
       // self.messagesCollectionView.reloadDataAndKeepOffset()
        self.messagesCollectionView.reloadData()
        self.messagesCollectionView.scrollToBottom()

    }

    func convertMessage(message:TCHMessage) -> MockMessage {
        
        let sender = Sender(id: message.author!, displayName: message.author!)
        
        switch message.messageType {
            
        case TCHMessageType.text:
            return MockMessage(text: message.body!, sender: sender, messageId: String(describing: message.index), date: message.timestampAsDate!)
            
//        case TCHMessageType.media:
//
//        return MockMessage(attributedText: attributedText, sender: senders[randomNumberSender], messageId: uniqueID, date: date)
//
            
//        case "Photo":
//        let image = messageImages[randomNumberImage]
//        return MockMessage(image: image, sender: sender, messageId: uniqueID, date: date)
//        case "Video":
//        let image = messageImages[randomNumberImage]
//        return MockMessage(thumbnail: image, sender: sender, messageId: uniqueID, date: date)
//        case "Location":
//        return MockMessage(location: locations[randomNumberLocation], sender: sender, messageId: uniqueID, date: date)
//        case "Emoji":
//        return MockMessage(emoji: emojis[randomNumberEmoji], sender: sender, messageId: uniqueID, date: date)
        default:
        fatalError("Unrecognized mock message type")
    }
    
    }
}

// MARK: - MessagesDataSource

extension ChatViewController:MessagesDataSource{

    func currentSender() -> Sender {
        let sender = Sender(id: currentIdentity, displayName: currentUserName!)
        return sender
    }

    func numberOfMessages(in messagesCollectionView: MessagesCollectionView) -> Int {
        return self.messages.count
    }

    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return self.messages[indexPath.section]
    }

    func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
       // let name = message.sender.displayName
        let attrString = NSAttributedString(string: "")
        return attrString
       // return NSAttributedString(string: name, attributes: [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: .caption1)])
    }

    func cellBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {

//        struct ConversationDateFormatter {
//            static let formatter: DateFormatter = {
//                let formatter = DateFormatter()
//                formatter.dateStyle = .medium
//                return formatter
//            }()
//        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormat.kCurrentFormat.rawValue
        let strDate = dateFormatter.string(from: message.sentDate)

        
       // let formatter = ConversationDateFormatter.formatter
       // let dateString = formatter.string(from: message.sentDate)
        let dateString = strDate.getTimeDifferenceStringTillNowWith(currentFormat: DateFormat.kCurrentFormat.rawValue)
        let myAttribute = [ NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: .caption2) ]
        let myAttrString = NSAttributedString(string: dateString, attributes: myAttribute)
        return myAttrString
       // return NSAttributedString(string: dateString, attributes: [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: .caption2)])
    }

}




// MARK: - MessagesDisplayDelegate

extension ChatViewController:MessagesDisplayDelegate {

    // MARK: - Text Messages

    func textColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ? .white : .darkText
    }

    func detectorAttributes(for detector: DetectorType, and message: MessageType, at indexPath: IndexPath) -> [NSAttributedStringKey : Any] {
        return MessageLabel.defaultAttributes
    }

    func enabledDetectors(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> [DetectorType] {
        return [.url, .address, .phoneNumber, .date]
    }

    // MARK: - All Messages

    func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ? UIColor(red: 128/255, green: 156/255, blue: 237/255, alpha: 1) : UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
    }

    func messageStyle(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
        let corner: MessageStyle.TailCorner = isFromCurrentSender(message: message) ? .bottomRight : .bottomLeft
        return .bubbleTail(corner, .curved)
        //        let configurationClosure = { (view: MessageContainerView) in}
        //        return .custom(configurationClosure)
    }

    func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        let name :String = (isFromCurrentSender(message: message) ? currentUserName:chatUserName)!
        let firstLetter = name.first?.description
        avatarView.initials = firstLetter
    }

    // MARK: - Location Messages

    func annotationViewForLocation(message: MessageType, at indexPath: IndexPath, in messageCollectionView: MessagesCollectionView) -> MKAnnotationView? {
        let annotationView = MKAnnotationView(annotation: nil, reuseIdentifier: nil)
        let pinImage = #imageLiteral(resourceName: "pin")
        annotationView.image = pinImage
        annotationView.centerOffset = CGPoint(x: 0, y: -pinImage.size.height / 2)
        return annotationView
    }

    func animationBlockForLocation(message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> ((UIImageView) -> Void)? {
        return { view in
            view.layer.transform = CATransform3DMakeScale(0, 0, 0)
            view.alpha = 0.0
            UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0, options: [], animations: {
                view.layer.transform = CATransform3DIdentity
                view.alpha = 1.0
            }, completion: nil)
        }
    }

    func snapshotOptionsForLocation(message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> LocationMessageSnapshotOptions {

        return LocationMessageSnapshotOptions()
    }
}




// MARK: - MessagesLayoutDelegate

extension ChatViewController: MessagesLayoutDelegate {

    func avatarPosition(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> AvatarPosition {
        return AvatarPosition(horizontal: .natural, vertical: .messageBottom)
    }

    func messagePadding(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIEdgeInsets {
        if isFromCurrentSender(message: message) {
            return UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 4)
        } else {
            return UIEdgeInsets(top: 0, left: 4, bottom: 0, right: 30)
        }
    }

    func cellTopLabelAlignment(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> LabelAlignment {
        if isFromCurrentSender(message: message) {
            return .messageTrailing(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10))
        } else {
            return .messageLeading(UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0))
        }
    }

    func cellBottomLabelAlignment(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> LabelAlignment {
        if isFromCurrentSender(message: message) {
            return .messageLeading(UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0))
        } else {
            return .messageTrailing(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10))
        }
    }

    func footerViewSize(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGSize {

        return CGSize(width: messagesCollectionView.bounds.width, height: 10)
    }

    // MARK: - Location Messages

    func heightForLocation(message: MessageType, at indexPath: IndexPath, with maxWidth: CGFloat, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 200
    }

}

// MARK: - MessageCellDelegate

extension ChatViewController: MessageCellDelegate {

    func didTapAvatar(in cell: MessageCollectionViewCell) {
        print("Avatar tapped")
    }

    func didTapMessage(in cell: MessageCollectionViewCell) {
        print("Message tapped")
    }

    func didTapTopLabel(in cell: MessageCollectionViewCell) {
        print("Top label tapped")
    }

    func didTapBottomLabel(in cell: MessageCollectionViewCell) {
        print("Bottom label tapped")
    }
    
    func didTapOnView(in cell: MessageCollectionViewCell) {
        self.messageInputBar.inputTextView.resignFirstResponder()
    }
}

// MARK: - MessageLabelDelegate

extension ChatViewController: MessageLabelDelegate {

    func didSelectAddress(_ addressComponents: [String : String]) {
        print("Address Selected: \(addressComponents)")
    }

    func didSelectDate(_ date: Date) {
        print("Date Selected: \(date)")
    }

    func didSelectPhoneNumber(_ phoneNumber: String) {
        print("Phone Number Selected: \(phoneNumber)")
    }

    func didSelectURL(_ url: URL) {
        print("URL Selected: \(url)")
    }

}

// MARK: - MessageInputBarDelegate

extension ChatViewController: MessageInputBarDelegate {

    func messageInputBar(_ inputBar: MessageInputBar, didPressSendButtonWith text: String) {

        // Each NSTextAttachment that contains an image will count as one empty character in the text: String

        for component in inputBar.inputTextView.components {

            if let image = component as? UIImage {

                let imageMessage = MockMessage(image: image, sender: currentSender(), messageId: UUID().uuidString, date: Date())
                messages.append(imageMessage)
                messagesCollectionView.insertSections([messages.count - 1])

            } else if let text = component as? String {

                //let attributedText = NSAttributedString(string: text, attributes: [.font: UIFont.systemFont(ofSize: 15), .foregroundColor: UIColor.blue])

              //  let message = MockMessage(attributedText: attributedText, sender: currentSender(), messageId: UUID().uuidString, date: Date())
                //messages.append(message)
               // messagesCollectionView.insertSections([messages.count - 1])
                
                let options:TCHMessageOptions = TCHMessageOptions.init()
                options.withBody(text)
                
                self.sendOnPusher(textmsg: text)
                
                channel.messages?.sendMessage(with: options, completion: { (result, sentMessage) in
                    if result.isSuccessful(){
                    //self.messages.append(message)
                    //self.messagesCollectionView.insertSections([self.messages.count - 1])
                    }
                })
            }

        }

        inputBar.inputTextView.text = String()
        messagesCollectionView.scrollToBottom()
    }

    func sendOnPusher(textmsg:String){
        let chatUser:String = chatUserIdentity!

        let parameters = [
            "interests": ["\(chatUser)"],
            "apns": ["aps": ["alert": [
                "title": "New message from: \(currentUserName ?? "")",
                "body": textmsg
                ],
                "badge":1]]
            ] as [String : Any]
        
        makePostForPusher(param: parameters)
    }
}


extension ChatViewController:TwilioChatClientDelegate{
    
    func chatClient(_ client: TwilioChatClient, typingEndedOn channel: TCHChannel, member: TCHMember) {
        print(member)
    }

}

extension ChatViewController:TCHChannelDelegate{
    func chatClient(_ client: TwilioChatClient, channelAdded channel: TCHChannel) {
        print(channel)
    }
    func chatClient(_ client: TwilioChatClient, channel: TCHChannel, messageAdded message: TCHMessage) {
        if !self.tchMessages.contains(message) && channel.uniqueName == self.channel.uniqueName{
            addMessages(messagesArray: [message])
        }
        print(message)
    }
   
}


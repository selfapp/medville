//
//  ChannelListModel.swift
//  Medville
//
//  Created by Durgesh on 19/02/18.
//  Copyright © 2018 jitendra. All rights reserved.
//

import UIKit
import TwilioChatClient

class ChannelListModel: NSObject {
    var channel:TCHChannel?
    var message:TCHMessage?
    var unreadMessage:Int
    var timeStamp:Date?
    override init() {
        unreadMessage = 0
    }
    
}
